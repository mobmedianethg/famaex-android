package com.gabrielguzman.android.famaex.web.endpoints;

import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.web.models.Token;
import com.gabrielguzman.android.famaex.web.models.TokenResult;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.PUT;

/**
 * Created by macpro1 on 3/20/17.
 */

public interface TokenEndPointInterface {

    public static final String LOG_TAG = "TokenEndPointInterface";

    public static final String REGISTER_TOKEN_URL = "providers/AddRegisterId";
    public static final String UPDATE_TOKEN_URL = "providers/UpdateRegisterId";
    public static final String LOGOUT_TOKEN_URL = "providers/LogoutProvider";

    @PUT(REGISTER_TOKEN_URL)
    Call<TokenResult> registerToken(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body Token token);

    @PUT(UPDATE_TOKEN_URL)
    Call<TokenResult> updateToken(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body Token token);

    @PUT(LOGOUT_TOKEN_URL)
    Call<TokenResult> deleteToken(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body Token token);


}
