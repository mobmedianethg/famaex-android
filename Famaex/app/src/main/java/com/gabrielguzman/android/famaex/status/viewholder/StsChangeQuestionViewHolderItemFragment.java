package com.gabrielguzman.android.famaex.status.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.status.interfaces.EditPhotoStatusChangeInterface;

/**
 * Created by macpro1 on 6/12/16.
 */
public class StsChangeQuestionViewHolderItemFragment extends RecyclerView.ViewHolder {

    private ImageView stsChangeQuestionviewholderItemFragmentImage;

    public StsChangeQuestionViewHolderItemFragment(final View itemView, final EditPhotoStatusChangeInterface editPhotoStatusChangeInterface) {
        super(itemView);

        stsChangeQuestionviewholderItemFragmentImage = (ImageView) itemView.findViewById(R.id.sts_change_question_viewholder_item_fragment_image);


        stsChangeQuestionviewholderItemFragmentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editPhotoStatusChangeInterface.onEditPhoto(v, getAdapterPosition());

            }
        });


    }

    public ImageView getStsChangeQuestionviewholderItemFragmentImage() {
        return stsChangeQuestionviewholderItemFragmentImage;
    }

    public void setStsChangeQuestionviewholderItemFragmentImage(ImageView stsChangeQuestionviewholderItemFragmentImage) {
        this.stsChangeQuestionviewholderItemFragmentImage = stsChangeQuestionviewholderItemFragmentImage;
    }
}
