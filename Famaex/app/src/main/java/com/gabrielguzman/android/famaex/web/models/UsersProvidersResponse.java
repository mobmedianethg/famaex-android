package com.gabrielguzman.android.famaex.web.models;

import java.util.ArrayList;

/**
 * Created by jose on 24/04/17.
 */

public class UsersProvidersResponse {

    private String status;
    private String message;
    private ArrayList<DataUsersProviders> data;

    public UsersProvidersResponse() {
    }

    public UsersProvidersResponse(String status, String message, ArrayList<DataUsersProviders> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DataUsersProviders> getData() {
        return data;
    }

    public void setData(ArrayList<DataUsersProviders> data) {
        this.data = data;
    }
}
