package com.gabrielguzman.android.famaex.services.finished;

import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RatingBar;
import android.widget.TextView;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;

import java.text.ParseException;

/**
 * Shows a full screen dialog for the finished service detail.
 *
 * @author Gabriel Guzmán
 * @see DialogFragment
 * @since 2016.28.07
 */
public class FinishedServicesDetailFragment extends DialogFragment {

    private TextView mServName;
    private TextView mClientName;
    private TextView mPrice;
    private TextView mPriceTag;
    private TextView mNTE;
    private TextView mNTETag;
    // private TextView mHourPrice;
    private TextView mDateStart;
    private TextView mTimeStart;
    private TextView mDateFinish;
    private TextView mTimeFinish;
    private TextView mAddress;
    private TextView mPreServiceComment;
    //private TextView mPaid;
    private TextView mArrivalDate;
    private TextView mArrivalTime;
    private TextView mProviderComment;
    private TextView mClientComment;
    private TextView mProviderName;
    private TextView mProviderNameTag;
    private TextView mRating;
    private RatingBar mRatingBar;
    private Service mService;
    private SharedPreferences mUserPref;

    public FinishedServicesDetailFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static FinishedServicesDetailFragment newInstance(String title, Service serv) {
        FinishedServicesDetailFragment frag = new FinishedServicesDetailFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putSerializable("service", serv);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        // create ContextThemeWrapper from the original Activity Context with the custom theme
        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.AppTheme);

        // clone the inflater using the ContextThemeWrapper
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);

        // inflate the layout using the cloned inflater, not default inflater
        return localInflater.inflate(R.layout.finished_services_detail_fragment, container, false);
        // return inflater.inflate(R.layout.finished_services_detail_fragment, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        mUserPref = getActivity().getSharedPreferences(AppSharedPreferences.SHARED_NAME, getActivity().MODE_PRIVATE);

        mServName = (TextView) view.findViewById(R.id.compDetFrgServName);
        mClientName = (TextView) view.findViewById(R.id.compDetFrgClientName);
        // mHourPrice = (TextView) view.findViewById(R.id.compDetFrgHourPrice);
        mPrice = (TextView) view.findViewById(R.id.compDetFrgPrice);
        mPriceTag = (TextView) view.findViewById(R.id.compDetFrgPriceTag);
        mNTE = (TextView) view.findViewById(R.id.compDetFrgNTE);
        mNTETag = (TextView) view.findViewById(R.id.compDetFrgNTETag);
        mDateStart = (TextView) view.findViewById(R.id.compDetFrgDateStart);
        mDateFinish = (TextView) view.findViewById(R.id.compDetFrgDateFinish);
        mTimeFinish = (TextView) view.findViewById(R.id.compDetFrgTimeFinish);
        mTimeStart = (TextView) view.findViewById(R.id.compDetFrgTimeStart);
        //mPaid = (TextView) view.findViewById(R.id.compDetFrgPaid);
        mAddress = (TextView) view.findViewById(R.id.compDetFrgAddress);
        mPreServiceComment = (TextView) view.findViewById(R.id.compDetFrgPreServComment);
        mArrivalDate = (TextView) view.findViewById(R.id.compDetFrgProvDate);
        mArrivalTime = (TextView) view.findViewById(R.id.compDetFrgProvTime);
        mProviderComment = (TextView) view.findViewById(R.id.compDetFrgProviderComment);
        mClientComment = (TextView) view.findViewById(R.id.compDetFrgClientComment);
        mRating = (TextView) view.findViewById(R.id.compDetFrgRating);
        mRatingBar = (RatingBar) view.findViewById(R.id.compDetFrgRatingBar);
        mProviderName = (TextView) view.findViewById(R.id.compDetFrgProviderName);
        mProviderNameTag = (TextView) view.findViewById(R.id.compDetFrgProviderNameTag);

        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Alerta");
        getDialog().setTitle(title);

        mService = (Service) getArguments().getSerializable("service");

        boolean state = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

        if (state) {

            mPrice.setVisibility(View.VISIBLE);
            mPriceTag.setVisibility(View.VISIBLE);
            mProviderName.setVisibility(View.VISIBLE);
            mProviderNameTag.setVisibility(View.VISIBLE);

            mProviderName.setText(mService.getProviderName());

        } else {

            if (mService.isVisibility()) {

                mPrice.setVisibility(View.VISIBLE);
                mPriceTag.setVisibility(View.VISIBLE);

            } else {

                mPrice.setVisibility(View.GONE);
                mPriceTag.setVisibility(View.GONE);

            }

        }

        if (mService.getNteProvider() != 0) {

            mNTE.setText(String.format("%d €", mService.getNteProvider()));
            mNTETag.setVisibility(View.VISIBLE);
            mNTE.setVisibility(View.VISIBLE);

        }


        mServName.setText(mService.getServiceName());
        mClientName.setText(mService.getClientName());
        mPrice.setText(String.format("%d €", mService.getTotalPrice()));
        // mHourPrice.setText(String.format("€%d/h", mService.getPrice()));
        mAddress.setText(mService.getClientAddress());
        mPreServiceComment.setText(mService.getComments());
        // mPaid.setText(mService.getPaid() ? "Sí":"No");
        mProviderComment.setText(mService.getProviderComment());
        mClientComment.setText(mService.getClientRatingComment());

        try {
            mDateStart.setText(Famaex.dateAppFormat.format(Famaex.dateBackendFormat.parse(mService.getDateStart())));
            mDateFinish.setText(Famaex.dateAppFormat.format(Famaex.dateBackendFormat.parse(mService.getDateFinish())));
            mArrivalDate.setText(Famaex.dateAppFormat.format(Famaex.dateArrivalBackendFormat.parse(mService.getDateArrival())));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        String hourUnformatted = mService.getTimeArrival();
        String timeTextview = "--";

        if (hourUnformatted.length() > 6) {

            String[] timeSecond = mService.getTimeArrival().split(":");

            try {

                timeTextview = timeSecond[0] + ":" + timeSecond[1] + " " + timeSecond[2].substring(3);

            } catch (Exception e) {
                timeTextview = "--";

            }


        } else {

            timeTextview = "--";

        }

        mTimeStart.setText(mService.getTimeStart().substring(0, 8));
        mTimeFinish.setText(mService.getTimeFinish().substring(0, 8));
        mArrivalTime.setText(timeTextview);

        mRating.setText(mService.getRatingValue() == 0 ?
                "No hay calificación" : String.format("%.1f / 5,0", 1.0f * mService.getRatingValue()));
        mRatingBar.setStepSize(0.5f);
        mRatingBar.setRating(mService.getRatingValue());
      /*  LayerDrawable stars = (LayerDrawable) mRatingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        /* stars.getDrawable(1).setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);*/
    }

    @Override

    public void onResume() {

        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();

        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

        // Call super onResume after sizing
        super.onResume();

    }

}
