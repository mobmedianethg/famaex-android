package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 20/04/17.
 */

public class TariffRequest {

    private Integer id_admin;
    private Boolean tariff;

    public TariffRequest() {
    }

    public TariffRequest(Integer id_admin, Boolean tariff) {
        this.id_admin = id_admin;
        this.tariff = tariff;
    }

    public Integer getId_admin() {
        return id_admin;
    }

    public void setId_admin(Integer id_amin) {
        this.id_admin = id_amin;
    }

    public Boolean getTariff() {
        return tariff;
    }

    public void setTariff(Boolean tariff) {
        this.tariff = tariff;
    }
}
