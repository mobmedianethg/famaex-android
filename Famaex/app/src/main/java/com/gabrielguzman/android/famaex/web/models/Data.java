package com.gabrielguzman.android.famaex.web.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by jose on 05/04/17.
 */

public class Data {

    public static final String SERVICE_STATUS = "service state";
    public static final String SERVICE_ID = "service id";

    private String cif;
    private String company;
    private String client_name;
    private String codigo_postal;
    private Boolean urgent;
    private ArrayList<String> preference_selected;
    @SerializedName(SERVICE_STATUS)
    private String service_state;
    private Integer service_request_id;
    @SerializedName(SERVICE_ID)
    private Integer service_id;
    private String service;
    private String subservice;
    private String service_per_mts2;
    private Double technical_hour_price;
    private Double assistant_hour_price;
    private String comment_client;
    private Integer nte_provider;
    private Integer nte_client;
    private Double mts2_place;
    private Double price_mts2;
    private Double total_price_mt2;
    private String visibility_tariff;

    public Data() {
    }

    public Data(String cif, String company, String client_name, String codigo_postal, Boolean urgent, ArrayList<String> preference_selected, String service_state, Integer service_request_id, Integer service_id, String service, String subservice, String service_per_mts2, Double technical_hour_price, Double assistant_hour_price, String comment_client, Integer nte_provider, Integer nte_client, Double mts2_place, Double price_mts2, Double total_price_mt2, String visibility_tariff) {
        this.cif = cif;
        this.company = company;
        this.client_name = client_name;
        this.codigo_postal = codigo_postal;
        this.urgent = urgent;
        this.preference_selected = preference_selected;
        this.service_state = service_state;
        this.service_request_id = service_request_id;
        this.service_id = service_id;
        this.service = service;
        this.subservice = subservice;
        this.service_per_mts2 = service_per_mts2;
        this.technical_hour_price = technical_hour_price;
        this.assistant_hour_price = assistant_hour_price;
        this.comment_client = comment_client;
        this.nte_provider = nte_provider;
        this.nte_client = nte_client;
        this.mts2_place = mts2_place;
        this.price_mts2 = price_mts2;
        this.total_price_mt2 = total_price_mt2;
        this.visibility_tariff = visibility_tariff;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getCodigo_postal() {
        return codigo_postal;
    }

    public void setCodigo_postal(String codigo_postal) {
        this.codigo_postal = codigo_postal;
    }

    public Boolean getUrgent() {
        return urgent;
    }

    public void setUrgent(Boolean urgent) {
        this.urgent = urgent;
    }

    public ArrayList<String> getPreference_selected() {
        return preference_selected;
    }

    public void setPreference_selected(ArrayList<String> preference_selected) {
        this.preference_selected = preference_selected;
    }

    public String getService_state() {
        return service_state;
    }

    public void setService_state(String service_state) {
        this.service_state = service_state;
    }

    public Integer getService_request_id() {
        return service_request_id;
    }

    public void setService_request_id(Integer service_request_id) {
        this.service_request_id = service_request_id;
    }

    public Integer getService_id() {
        return service_id;
    }

    public void setService_id(Integer service_id) {
        this.service_id = service_id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getSubservice() {
        return subservice;
    }

    public void setSubservice(String subservice) {
        this.subservice = subservice;
    }

    public String getService_per_mts2() {
        return service_per_mts2;
    }

    public void setService_per_mts2(String service_per_mts2) {
        this.service_per_mts2 = service_per_mts2;
    }

    public Double getTechnical_hour_price() {
        return technical_hour_price;
    }

    public void setTechnical_hour_price(Double technical_hour_price) {
        this.technical_hour_price = technical_hour_price;
    }

    public Double getAssistant_hour_price() {
        return assistant_hour_price;
    }

    public void setAssistant_hour_price(Double assistant_hour_price) {
        this.assistant_hour_price = assistant_hour_price;
    }

    public String getComment_client() {
        return comment_client;
    }

    public void setComment_client(String comment_client) {
        this.comment_client = comment_client;
    }

    public Integer getNte_provider() {
        return nte_provider;
    }

    public void setNte_provider(Integer nte_provider) {
        this.nte_provider = nte_provider;
    }

    public Integer getNte_client() {
        return nte_client;
    }

    public void setNte_client(Integer nte_client) {
        this.nte_client = nte_client;
    }

    public Double getMts2_place() {
        return mts2_place;
    }

    public void setMts2_place(Double mts2_place) {
        this.mts2_place = mts2_place;
    }

    public Double getPrice_mts2() {
        return price_mts2;
    }

    public void setPrice_mts2(Double price_mts2) {
        this.price_mts2 = price_mts2;
    }

    public Double getTotal_price_mt2() {
        return total_price_mt2;
    }

    public void setTotal_price_mt2(Double total_price_mt2) {
        this.total_price_mt2 = total_price_mt2;
    }

    public String getVisibility_tariff() {
        return visibility_tariff;
    }

    public void setVisibility_tariff(String visibility_tariff) {
        this.visibility_tariff = visibility_tariff;
    }
}
