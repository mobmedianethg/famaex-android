package com.gabrielguzman.android.famaex.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Model for the Service object.
 *
 * @author Gabriel Guzmán
 * @since 2016.25.06
 */
public class Service implements Serializable {

    private String serviceName = "";
    private String subService = "";
    private String dateStart = "";
    private String dateFinish = "";
    private String TimeStart = "";
    private String TimeFinish = "";
    private int price = 0;
    private int totalPrice = 0;
    private int nteProvider = 0;
    private Boolean typePrice = false;
    private String priority = "";
    private String comments = "";
    private String clientName = "";
    private String clientPhone = "";
    private String clientAddress = "";
    private int serviceRqID = 0;
    private String dateSelected = "";
    private String TimeSelected = "";
    private String admin = "";
    private String cif = "";
    private String dateArrival = "";
    private String timeArrival = "";
    private int statusID = 0;
    private String status = "";
    private Boolean paid = false;
    private int serviceID;
    private String providerComment = "";
    private String clientRatingComment = "";
    private int ratingValue;
    private String postalCode = "";
    private boolean visibility;
    private String providerName;
    private ArrayList<DateService> preferedDates;

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public int getStatusID() {
        return statusID;
    }

    public void setStatusID(int statusID) {
        this.statusID = statusID;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getDateArrival() {
        return dateArrival;
    }

    public void setDateArrival(String dateArrival) {
        this.dateArrival = dateArrival;
    }

    public String getTimeArrival() {
        return timeArrival;
    }

    public void setTimeArrival(String timeArrival) {
        this.timeArrival = timeArrival;
    }

    public int getServiceID() {
        return serviceID;
    }

    public void setServiceID(int serviceID) {
        this.serviceID = serviceID;
    }

    public String getProviderComment() {
        return providerComment;
    }

    public void setProviderComment(String providerComment) {
        this.providerComment = providerComment;
    }

    public String getClientRatingComment() {
        return clientRatingComment;
    }

    public void setClientRatingComment(String clientRatingComment) {
        this.clientRatingComment = clientRatingComment;
    }

    public int getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(int ratingValue) {
        this.ratingValue = ratingValue;
    }

    public String getClientPhone() {
        return clientPhone;
    }

    public void setClientPhone(String clientPhone) {
        this.clientPhone = clientPhone;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public int getServiceRqID() {
        return serviceRqID;
    }

    public void setServiceRqID(int serviceRqID) {
        this.serviceRqID = serviceRqID;
    }

    public String getDateSelected() {
        return dateSelected;
    }

    public void setDateSelected(String dateSelected) {
        this.dateSelected = dateSelected;
    }

    public String getTimeSelected() {
        return TimeSelected;
    }

    public void setTimeSelected(String timeSelected) {
        TimeSelected = timeSelected;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getSubService() {
        return subService;
    }

    public void setSubService(String subService) {
        this.subService = subService;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(String dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getTimeStart() {
        return TimeStart;
    }

    public void setTimeStart(String timeStart) {
        TimeStart = timeStart;
    }

    public String getTimeFinish() {
        return TimeFinish;
    }

    public void setTimeFinish(String timeFinish) {
        TimeFinish = timeFinish;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getNteProvider() {
        return nteProvider;
    }

    public void setNteProvider(int nteProvider) {
        this.nteProvider = nteProvider;
    }

    public Boolean getTypePrice() {
        return typePrice;
    }

    public void setTypePrice(boolean typePrice) {
        this.typePrice = typePrice;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public ArrayList<DateService> getPreferedDates() {
        return preferedDates;
    }

    public void setPreferedDates(ArrayList<DateService> preferedDates) {
        this.preferedDates = preferedDates;
    }
}