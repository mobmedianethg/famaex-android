package com.gabrielguzman.android.famaex.web.models;

import java.util.ArrayList;

/**
 * Created by jose on 08/04/17.
 */

public class UserRegister {

    private Boolean provider;
    private String name;
    private String email;
    private String password;
    private String phone;
    private ArrayList<Object> company;
    private ArrayList<String> address;
    private String nif;
    private ArrayList<String> specializations;

    public UserRegister() {
    }

    public UserRegister(Boolean provider, String name, String email, String password, String phone, ArrayList<Object> company, ArrayList<String> address, String nif, ArrayList<String> specializations) {
        this.provider = provider;
        this.name = name;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.company = company;
        this.address = address;
        this.nif = nif;
        this.specializations = specializations;
    }

    public Boolean getProvider() {
        return provider;
    }

    public void setProvider(Boolean provider) {
        this.provider = provider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public ArrayList<Object> getCompany() {
        return company;
    }

    public void setCompany(ArrayList<Object> company) {
        this.company = company;
    }

    public ArrayList<String> getAddress() {
        return address;
    }

    public void setAddress(ArrayList<String> address) {
        this.address = address;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public ArrayList<String> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(ArrayList<String> specializations) {
        this.specializations = specializations;
    }
}
