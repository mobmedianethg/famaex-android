package com.gabrielguzman.android.famaex.services.available.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.services.available.interfaces.UsersProvidersInterface;
import com.gabrielguzman.android.famaex.services.available.viewholder.UsersProvidersViewHolder;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.web.models.DataUsersProviders;

import java.util.ArrayList;

/**
 * Created by jose on 24/04/17.
 */

public class UsersProvidersApdater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<DataUsersProviders> usersProviders;
    private Context context;
    private LayoutInflater layoutInflater;
    private UsersProvidersInterface usersProvidersInterface;
    private SharedPreferences mUserPref;

    public UsersProvidersApdater(ArrayList<DataUsersProviders> usersProviders, Context context, UsersProvidersInterface usersProvidersInterface) {
        this.usersProviders = usersProviders;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(this.context);
        this.usersProvidersInterface = usersProvidersInterface;
        mUserPref = this.context.getSharedPreferences(AppSharedPreferences.SHARED_NAME, context.MODE_PRIVATE);

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.users_provider_item_view_holder, parent, false);
        UsersProvidersViewHolder holder = new UsersProvidersViewHolder(view, this.usersProvidersInterface);

        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final UsersProvidersViewHolder viewHolder = (UsersProvidersViewHolder) holder;
        DataUsersProviders dataUsersProviders = usersProviders.get(position);

        if (dataUsersProviders.getSelect() == true) {

            viewHolder.getUsersProviderViewHolderCheckImageview().setVisibility(View.VISIBLE);

        } else {

            viewHolder.getUsersProviderViewHolderCheckImageview().setVisibility(View.GONE);

        }

        viewHolder.getUsersProviderViewHolderNameTextview().setText(dataUsersProviders.getName());
        viewHolder.getUsersProviderViewHolderEmailTextview().setText(dataUsersProviders.getEmail());

    }


    public void clearList() {

        for (DataUsersProviders dataUsersProvider : usersProviders) {

            dataUsersProvider.setSelect(false);

        }

    }

    public void updateElements(int position) {


        int element = 0;

        for (DataUsersProviders dataUsersProviders : usersProviders) {


            if (element == position) {

                dataUsersProviders.setSelect(true);
                mUserPref.edit().putInt(AppSharedPreferences.PROVIDER_SERVICE_SELECTED, dataUsersProviders.getUser_id()).apply();

            } else {

                dataUsersProviders.setSelect(false);

            }

            element++;


        }


        this.notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return this.usersProviders.size();
    }
}
