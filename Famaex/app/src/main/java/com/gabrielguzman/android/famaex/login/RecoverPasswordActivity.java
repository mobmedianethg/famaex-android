package com.gabrielguzman.android.famaex.login;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.utils.Famaex;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.UserEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A screen that allows to reset the password and see Famaex contact info.
 *
 * @author Gabriel Guzman
 * @see AppCompatActivity
 * @since 2017.26.02
 */
public class RecoverPasswordActivity extends AppCompatActivity implements Callback<User> {

    public static final String LOG_TAG = "RecoverPasswordActivity";
    public static int recoverPasswordRetry;
    public Retrofit retrofit;
    private EditText mUsernameView;
    private View mProgressOverlay;
    private TextView mPsswdRecoverButton;
    private RecoverPasswordActivity activity;
    private Toolbar activityRecoverPasswordToolbar;
    private ActionBar activityRecoverPasswordActionBar;
    private TextView progressMssg;
    private User userResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recover_password);

        activity = (RecoverPasswordActivity) this;

        activityRecoverPasswordToolbar = (Toolbar) findViewById(R.id.activity_recover_password_toolbar);
        setSupportActionBar(activityRecoverPasswordToolbar);
        activityRecoverPasswordActionBar = getSupportActionBar();

        Utils.setPortaitOrientation(activity);
        retrofit = RestClient.getRetrofitInstance();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorPrimary));

        }

        recoverPasswordRetry = 0;

        if (activityRecoverPasswordActionBar != null) {
            activityRecoverPasswordActionBar.setDisplayHomeAsUpEnabled(true);
        }

        mUsernameView = (EditText) findViewById(R.id.recoverEmail);

        progressMssg = (TextView) findViewById(R.id.progressMssg);
        progressMssg.setText(activity.getResources().getString(R.string.loading_recover_password));

        mPsswdRecoverButton = (TextView) findViewById(R.id.sign_in_button);
        mPsswdRecoverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Utils.hideSoftKeyboard(activity);

                attemptRecover();
            }
        });

        mProgressOverlay = findViewById(R.id.progress_overlay);
        ImageView mLogo = (ImageView) findViewById(R.id.famaexLogoRecover);
        if (mLogo != null) {
            mLogo.requestFocus();
        }
    }

    /**
     * Attempts to reset the password for the specified email.
     */
    private void attemptRecover() {

        // Reset errors.
        mUsernameView.setError(null);

        // Store values at the time of the login attempt.
        String user = mUsernameView.getText().toString();

        // Check for a valid username.
        if (TextUtils.isEmpty(user)) {

            mUsernameView.setError(getString(R.string.errorFieldRequired));
            mUsernameView.requestFocus();

        } else if (!Utils.verifyEmail(user)) {

            mUsernameView.setError(getString(R.string.invalid_mail_format));
            mUsernameView.requestFocus();

        } else {

            mPsswdRecoverButton.setEnabled(false);

            Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

            recoverPasswordRetry = 0;

            UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);

            User userRecover = new User(null, user, null, null, null, null, null, true, null, null);

            Call<User> call = userEndPointInterface.recoverPassword(RestClient.CONTENT_TYPE, userRecover);
            call.enqueue(activity);


        }
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {

        if (response.body() != null) {

            userResponse = response.body();

            if (Utils.verifyOKTAG(userResponse, Utils.RECOVER_USER, activity)) {

                Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

                Toast.makeText(getBaseContext(), activity.getResources().getString(R.string.activity_recover_password_success), Toast.LENGTH_SHORT).show();

                finish();


            } else {

                displayError(userResponse.getStatus());

            }


        } else {

            recoverPasswordRetry++;

            if (recoverPasswordRetry < 3) {

                call.clone().enqueue(this);

            } else {


                displayError(activity.getResources().getString(R.string.recover_password_error_message));

            }

        }


    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {


        recoverPasswordRetry++;

        if (recoverPasswordRetry < 3) {

            call.clone().enqueue(this);

        } else {

            displayError(activity.getResources().getString(R.string.recover_password_error_message));

        }


    }


    private void displayError(String message) {

        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

        mPsswdRecoverButton.setEnabled(true);


    }

}
