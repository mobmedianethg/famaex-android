package com.gabrielguzman.android.famaex.utils;

/**
 * Created by jose on 30/03/17.
 */

public class AppSharedPreferences {

    public static final String SHARED_NAME = "user_pref";

    public static final String LOGGED_USER = "logged";
    public static final String PROVIDER_ID = "providerID";
    public static final String PROVIDER_NAME = "providerName";
    public static final String PROVIDER_COMPANY = "providerCompany";
    public static final String PROVIDER_ADMIN = "providerIsAdmin";
    public static final String DEVICE_TOKEN = "devicetoken";
    public static final String PROVIDER_EMAIL = "providerEmail";
    public static final String PROVIDER_COMPANY_CIF = "companyCif";
    public static final String PROVIDER_COMPANY_NUMBER_EMPLOYEES = "companyNumberEmployees";
    public static final String PROVIDER_COMPANY_NUMBER_SELF_EMPLOYED = "companyNumberSelfEmployed";
    public static final String PROVIDER_COMPANY_QUALITY_FILTER = "companyQualityFilter";
    public static final String PROVIDER_COMPANY_LEGAL_TOPICS = "companyLegalTopics";
    public static final String PROVIDER_SERVICE_SELECTED = "userProviderSelected";


}
