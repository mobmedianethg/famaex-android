package com.gabrielguzman.android.famaex.password.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.password.viewholder.SpecializationUserViewHolder;

import java.util.ArrayList;

/**
 * Created by macpro1 on 6/12/16.
 */
public class RecyclerViewSpecializationUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String LOG_TAG = "RecyclerViewSpecializationUserAdapter";
    private ArrayList<String> specializations;
    private Context context;
    private LayoutInflater layoutInflater;

    public RecyclerViewSpecializationUserAdapter(ArrayList<String> specializations, Context context) {
        this.specializations = specializations;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.specialization_user_item_view_holder, parent, false);
        SpecializationUserViewHolder holder = new SpecializationUserViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final SpecializationUserViewHolder viewHolder = (SpecializationUserViewHolder) holder;
        String specialization = specializations.get(position);

        viewHolder.getSpecializationUserViewHolderNameTextview().setText(specialization);


    }

    public void updateElements(ArrayList<String> specializations) {

        this.specializations = specializations;

        this.notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return this.specializations.size();
    }

}
