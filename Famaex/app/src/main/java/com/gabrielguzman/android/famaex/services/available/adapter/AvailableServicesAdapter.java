package com.gabrielguzman.android.famaex.services.available.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.models.DateService;
import com.gabrielguzman.android.famaex.services.available.interfaces.AvailableServicesDetailFragmentInterface;
import com.gabrielguzman.android.famaex.services.available.viewholder.AvailableServicesDetailFragmentItemViewHolder;
import com.gabrielguzman.android.famaex.utils.Famaex;

import java.util.ArrayList;

/**
 * Created by jose on 25/04/17.
 */

public class AvailableServicesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<DateService> dates;
    private Context context;
    private LayoutInflater layoutInflater;
    private AvailableServicesDetailFragmentInterface availableServicesDetailFragmentInterface;


    public AvailableServicesAdapter(ArrayList<DateService> dates, Context context, AvailableServicesDetailFragmentInterface availableServicesDetailFragmentInterface) {

        this.dates = dates;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(this.context);
        this.availableServicesDetailFragmentInterface = availableServicesDetailFragmentInterface;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.available_services_detail_fragment_item, parent, false);
        AvailableServicesDetailFragmentItemViewHolder holder = new AvailableServicesDetailFragmentItemViewHolder(view, this.availableServicesDetailFragmentInterface);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final AvailableServicesDetailFragmentItemViewHolder viewHolder = (AvailableServicesDetailFragmentItemViewHolder) holder;
        DateService date = dates.get(position);

        try {

            viewHolder.getAvDetFrgDateStart().setText(Famaex.dateAppFormat.format(Famaex.dateBackendFormat.parse(date.getStartDate())));
            viewHolder.getAvDetFrgDateFinish().setText(Famaex.dateAppFormat.format(Famaex.dateBackendFormat.parse(date.getEndDate())));

            viewHolder.getAvDetFrgTimeStart().setText(date.getStartTime().substring(0, 8));
            viewHolder.getAvDetFrgTimeFinish().setText(date.getEndTime().substring(0, 8));

        } catch (Exception e) {

            viewHolder.getAvDetFrgDateStart().setText(context.getResources().getString(R.string.completeDatePlaceholder));
            viewHolder.getAvDetFrgDateFinish().setText(context.getResources().getString(R.string.completeDatePlaceholder));

            viewHolder.getAvDetFrgTimeStart().setText("");
            viewHolder.getAvDetFrgTimeFinish().setText("");


        }

        if (date.isSelected()) {

            viewHolder.getAvailableServicesDetailFragmentItemCheck().setVisibility(View.VISIBLE);

        } else {

            viewHolder.getAvailableServicesDetailFragmentItemCheck().setVisibility(View.GONE);

        }


    }


    public void updateDateSelection(int position) {

        for (int index = 0; index < this.dates.size(); index++) {

            if (index == position) {


                this.dates.get(index).setSelected(true);


            } else {

                this.dates.get(index).setSelected(false);

            }

        }

        this.notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return dates.size();
    }
}
