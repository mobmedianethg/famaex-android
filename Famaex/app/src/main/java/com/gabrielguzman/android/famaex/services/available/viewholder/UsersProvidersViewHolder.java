package com.gabrielguzman.android.famaex.services.available.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.services.available.interfaces.UsersProvidersInterface;

/**
 * Created by macpro1 on 6/12/16.
 */
public class UsersProvidersViewHolder extends RecyclerView.ViewHolder {

    private TextView usersProviderViewHolderNameTextview;
    private TextView usersProviderViewHolderEmailTextview;
    private ImageView usersProviderViewHolderCheckImageview;
    private View usersProviderViewHolderViewHolderDivider;
    private LinearLayout usersProviderItemViewHolderContainer;

    public UsersProvidersViewHolder(final View itemView, final UsersProvidersInterface usersProvidersInterface) {
        super(itemView);

        usersProviderViewHolderEmailTextview = (TextView) itemView.findViewById(R.id.users_providers_item_view_holder_item_email_textview);
        usersProviderViewHolderNameTextview = (TextView) itemView.findViewById(R.id.users_providers_item_view_holder_item_name_textview);
        usersProviderViewHolderCheckImageview = (ImageView) itemView.findViewById(R.id.users_providers_item_view_holder_item_icon_imageview);
        usersProviderViewHolderViewHolderDivider = itemView.findViewById(R.id.users_providers_item_view_holder_divider);
        usersProviderItemViewHolderContainer = (LinearLayout) itemView.findViewById(R.id.users_providers_item_view_holder_container);

        usersProviderItemViewHolderContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                usersProvidersInterface.onUserSelected(v, getAdapterPosition());

            }
        });


    }

    public TextView getUsersProviderViewHolderNameTextview() {
        return usersProviderViewHolderNameTextview;
    }

    public void setUsersProviderViewHolderNameTextview(TextView usersProviderViewHolderNameTextview) {
        this.usersProviderViewHolderNameTextview = usersProviderViewHolderNameTextview;
    }

    public TextView getUsersProviderViewHolderEmailTextview() {
        return usersProviderViewHolderEmailTextview;
    }

    public void setUsersProviderViewHolderEmailTextview(TextView usersProviderViewHolderEmailTextview) {
        this.usersProviderViewHolderEmailTextview = usersProviderViewHolderEmailTextview;
    }

    public ImageView getUsersProviderViewHolderCheckImageview() {
        return usersProviderViewHolderCheckImageview;
    }

    public void setUsersProviderViewHolderCheckImageview(ImageView usersProviderViewHolderCheckImageview) {
        this.usersProviderViewHolderCheckImageview = usersProviderViewHolderCheckImageview;
    }

    public View getUsersProviderViewHolderViewHolderDivider() {
        return usersProviderViewHolderViewHolderDivider;
    }

    public void setUsersProviderViewHolderViewHolderDivider(View usersProviderViewHolderViewHolderDivider) {
        this.usersProviderViewHolderViewHolderDivider = usersProviderViewHolderViewHolderDivider;
    }

    public LinearLayout getUsersProviderItemViewHolderContainer() {
        return usersProviderItemViewHolderContainer;
    }

    public void setUsersProviderItemViewHolderContainer(LinearLayout usersProviderItemViewHolderContainer) {
        this.usersProviderItemViewHolderContainer = usersProviderItemViewHolderContainer;
    }
}
