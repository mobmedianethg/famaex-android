package com.gabrielguzman.android.famaex.about;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.faq.FaqFragment;
import com.gabrielguzman.android.famaex.utils.Utils;

/**
 * Class for the AboutActivity screen.
 *
 * @author Gabriel Guzman
 * @see AppCompatActivity
 * @since 2016.28.06
 */
public class AboutActivity extends AppCompatActivity {

    private View mDummy;
    private int mDummyClick = 0;
    private TextView faqButton;
    private AboutActivity activity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        // my_child_toolbar is defined in the layout file
        Toolbar guidesToolbar = (Toolbar) findViewById(R.id.famaexAboutToolbar);
        setSupportActionBar(guidesToolbar);

        activity = (AboutActivity) this;

        Utils.setPortaitOrientation(activity);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        faqButton = (TextView) findViewById(R.id.faqButton);
        faqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFaqDialog();
            }
        });

        View mLogo = findViewById(R.id.famaexLogoAbout);

        if (mLogo != null) {
            mLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDummyClick++;
                    if (mDummyClick > 6) {
                        mDummy = findViewById(R.id.dummy);
                        if (mDummy != null) {
                            mDummy.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });
        }
    }

    private void showFaqDialog() {
        FragmentManager fm = getFragmentManager();
        FaqFragment faqFragment = FaqFragment.newInstance();

        faqFragment.show(fm, "fragment_faq");
    }
}
