package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 07/04/17.
 */

public class UpdateResponse {

    private String status;

    public UpdateResponse() {
    }

    public UpdateResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
