package com.gabrielguzman.android.famaex.services.available.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.services.available.interfaces.AvailableServicesDetailFragmentInterface;

/**
 * Created by jose on 25/04/17.
 */

public class AvailableServicesDetailFragmentItemViewHolder extends RecyclerView.ViewHolder {

    private TextView avDetFrgDateStart;
    private TextView avDetFrgTimeStart;
    private TextView avDetFrgDateFinish;
    private TextView avDetFrgTimeFinish;
    private ImageView availableServicesDetailFragmentItemCheck;
    private LinearLayout availableServicesDetailFragmentItemContainer;

    public AvailableServicesDetailFragmentItemViewHolder(final View itemView, final AvailableServicesDetailFragmentInterface availableServicesDetailFragmentInterface) {

        super(itemView);

        avDetFrgDateStart = (TextView) itemView.findViewById(R.id.avDetFrgDateStart);
        avDetFrgTimeStart = (TextView) itemView.findViewById(R.id.avDetFrgTimeStart);
        avDetFrgDateFinish = (TextView) itemView.findViewById(R.id.avDetFrgDateFinish);
        avDetFrgTimeFinish = (TextView) itemView.findViewById(R.id.avDetFrgTimeFinish);
        availableServicesDetailFragmentItemCheck = (ImageView) itemView.findViewById(R.id.available_services_detail_fragment_item_check);
        availableServicesDetailFragmentItemContainer = (LinearLayout) itemView.findViewById(R.id.available_services_detail_fragment_item_container);

        availableServicesDetailFragmentItemContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                availableServicesDetailFragmentInterface.onDateSlected(v, getAdapterPosition());

            }
        });

    }


    public TextView getAvDetFrgDateStart() {
        return avDetFrgDateStart;
    }

    public void setAvDetFrgDateStart(TextView avDetFrgDateStart) {
        this.avDetFrgDateStart = avDetFrgDateStart;
    }

    public TextView getAvDetFrgTimeStart() {
        return avDetFrgTimeStart;
    }

    public void setAvDetFrgTimeStart(TextView avDetFrgTimeStart) {
        this.avDetFrgTimeStart = avDetFrgTimeStart;
    }

    public TextView getAvDetFrgDateFinish() {
        return avDetFrgDateFinish;
    }

    public void setAvDetFrgDateFinish(TextView avDetFrgDateFinish) {
        this.avDetFrgDateFinish = avDetFrgDateFinish;
    }

    public TextView getAvDetFrgTimeFinish() {
        return avDetFrgTimeFinish;
    }

    public void setAvDetFrgTimeFinish(TextView avDetFrgTimeFinish) {
        this.avDetFrgTimeFinish = avDetFrgTimeFinish;
    }

    public ImageView getAvailableServicesDetailFragmentItemCheck() {
        return availableServicesDetailFragmentItemCheck;
    }

    public void setAvailableServicesDetailFragmentItemCheck(ImageView availableServicesDetailFragmentItemCheck) {
        this.availableServicesDetailFragmentItemCheck = availableServicesDetailFragmentItemCheck;
    }

    public LinearLayout getAvailableServicesDetailFragmentItemContainer() {
        return availableServicesDetailFragmentItemContainer;
    }

    public void setAvailableServicesDetailFragmentItemContainer(LinearLayout availableServicesDetailFragmentItemContainer) {
        this.availableServicesDetailFragmentItemContainer = availableServicesDetailFragmentItemContainer;
    }
}
