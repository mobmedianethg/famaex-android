package com.gabrielguzman.android.famaex.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.TokenEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.Token;
import com.gabrielguzman.android.famaex.web.models.TokenResult;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Class that obtains and saves the device token for GCM, both
 * internally and at the backend
 *
 * @author Gabriel Guzmán
 * @since 2016.15.07
 */
public class GCMRegistrationIntentService extends IntentService implements Callback<TokenResult> {

    public static final String REGISTRATION_SUCCESS = "RegistrationSuccess";
    public static final String REGISTRATION_ERROR = "RegistrationError";
    public static final String LOG_TAG = "GCMRegIntentService";
    private static final String SENDER_ID = "902980036607";
    private static final int REGISTER_TOKEN = 1;
    private static final int UPDATE_TOKEN = 2;
    private static int registerOperationRetry;
    private static int tokenOperation;
    private SharedPreferences mUserPref;
    private Retrofit retrofit;
    private TokenResult tokenResult;
    private Token tokenObject;


    public GCMRegistrationIntentService() {
        super("");

        retrofit = RestClient.getRetrofitInstance();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        registerGCM();
    }

    private void registerGCM() {
        String token;
        try {
            InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
            token = instanceID.getToken(SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            TokenEndPointInterface tokenEndPointInterface = retrofit.create(TokenEndPointInterface.class);

            mUserPref = getSharedPreferences(AppSharedPreferences.SHARED_NAME, MODE_PRIVATE);


            registerOperationRetry = 0;

            tokenObject = new Token(token, mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), Utils.PLATFORM_VALUE, null, null);

            Call<TokenResult> call = tokenEndPointInterface.registerToken(RestClient.CONTENT_TYPE, tokenObject);
            call.enqueue(this);

            tokenOperation = REGISTER_TOKEN;


        } catch (Exception e) {

            Log.e(LOG_TAG, "Registration error " + e.getMessage());

        }
    }

    @Override
    public void onResponse(Call<TokenResult> call, Response<TokenResult> response) {

        if (response.body() != null) {

            tokenResult = response.body();

            if (Utils.verifyOKTAG(tokenResult, getApplicationContext())) {

                mUserPref.edit().putString(AppSharedPreferences.DEVICE_TOKEN, tokenObject.getRegister_id()).apply();

                Log.v(LOG_TAG, "OK RESULT");


            } else {

                Log.v(LOG_TAG, tokenResult.getStatus());

            }


        } else {

            registerOperationRetry++;

            if (registerOperationRetry < 3) {

                call.clone().enqueue(this);

            }

        }

    }

    @Override
    public void onFailure(Call<TokenResult> call, Throwable t) {

        registerOperationRetry++;

        if (registerOperationRetry < 3) {

            call.clone().enqueue(this);

        }


    }
}