package com.gabrielguzman.android.famaex.models;

import android.graphics.Bitmap;

/**
 * Created by jose on 13/04/17.
 */

public class Picture {

    private Bitmap bitmapImage;

    public Picture(Bitmap bitmapImage) {
        this.bitmapImage = bitmapImage;
    }


    public Picture() {
    }

    public Bitmap getBitmapImage() {
        return bitmapImage;
    }

    public void setBitmapImage(Bitmap bitmapImage) {
        this.bitmapImage = bitmapImage;
    }
}
