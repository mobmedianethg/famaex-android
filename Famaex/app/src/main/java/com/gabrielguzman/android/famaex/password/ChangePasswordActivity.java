package com.gabrielguzman.android.famaex.password;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.dashboard.DashboardActivity;
import com.gabrielguzman.android.famaex.login.adapter.RecyclerViewSpecializationAdapter;
import com.gabrielguzman.android.famaex.login.interfaces.RegisterSpecializationsInterface;
import com.gabrielguzman.android.famaex.models.Specialization;
import com.gabrielguzman.android.famaex.password.adapter.RecyclerViewSpecializationUserAdapter;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.UserEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.SpecializationsResponse;
import com.gabrielguzman.android.famaex.web.models.User;
import com.gabrielguzman.android.famaex.web.models.UserSpecializationsRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Allows user to change the password.
 *
 * @author Gabriel Guzman
 * @see AppCompatActivity
 * @since 2016.25.06
 */
public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener, RegisterSpecializationsInterface {

    public static final String LOG_TAG = "RecoverPasswordActivity";
    public static int changePasswordRetry;
    public static int providerSpecializationsRetry;
    public static int userSpecializationsRetry;
    public static int updaterUserSpecializationsRetry;
    private static final int SPECIALIZATION_SERVICE = 0;
    private static final int USER_SPECIALIZATION_SERVICE = 1;
    private static final int CHANGE_PASSWORD_SERVICE = 2;
    private static final int UPDATE_USER_SPECIALIZATIONS_SERVICE = 3;
    public Retrofit retrofit;
    private SharedPreferences mUserPref;
    private TextView mOldPassword;
    private TextView mNewPassword;
    private TextView mValNewPassword;
    private Button mChangePsswButton;
    private Button chSpecializationsButton;
    private RecyclerView activityChangePasswordSpecializationsList;
    private ViewFlipper activityChangePasswordViewFlipper;
    private RecyclerView activityRegisterSpecializationsRecyclerView;
    private TextView activityRegisterSaveTextview;
    private TextView activityChangePasswordDashboardTitle;
    private View mProgressOverlay;
    private TextView mProgressMsg;
    private String mUserEmail;
    private ChangePasswordActivity activity;
    private User userResponse;
    private int currentView;
    private ImageView activityChangePasswordDashboardBack;
    private RecyclerViewSpecializationAdapter recyclerViewSpecializationAdapter;
    private ImageView activityRegisterSpecializationsBack;
    private SpecializationsResponse specializationsResponse;
    private ArrayList<Specialization> specializations;
    private ArrayList<String> specializationSelected;
    private ArrayList<String> newSpecializationsSelected;
    private TextView progressMssg;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayoutManager linearLayoutManagerUser;
    private boolean changeSpecializations;
    private RecyclerViewSpecializationUserAdapter recyclerViewSpecializationUserAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        // Get the shared preferences for the user
        //TODO: IF EMAIL IS CHANGED AT BACKEND AND THE USER DOESN'T LOG OUT IN THE APP, THE CHANGE WILL FAIL
        mUserPref = getSharedPreferences(AppSharedPreferences.SHARED_NAME, MODE_PRIVATE);
        mUserEmail = mUserPref.getString(AppSharedPreferences.PROVIDER_EMAIL, "");

        mOldPassword = (TextView) findViewById(R.id.chPsswOldPassword);
        mNewPassword = (TextView) findViewById(R.id.chPsswNewPassword);
        mValNewPassword = (TextView) findViewById(R.id.chPsswValNewPassword);
        mChangePsswButton = (Button) findViewById(R.id.chPasswChangeButton);
        mProgressOverlay = findViewById(R.id.progress_overlay);
        mProgressMsg = (TextView) findViewById(R.id.progressMssg);
        chSpecializationsButton = (Button) findViewById(R.id.chSpecializationsButton);
        activityChangePasswordSpecializationsList = (RecyclerView) findViewById(R.id.activity_change_password_specializations_list);
        activityChangePasswordViewFlipper = (ViewFlipper) findViewById(R.id.activity_change_password_view_flipper);
        activityRegisterSpecializationsRecyclerView = (RecyclerView) findViewById(R.id.activity_register_specializations_recyclerview);
        activityRegisterSaveTextview = (TextView) findViewById(R.id.register_save_button);
        activityChangePasswordDashboardBack = (ImageView) findViewById(R.id.activity_change_password_dashboard_back);
        activityRegisterSpecializationsBack = (ImageView) findViewById(R.id.activity_register_specializations_back);
        activityChangePasswordDashboardTitle = (TextView) findViewById(R.id.activity_change_password_dashboard_title);

        chSpecializationsButton.setOnClickListener(this);
        activityRegisterSaveTextview.setOnClickListener(this);
        activityChangePasswordDashboardBack.setOnClickListener(this);
        activityRegisterSpecializationsBack.setOnClickListener(this);

        currentView = 0;

        activity = (ChangePasswordActivity) this;
        retrofit = RestClient.getRetrofitInstance();

        linearLayoutManager = new LinearLayoutManager(activity);
        linearLayoutManagerUser = new LinearLayoutManager(activity);


        setUserData();

        boolean admin = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

        if (admin) {

            getSpecializations();

        } else {

            activityChangePasswordDashboardTitle.setVisibility(View.GONE);
            activityChangePasswordSpecializationsList.setVisibility(View.GONE);
            chSpecializationsButton.setVisibility(View.GONE);


        }

        Utils.setPortaitOrientation(activity);
    }


    private void getUserSpecializations() {

        userSpecializationsRetry = 0;
        UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);
        specializationSelected = new ArrayList<String>();

        final User userSpecializations = new User(mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), null, null, null, null, null, null, null, null, null);

        Call<SpecializationsResponse> call = userEndPointInterface.userSpecializations(RestClient.CONTENT_TYPE, userSpecializations);
        call.enqueue(new Callback<SpecializationsResponse>() {
            @Override
            public void onResponse(Call<SpecializationsResponse> call, Response<SpecializationsResponse> response) {


                if (response.body() != null) {

                    specializationsResponse = response.body();

                    if (Utils.verifyOKTAG(specializationsResponse, activity)) {

                        for (String specializationUser : specializationsResponse.getSpecialization()) {

                            int position = 0;


                            for (Specialization specialization : specializations) {


                                if (specializationUser.equals(specialization.getName())) {

                                    specializationSelected.add(specialization.getName());

                                    recyclerViewSpecializationAdapter.updateElements(position);
                                    break;

                                }


                                position++;

                            }


                        }

                        recyclerViewSpecializationUserAdapter = new RecyclerViewSpecializationUserAdapter(specializationSelected, activity.getApplicationContext());
                        activityChangePasswordSpecializationsList.setAdapter(recyclerViewSpecializationUserAdapter);
                        activityChangePasswordSpecializationsList.setLayoutManager(linearLayoutManagerUser);

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

                        chSpecializationsButton.setEnabled(true);
                        activityChangePasswordDashboardBack.setEnabled(true);
                        mChangePsswButton.setEnabled(true);


                    } else {

                        displayError(specializationsResponse.getStatus(), USER_SPECIALIZATION_SERVICE);

                    }


                } else {

                    providerSpecializationsRetry++;

                    if (providerSpecializationsRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.global_network_error), USER_SPECIALIZATION_SERVICE);

                    }

                }

            }

            @Override
            public void onFailure(Call<SpecializationsResponse> call, Throwable t) {

                providerSpecializationsRetry++;

                if (providerSpecializationsRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.global_network_error), USER_SPECIALIZATION_SERVICE);

                }

            }
        });


    }


    private void getSpecializations() {

        chSpecializationsButton.setEnabled(false);
        activityChangePasswordDashboardBack.setEnabled(false);
        mChangePsswButton.setEnabled(false);

        providerSpecializationsRetry = 0;
        UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);

        progressMssg = (TextView) findViewById(R.id.progressMssg);
        progressMssg.setText(activity.getResources().getString(R.string.loading_getting_specializations));

        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        Call<SpecializationsResponse> call = userEndPointInterface.categories(RestClient.CONTENT_TYPE);
        call.enqueue(new Callback<SpecializationsResponse>() {
            @Override
            public void onResponse(Call<SpecializationsResponse> call, Response<SpecializationsResponse> response) {


                if (response.body() != null) {

                    specializationsResponse = response.body();

                    if (Utils.verifyOKTAG(specializationsResponse, activity)) {

                        specializations = new ArrayList<Specialization>();

                        int position = 0;

                        for (String specialization : specializationsResponse.getSpecialization()) {

                            Specialization specializationList;

                            if (position == specializationsResponse.getSpecialization().size()) {

                                specializationList = new Specialization(specialization, false, 0);

                            } else {

                                specializationList = new Specialization(specialization, false, 1);

                            }

                            position++;
                            specializations.add(specializationList);


                        }

                        recyclerViewSpecializationAdapter = new RecyclerViewSpecializationAdapter(specializations, activity.getApplicationContext(), activity);
                        activityRegisterSpecializationsRecyclerView.setAdapter(recyclerViewSpecializationAdapter);
                        activityRegisterSpecializationsRecyclerView.setLayoutManager(linearLayoutManager);

                        getUserSpecializations();


                    } else {

                        displayError(specializationsResponse.getStatus(), SPECIALIZATION_SERVICE);

                    }


                } else {

                    providerSpecializationsRetry++;

                    if (providerSpecializationsRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.global_network_error), SPECIALIZATION_SERVICE);

                    }

                }

            }

            @Override
            public void onFailure(Call<SpecializationsResponse> call, Throwable t) {

                providerSpecializationsRetry++;

                if (providerSpecializationsRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.global_network_error), SPECIALIZATION_SERVICE);

                }

            }
        });


    }

    private Animation inFromRightAnimation() {

        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    private Animation outToLeftAnimation() {
        Animation outtoLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoLeft.setDuration(500);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;
    }

    private Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(500);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }

    private Animation outToRightAnimation() {
        Animation outtoRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoRight.setDuration(500);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;
    }


    /**
     * Sets the header data for the user
     *
     * @since 2016.12.07
     */
    public void setUserData() {

        TextView userAndCo = (TextView) findViewById(R.id.chPsswUserAndCo);
        TextView userType = (TextView) findViewById(R.id.chPsswUserRole);
        TextView chPsswUserCo = (TextView) findViewById(R.id.chPsswUserCo);

        //TODO: MISSING COMPANY NAME

        if (userAndCo != null) {
            userAndCo.setText(mUserPref.getString(AppSharedPreferences.PROVIDER_NAME, activity.getResources().getString(R.string.global_no_name_user)));
        }

        if (chPsswUserCo != null) {
            chPsswUserCo.setText(mUserPref.getString(AppSharedPreferences.PROVIDER_COMPANY, activity.getResources().getString(R.string.global_no_company_user)));
        }


        if (userType != null) {
            userType.setText(mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false) ? activity.getResources().getString(R.string.global_admin_user) : activity.getResources().getString(R.string.global_normal_user));
        }

    }

    /**
     * Attempts to change the user's password
     * If there are form errors the errors are
     * presented and no actual change attempt is made.
     */
    public void attemptPsswChange(View v) {

        // Reset errors.
        mOldPassword.setError(null);
        mNewPassword.setError(null);
        mValNewPassword.setError(null);

        // Store values at the time of the login attempt.
        String oldPssw = mOldPassword.getText().toString();
        String newPssw = mNewPassword.getText().toString();
        String valNewPssw = mValNewPassword.getText().toString();


        boolean cancel = false;
        View focusView = null;

        if (newPssw.length() < 6) {
            mNewPassword.setError(getString(R.string.errorShortPssw));
            focusView = mNewPassword;
            cancel = true;
        }
        if (!valNewPssw.equals(newPssw)) {
            mNewPassword.setError(getString(R.string.errorPsswsDontMatch));
            focusView = mNewPassword;
            cancel = true;
        }
        // Check for valid passwords
        if (TextUtils.isEmpty(valNewPssw)) {
            mValNewPassword.setError(getString(R.string.errorFieldRequired));
            focusView = mValNewPassword;
            cancel = true;
        }

        if (TextUtils.isEmpty(newPssw)) {
            mNewPassword.setError(getString(R.string.errorFieldRequired));
            focusView = mNewPassword;
            cancel = true;
        }

        if (TextUtils.isEmpty(oldPssw)) {
            mOldPassword.setError(getString(R.string.errorFieldRequired));
            focusView = mOldPassword;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt password change and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mChangePsswButton.setEnabled(false);

            mProgressMsg.setText(R.string.chPsswProgMsg);

            Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

            UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);

            User userChange = new User(null, mUserEmail, null, null, null, null, null, null, oldPssw, newPssw);

            Call<User> call = userEndPointInterface.changePassword(RestClient.CONTENT_TYPE, userChange);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    if (response.body() != null) {

                        userResponse = response.body();

                        if (Utils.verifyOKTAG(userResponse, Utils.CHANGE_USER, activity)) {

                            Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                            Toast.makeText(getBaseContext(), activity.getResources().getString(R.string.change_password_success), Toast.LENGTH_SHORT).show();

                            mChangePsswButton.setEnabled(true);
                            mOldPassword.setText("");
                            mNewPassword.setText("");
                            mValNewPassword.setText("");


                        } else {

                            displayError(userResponse.getStatus(), CHANGE_PASSWORD_SERVICE);

                        }


                    } else {

                        changePasswordRetry++;

                        if (changePasswordRetry < 3) {

                            call.clone().enqueue(this);

                        } else {

                            displayError(activity.getResources().getString(R.string.change_password_error_message), CHANGE_PASSWORD_SERVICE);

                        }

                    }

                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {

                    changePasswordRetry++;

                    if (changePasswordRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.change_password_error_message), CHANGE_PASSWORD_SERVICE);

                    }

                }

            });
        }
    }

    private void displayError(String message, int option) {


        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

        switch (option) {

            case (CHANGE_PASSWORD_SERVICE):

                mChangePsswButton.setEnabled(true);

                break;

            case (SPECIALIZATION_SERVICE):

                chSpecializationsButton.setEnabled(true);
                activityChangePasswordDashboardBack.setEnabled(true);
                mChangePsswButton.setEnabled(true);

                break;

            case (USER_SPECIALIZATION_SERVICE):

                chSpecializationsButton.setEnabled(true);
                activityChangePasswordDashboardBack.setEnabled(true);
                mChangePsswButton.setEnabled(true);

                break;

            case (UPDATE_USER_SPECIALIZATIONS_SERVICE):

                break;


        }


    }

    public void updateSpecializations() {

        int selected = 0;
        newSpecializationsSelected = new ArrayList<String>();

        for (Specialization specialization : specializations) {


            if (specialization.getSelected()) {

                Log.v(LOG_TAG, specialization.getName());

                selected = 1;
                newSpecializationsSelected.add(specialization.getName());

            }

        }


        if (selected == 1) {

            activityRegisterSpecializationsBack.setEnabled(false);
            activityRegisterSaveTextview.setEnabled(false);

            updaterUserSpecializationsRetry = 0;
            UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);

            progressMssg = (TextView) findViewById(R.id.progressMssg);
            progressMssg.setText(activity.getResources().getString(R.string.loading_updating_specializations));

            UserSpecializationsRequest userSpecializationsRequest = new UserSpecializationsRequest(mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), newSpecializationsSelected);

            Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

            Call<SpecializationsResponse> call = userEndPointInterface.updateUserSpecializations(RestClient.CONTENT_TYPE, userSpecializationsRequest);
            call.enqueue(new Callback<SpecializationsResponse>() {
                @Override
                public void onResponse(Call<SpecializationsResponse> call, Response<SpecializationsResponse> response) {

                    if (response.body() != null) {

                        specializationsResponse = response.body();

                        if (Utils.verifyOKTAG(specializationsResponse, activity)) {

                            specializationSelected = newSpecializationsSelected;

                            activityChangePasswordViewFlipper.setInAnimation(inFromLeftAnimation());
                            activityChangePasswordViewFlipper.setOutAnimation(outToRightAnimation());
                            activityChangePasswordViewFlipper.setDisplayedChild(0);

                            currentView = 0;


                            recyclerViewSpecializationUserAdapter.updateElements(specializationSelected);

                            activityRegisterSpecializationsBack.setEnabled(true);
                            activityRegisterSaveTextview.setEnabled(true);

                            Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);


                        } else {

                            displayError(specializationsResponse.getStatus(), SPECIALIZATION_SERVICE);

                        }


                    } else {

                        updaterUserSpecializationsRetry++;

                        if (updaterUserSpecializationsRetry < 3) {

                            call.clone().enqueue(this);

                        } else {

                            displayError(activity.getResources().getString(R.string.global_network_error), SPECIALIZATION_SERVICE);

                        }

                    }

                }

                @Override
                public void onFailure(Call<SpecializationsResponse> call, Throwable t) {

                    updaterUserSpecializationsRetry++;

                    if (updaterUserSpecializationsRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.update_user_specializations_error_message), UPDATE_USER_SPECIALIZATIONS_SERVICE);

                    }


                }


            });


        } else

        {

            Toast.makeText(getBaseContext(), activity.getResources().getString(R.string.no_specializations_selected_failure), Toast.LENGTH_SHORT).show();

        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case (R.id.chSpecializationsButton):

                if (specializations != null) {

                    activityChangePasswordViewFlipper.setInAnimation(inFromRightAnimation());
                    activityChangePasswordViewFlipper.setOutAnimation(outToLeftAnimation());

                    activityChangePasswordViewFlipper.showNext();

                    currentView = 1;

                } else {

                    getSpecializations();

                }


                break;


            case (R.id.register_save_button):

                if (changeSpecializations == true) {

                    updateSpecializations();

                } else {

                    activityChangePasswordViewFlipper.setInAnimation(inFromLeftAnimation());
                    activityChangePasswordViewFlipper.setOutAnimation(outToRightAnimation());
                    activityChangePasswordViewFlipper.setDisplayedChild(0);

                    currentView = 0;

                    Toast.makeText(getBaseContext(), activity.getResources().getString(R.string.no_specializations_update_text), Toast.LENGTH_SHORT).show();

                }

                break;

            case (R.id.activity_change_password_dashboard_back):

                Intent dashboardIntent = new Intent(ChangePasswordActivity.this, DashboardActivity.class);
                startActivity(dashboardIntent);

                break;


            case (R.id.activity_register_specializations_back):

                if (changeSpecializations == false) {

                    activityChangePasswordViewFlipper.setInAnimation(inFromLeftAnimation());
                    activityChangePasswordViewFlipper.setOutAnimation(outToRightAnimation());
                    activityChangePasswordViewFlipper.setDisplayedChild(0);

                    currentView = 0;

                } else {

                    Toast.makeText(getBaseContext(), activity.getResources().getString(R.string.activity_register_save_changes), Toast.LENGTH_SHORT).show();

                }

                break;

        }

    }

    @Override
    public void onSpecializationSelect(View view, int position) {

        changeSpecializations = true;

        recyclerViewSpecializationAdapter.updateElements(position);

    }
}
