package com.gabrielguzman.android.famaex.status.interfaces;

import android.view.View;

/**
 * Created by jose on 13/04/17.
 */

public interface EditPhotoStatusChangeInterface {

    public void onEditPhoto(View view, int position);


}
