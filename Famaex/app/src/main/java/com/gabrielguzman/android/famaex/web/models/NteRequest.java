package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 19/05/17.
 */

public class NteRequest {

    private Integer service_request_id;
    private Integer hours_to_work;
    private Integer quantity_of_technicals;
    private Integer quantity_of_assistants;
    private Integer material_costs_estimated;


    public NteRequest() {
    }

    public NteRequest(Integer service_request_id, Integer hours_to_work, Integer quantity_of_technicals, Integer quantity_of_assistants, Integer material_costs_estimated) {
        this.service_request_id = service_request_id;
        this.hours_to_work = hours_to_work;
        this.quantity_of_technicals = quantity_of_technicals;
        this.quantity_of_assistants = quantity_of_assistants;
        this.material_costs_estimated = material_costs_estimated;
    }

    public Integer getService_request_id() {
        return service_request_id;
    }

    public void setService_request_id(Integer service_request_id) {
        this.service_request_id = service_request_id;
    }

    public Integer getHours_to_work() {
        return hours_to_work;
    }

    public void setHours_to_work(Integer hours_to_work) {
        this.hours_to_work = hours_to_work;
    }

    public Integer getQuantity_of_technicals() {
        return quantity_of_technicals;
    }

    public void setQuantity_of_technicals(Integer quantity_of_technicals) {
        this.quantity_of_technicals = quantity_of_technicals;
    }

    public Integer getQuantity_of_assistants() {
        return quantity_of_assistants;
    }

    public void setQuantity_of_assistants(Integer quantity_of_assistants) {
        this.quantity_of_assistants = quantity_of_assistants;
    }

    public Integer getMaterial_costs_estimated() {
        return material_costs_estimated;
    }

    public void setMaterial_costs_estimated(Integer material_costs_estimated) {
        this.material_costs_estimated = material_costs_estimated;
    }
}
