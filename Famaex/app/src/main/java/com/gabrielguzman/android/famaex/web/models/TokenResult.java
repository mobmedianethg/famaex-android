package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 31/03/17.
 */

public class TokenResult {

    private String status;


    public TokenResult(String status) {
        this.status = status;
    }

    public TokenResult() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
