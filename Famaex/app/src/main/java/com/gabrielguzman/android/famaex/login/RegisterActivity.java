package com.gabrielguzman.android.famaex.login;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.login.adapter.RecyclerViewSpecializationAdapter;
import com.gabrielguzman.android.famaex.login.interfaces.RegisterSpecializationsInterface;
import com.gabrielguzman.android.famaex.models.Specialization;
import com.gabrielguzman.android.famaex.utils.Famaex;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.UserEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.SpecializationsResponse;
import com.gabrielguzman.android.famaex.web.models.UserRegister;
import com.gabrielguzman.android.famaex.web.models.UserRegisterResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, RegisterSpecializationsInterface {

    public static final String LOG_TAG = "RegisterActivity";
    public static int registerRetry;
    public static int providerSpecializationsRetry;
    private static final int SPECIALIZATION_SERVICE = 0;
    private static final int REGISTER_SERVICE = 1;
    private Retrofit retrofit;
    private View mProgressOverlay;
    private RegisterActivity activity;
    private ImageView activityRegisterBackFirstForm;
    private ImageView activityRegisterBackSecondForm;
    private ImageView activityRegisterBackConfiguration;
    private ViewFlipper activityRegisterViewFlipper;
    private EditText activityRegisterEmailEdittext;
    private EditText activityRegisterNameEdittext;
    private EditText activityRegisterPasswordEdittext;
    private EditText activityRegisterPhoneEdittext;
    private EditText activityRegisterStreetEdittext;
    private EditText activityRegisterNumberEdittext;
    private EditText activityRegisterPostalCodeEdittext;
    private EditText activityRegisterStateEdittext;
    private EditText activityRegisterCountryEdittext;
    private TextView activityRegisterNextTextview;
    private TextView activityRegisterSpecializationTextview;
    private EditText activityRegisterCifEdittext;
    private EditText activityRegisterCompanyName;
    private EditText activityRegisterEmployeesEdittext;
    private EditText activityRegisterIndependientEmployeesEdittext;
    private TextView actvitityRegisterSendTextview;
    private RecyclerView activityRegisterSpecializationsRecyclerView;
    private TextView activityRegisterSaveTextview;
    private EditText activityRegisterNifEdittext;
    private int currentView;
    private SpecializationsResponse specializationsResponse;
    private UserRegisterResponse userRegisterResponse;
    private ArrayList<Specialization> specializations = null;
    private LinearLayoutManager linearLayoutManager;
    private TextView progressMssg;
    private RecyclerViewSpecializationAdapter recyclerViewSpecializationAdapter;
    private ArrayList<String> specializationSelected;
    private boolean changeSpecializations;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        activity = this;
        retrofit = RestClient.getRetrofitInstance();

        linearLayoutManager = new LinearLayoutManager(activity);

        activityRegisterBackFirstForm = (ImageView) findViewById(R.id.activity_register_first_back);
        activityRegisterBackSecondForm = (ImageView) findViewById(R.id.activity_register_second_back);
        activityRegisterBackConfiguration = (ImageView) findViewById(R.id.activity_register_specializations_back);
        activityRegisterViewFlipper = (ViewFlipper) findViewById(R.id.activity_register_view_flipper);
        activityRegisterEmailEdittext = (EditText) findViewById(R.id.activity_register_email_edittext);
        activityRegisterNameEdittext = (EditText) findViewById(R.id.activity_register_name_edittext);
        activityRegisterPasswordEdittext = (EditText) findViewById(R.id.activity_register_password_edittext);
        activityRegisterPhoneEdittext = (EditText) findViewById(R.id.activity_register_phone_edittext);
        activityRegisterStreetEdittext = (EditText) findViewById(R.id.activity_register_street_edittext);
        activityRegisterNumberEdittext = (EditText) findViewById(R.id.activity_register_number_edittext);
        activityRegisterPostalCodeEdittext = (EditText) findViewById(R.id.activity_register_postal_code_edittext);
        activityRegisterStateEdittext = (EditText) findViewById(R.id.activity_register_state_edittext);
        activityRegisterCountryEdittext = (EditText) findViewById(R.id.activity_register_country_edittext);
        activityRegisterNextTextview = (TextView) findViewById(R.id.register_next_button);
        activityRegisterSpecializationTextview = (TextView) findViewById(R.id.activity_register_specialization_textview);
        activityRegisterCifEdittext = (EditText) findViewById(R.id.activity_register_cif_edittext);
        activityRegisterCompanyName = (EditText) findViewById(R.id.activity_register_company_name_edittext);
        activityRegisterEmployeesEdittext = (EditText) findViewById(R.id.activity_register_employees_edittext);
        activityRegisterNifEdittext = (EditText) findViewById(R.id.activity_register_nif_edittext);
        activityRegisterIndependientEmployeesEdittext = (EditText) findViewById(R.id.activity_register_independient_employees_edittext);
        actvitityRegisterSendTextview = (TextView) findViewById(R.id.register_send_button);
        activityRegisterSpecializationsRecyclerView = (RecyclerView) findViewById(R.id.activity_register_specializations_recyclerview);
        activityRegisterSaveTextview = (TextView) findViewById(R.id.register_save_button);
        mProgressOverlay = findViewById(R.id.progress_overlay);

        currentView = 0;

        activityRegisterNextTextview.setOnClickListener(this);
        activityRegisterSpecializationTextview.setOnClickListener(this);
        actvitityRegisterSendTextview.setOnClickListener(this);
        activityRegisterSaveTextview.setOnClickListener(this);
        activityRegisterBackFirstForm.setOnClickListener(this);
        activityRegisterBackSecondForm.setOnClickListener(this);
        activityRegisterBackConfiguration.setOnClickListener(this);

        Utils.setPortaitOrientation(activity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorPrimary));

        }

        getSpecializations();


    }

    private void registerUser() {

        activityRegisterSpecializationTextview.setEnabled(false);
        actvitityRegisterSendTextview.setEnabled(false);
        activityRegisterBackSecondForm.setEnabled(false);

        registerRetry = 0;
        UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);

        progressMssg = (TextView) findViewById(R.id.progressMssg);
        progressMssg.setText(activity.getResources().getString(R.string.loading_register_user));

        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        ArrayList<Object> company = new ArrayList<Object>();
        ArrayList<String> address = new ArrayList<String>();

        company.add(activityRegisterCifEdittext.getText().toString());
        company.add(activityRegisterCompanyName.getText().toString());
        company.add(new Integer(Integer.parseInt(activityRegisterEmployeesEdittext.getText().toString())));
        company.add(new Integer(Integer.parseInt(activityRegisterIndependientEmployeesEdittext.getText().toString())));
        company.add(new Boolean(false));
        company.add(new Boolean(false));

        address.add(activityRegisterStreetEdittext.getText().toString());
        address.add(activityRegisterNumberEdittext.getText().toString());
        address.add(activityRegisterPostalCodeEdittext.getText().toString());
        address.add(activityRegisterStateEdittext.getText().toString());
        address.add(activityRegisterCountryEdittext.getText().toString());

        UserRegister userRegister = new UserRegister(true, activityRegisterNameEdittext.getText().toString(), activityRegisterEmailEdittext.getText().toString(), activityRegisterPasswordEdittext.getText().toString(), activityRegisterPhoneEdittext.getText().toString(), company, address, activityRegisterNifEdittext.getText().toString(), specializationSelected);

        Call<UserRegisterResponse> call = userEndPointInterface.registerUser(RestClient.CONTENT_TYPE, userRegister);
        call.enqueue(new Callback<UserRegisterResponse>() {
            @Override
            public void onResponse(Call<UserRegisterResponse> call, Response<UserRegisterResponse> response) {


                if (response.body() != null) {

                    userRegisterResponse = response.body();

                    if (Utils.verifyOKTAG(userRegisterResponse, activity)) {

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

                        Toast.makeText(getBaseContext(), activity.getResources().getString(R.string.activity_register_success), Toast.LENGTH_SHORT).show();

                        finish();


                    } else {

                        displayError(specializationsResponse.getStatus(), SPECIALIZATION_SERVICE);

                    }


                } else {

                    registerRetry++;

                    if (registerRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.global_network_error), REGISTER_SERVICE);

                    }

                }

            }

            @Override
            public void onFailure(Call<UserRegisterResponse> call, Throwable t) {

                registerRetry++;

                if (registerRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.global_network_error), REGISTER_SERVICE);

                }

            }
        });

    }


    private void getSpecializations() {

        activityRegisterNextTextview.setEnabled(false);
        activityRegisterSpecializationTextview.setEnabled(false);
        actvitityRegisterSendTextview.setEnabled(false);
        activityRegisterBackFirstForm.setEnabled(false);
        activityRegisterBackSecondForm.setEnabled(false);

        providerSpecializationsRetry = 0;
        UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);

        progressMssg = (TextView) findViewById(R.id.progressMssg);
        progressMssg.setText(activity.getResources().getString(R.string.loading_getting_specializations));

        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        Call<SpecializationsResponse> call = userEndPointInterface.categories(RestClient.CONTENT_TYPE);
        call.enqueue(new Callback<SpecializationsResponse>() {
            @Override
            public void onResponse(Call<SpecializationsResponse> call, Response<SpecializationsResponse> response) {


                if (response.body() != null) {

                    specializationsResponse = response.body();

                    if (Utils.verifyOKTAG(specializationsResponse, activity)) {

                        specializations = new ArrayList<Specialization>();

                        int position = 0;

                        for (String specialization : specializationsResponse.getSpecialization()) {

                            Specialization specializationList;

                            if (position == specializationsResponse.getSpecialization().size()) {

                                specializationList = new Specialization(specialization, false, 0);

                            } else {

                                specializationList = new Specialization(specialization, false, 1);

                            }

                            position++;
                            specializations.add(specializationList);


                        }

                        activityRegisterNextTextview.setEnabled(true);
                        activityRegisterSpecializationTextview.setEnabled(true);
                        actvitityRegisterSendTextview.setEnabled(true);
                        activityRegisterBackFirstForm.setEnabled(true);
                        activityRegisterBackSecondForm.setEnabled(true);

                        recyclerViewSpecializationAdapter = new RecyclerViewSpecializationAdapter(specializations, activity.getApplicationContext(), activity);
                        activityRegisterSpecializationsRecyclerView.setAdapter(recyclerViewSpecializationAdapter);
                        activityRegisterSpecializationsRecyclerView.setLayoutManager(linearLayoutManager);

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);


                    } else {

                        displayError(specializationsResponse.getStatus(), SPECIALIZATION_SERVICE);

                    }


                } else {

                    providerSpecializationsRetry++;

                    if (providerSpecializationsRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.global_network_error), SPECIALIZATION_SERVICE);

                    }

                }

            }

            @Override
            public void onFailure(Call<SpecializationsResponse> call, Throwable t) {

                providerSpecializationsRetry++;

                if (providerSpecializationsRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.global_network_error), SPECIALIZATION_SERVICE);

                }

            }
        });


    }


    private void displayError(String message, int option) {

        if (option == SPECIALIZATION_SERVICE) {

            Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
            Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

            activityRegisterNextTextview.setEnabled(true);
            activityRegisterSpecializationTextview.setEnabled(true);
            actvitityRegisterSendTextview.setEnabled(true);
            activityRegisterBackFirstForm.setEnabled(true);
            activityRegisterBackSecondForm.setEnabled(true);

        } else {

            Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
            Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

            activityRegisterSpecializationTextview.setEnabled(true);
            actvitityRegisterSendTextview.setEnabled(true);
            activityRegisterBackSecondForm.setEnabled(true);


        }


    }


    private Animation inFromRightAnimation() {

        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    private Animation outToLeftAnimation() {
        Animation outtoLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoLeft.setDuration(500);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;
    }

    private Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(500);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }

    private Animation outToRightAnimation() {
        Animation outtoRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoRight.setDuration(500);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;
    }


    @Override
    public void onBackPressed() {

        switch (currentView) {

            case (0):

                Intent registerIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(registerIntent);

                break;

            case (1):

                activityRegisterViewFlipper.setInAnimation(inFromLeftAnimation());
                activityRegisterViewFlipper.setOutAnimation(outToRightAnimation());
                activityRegisterViewFlipper.setDisplayedChild(0);

                currentView = 0;

                break;

            case (2):

                activityRegisterViewFlipper.setInAnimation(inFromLeftAnimation());
                activityRegisterViewFlipper.setOutAnimation(outToRightAnimation());
                activityRegisterViewFlipper.setDisplayedChild(1);

                currentView = 1;

                break;

        }


    }


    private boolean validateFirstForm() {

        boolean result = true;

        activityRegisterEmailEdittext.setError(null);
        activityRegisterNameEdittext.setError(null);
        activityRegisterPasswordEdittext.setError(null);
        activityRegisterPhoneEdittext.setError(null);
        activityRegisterStreetEdittext.setError(null);
        activityRegisterNumberEdittext.setError(null);
        activityRegisterPostalCodeEdittext.setError(null);
        activityRegisterStateEdittext.setError(null);
        activityRegisterCountryEdittext.setError(null);
        activityRegisterNifEdittext.setError(null);

        if (activityRegisterEmailEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterEmailEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.verifyEmail(activityRegisterEmailEdittext.getText().toString())) {

            result = false;
            activityRegisterEmailEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (activityRegisterNameEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterNameEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.noContainsNumbers(activityRegisterNameEdittext.getText().toString())) {

            result = false;
            activityRegisterNameEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (activityRegisterPasswordEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterPasswordEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (activityRegisterPasswordEdittext.getText().toString().length() < 6) {

            result = false;
            activityRegisterPasswordEdittext.setError(activity.getResources().getString(R.string.invalid_password_lenght));

        } else if (activityRegisterStreetEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterStreetEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (activityRegisterNumberEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterNumberEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.containsOnlyNumbers(activityRegisterNumberEdittext.getText().toString())) {

            result = false;
            activityRegisterNumberEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));


        } else if (activityRegisterNifEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterNifEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.containsOnlyNumbers(activityRegisterNifEdittext.getText().toString())) {

            result = false;
            activityRegisterNifEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (activityRegisterPostalCodeEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterPostalCodeEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.containsOnlyNumbers(activityRegisterPostalCodeEdittext.getText().toString())) {

            result = false;
            activityRegisterPostalCodeEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (activityRegisterStateEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterStateEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.noContainsNumbers(activityRegisterStateEdittext.getText().toString())) {

            result = false;
            activityRegisterStateEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (activityRegisterCountryEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterCountryEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.noContainsNumbers(activityRegisterCountryEdittext.getText().toString())) {

            result = false;
            activityRegisterCountryEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        }


        return result;


    }


    public boolean validateSecondForm() {

        boolean result = true;

        activityRegisterSpecializationTextview.setError(null);
        activityRegisterCifEdittext.setError(null);
        activityRegisterCompanyName.setError(null);
        activityRegisterEmployeesEdittext.setError(null);
        activityRegisterIndependientEmployeesEdittext.setError(null);

        if (specializationSelected == null) {

            result = false;
            activityRegisterSpecializationTextview.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (specializationSelected.size() == 0) {

            result = false;
            activityRegisterSpecializationTextview.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (activityRegisterCifEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterCifEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (activityRegisterCompanyName.getText().toString().length() == 0) {

            result = false;
            activityRegisterCompanyName.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (activityRegisterEmployeesEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterEmployeesEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.containsOnlyNumbers(activityRegisterEmployeesEdittext.getText().toString())) {

            result = false;
            activityRegisterEmployeesEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (activityRegisterIndependientEmployeesEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterIndependientEmployeesEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.containsOnlyNumbers(activityRegisterIndependientEmployeesEdittext.getText().toString())) {

            result = false;
            activityRegisterIndependientEmployeesEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        }


        return result;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case (R.id.activity_register_first_back):

                Intent registerIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(registerIntent);

                break;

            case (R.id.activity_register_second_back):

                activityRegisterViewFlipper.setInAnimation(inFromLeftAnimation());
                activityRegisterViewFlipper.setOutAnimation(outToRightAnimation());
                activityRegisterViewFlipper.setDisplayedChild(0);

                currentView = 0;

                break;

            case (R.id.activity_register_specializations_back):

                if (changeSpecializations == false) {

                    activityRegisterViewFlipper.setInAnimation(inFromLeftAnimation());
                    activityRegisterViewFlipper.setOutAnimation(outToRightAnimation());
                    activityRegisterViewFlipper.setDisplayedChild(1);

                    currentView = 1;

                } else {

                    Toast.makeText(getBaseContext(), activity.getResources().getString(R.string.activity_register_save_changes), Toast.LENGTH_SHORT).show();

                }

                break;

            case (R.id.activity_register_specialization_textview):

                if (specializations != null) {

                    changeSpecializations = false;

                    Utils.hideSoftKeyboard(activity);

                    activityRegisterViewFlipper.setInAnimation(inFromRightAnimation());
                    activityRegisterViewFlipper.setOutAnimation(outToLeftAnimation());

                    activityRegisterViewFlipper.showNext();

                    currentView = 2;

                } else {

                    getSpecializations();

                }

                break;

            case (R.id.register_send_button):

                if (validateSecondForm()) {

                    Utils.hideSoftKeyboard(activity);

                    registerUser();

                }

                break;

            case (R.id.register_save_button):

                specializationSelected = new ArrayList<String>();

                for (Specialization specialization : specializations) {


                    if (specialization.getSelected() == true) {

                        specializationSelected.add(specialization.getName());

                    }

                }

                changeSpecializations = false;

                activityRegisterViewFlipper.setInAnimation(inFromLeftAnimation());
                activityRegisterViewFlipper.setOutAnimation(outToRightAnimation());
                activityRegisterViewFlipper.setDisplayedChild(1);

                currentView = 1;

                break;


            case (R.id.register_next_button):

                if (validateFirstForm()) {

                    Utils.hideSoftKeyboard(activity);

                    activityRegisterViewFlipper.setInAnimation(inFromRightAnimation());
                    activityRegisterViewFlipper.setOutAnimation(outToLeftAnimation());

                    activityRegisterViewFlipper.showNext();

                    currentView = 1;

                }

                break;


        }

    }

    @Override
    public void onSpecializationSelect(View view, int position) {

        changeSpecializations = true;

        recyclerViewSpecializationAdapter.updateElements(position);

    }
}
