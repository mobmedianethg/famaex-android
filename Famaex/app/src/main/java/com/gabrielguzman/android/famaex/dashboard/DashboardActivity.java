package com.gabrielguzman.android.famaex.dashboard;

import android.Manifest;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.about.AboutActivity;
import com.gabrielguzman.android.famaex.gcm.GCMRegistrationIntentService;
import com.gabrielguzman.android.famaex.login.LoginActivity;
import com.gabrielguzman.android.famaex.models.DateService;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.notification.NotificationDetailFragment;
import com.gabrielguzman.android.famaex.password.ChangePasswordActivity;
import com.gabrielguzman.android.famaex.services.available.AvailableServicesActivity;
import com.gabrielguzman.android.famaex.services.available.AvailableServicesCardAdapter;
import com.gabrielguzman.android.famaex.services.available.interfaces.UsersProvidersInterface;
import com.gabrielguzman.android.famaex.services.finished.FinishedServicesActivity;
import com.gabrielguzman.android.famaex.services.finished.FinishedServicesCardAdapter;
import com.gabrielguzman.android.famaex.services.won.WonServicesActivity;
import com.gabrielguzman.android.famaex.services.won.WonServicesCardAdapter;
import com.gabrielguzman.android.famaex.status.StsChangeQuestionFragment;
import com.gabrielguzman.android.famaex.users.UserActivity;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.ServiceEndPointInterface;
import com.gabrielguzman.android.famaex.web.endpoints.TokenEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.Approved;
import com.gabrielguzman.android.famaex.web.models.Data;
import com.gabrielguzman.android.famaex.web.models.DataApproved;
import com.gabrielguzman.android.famaex.web.models.DataHistory;
import com.gabrielguzman.android.famaex.web.models.DataUsersProviders;
import com.gabrielguzman.android.famaex.web.models.History;
import com.gabrielguzman.android.famaex.web.models.Token;
import com.gabrielguzman.android.famaex.web.models.TokenResult;
import com.gabrielguzman.android.famaex.web.models.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Dashboard screen that shows the user a condensed view of the available,
 * won and finished services, as well as the average rating received.
 *
 * @author Gabriel Guzman
 * @see AppCompatActivity
 * @since 2016.23.06
 */
public class DashboardActivity extends AppCompatActivity
        implements AvailableServicesCardAdapter.UpdateCallback,
        WonServicesCardAdapter.UpdateCallback, NotificationDetailFragment.UpdateCallback {

    public static final String LOG_TAG = "DashboardActivity";
    private static final int NONE_SERVICE = 0;
    private static final int LOGOUT_SERVICE = 1;
    private static final int AVAILABLE_SERVICES = 2;
    private static final int HISTORY_SERVICES = 3;
    private static final int WON_SERVICES = 4;
    private static final int USERS_PROVIDERS = 5;
    private static final int USERS_PROVIDERS_SPINNER = 6;
    private static final int USERS_PROVIDERS_SILENT = 7;
    public static final int MANY_PERMISSIONS_REQUEST = 5000;
    public static int logoutUserRetry;
    public static int availableServicesRetry;
    public static int historyServiceRetry;
    public static int wonServiceRetry;
    private static int getUsersProvidersRetry;
    public Retrofit retrofit;
    private SharedPreferences mUserPref;
    private View mProgressOverlay;
    private View mProgressOverlay2;
    private View mProgressOverlay3;
    private View mProgressOverlay4;
    private View mProgressOverlayClose;
    private TextView progressMssg;
    private int avServLength;
    private int compServLength;
    private int compServPrice;
    private int rtQty;
    private float rtPoints;
    private ArrayList<Service> avServList;
    private ArrayList<Service> avServCardList;
    private ArrayList<Service> wnServList;
    private ArrayList<Service> wnServCardList;
    private ArrayList<Service> compServList;
    private ArrayList<Service> compServCardList;
    private ArrayList<DataUsersProviders> usersProviders;
    private DashboardActivity activity;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private int currentService;
    private TextView avServTotalQty;
    private Service serviceDetail;
    public int resume;
    private AvailableServicesCardAdapter asca;
    private boolean visibilityTariff;
    private MenuItem actionRefresh;
    private MenuItem actionAbout;
    private MenuItem actionSettings;
    private MenuItem actionUsers;
    private MenuItem actionLogout;
    private static final String startTimeDefault = "07:00 AM";
    private static final String endTimeDefault = "09:00 PM";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        // Get the shared preferences for the user
        mUserPref = getSharedPreferences(AppSharedPreferences.SHARED_NAME, MODE_PRIVATE);
        activity = (DashboardActivity) this;
        retrofit = RestClient.getRetrofitInstance();

        avServTotalQty = (TextView) findViewById(R.id.avServTotalQty);

        usersProviders = new ArrayList<DataUsersProviders>();

        setUserData();

        resume = 0;

        mProgressOverlayClose = findViewById(R.id.progress_overlay);
        progressMssg = (TextView) findViewById(R.id.progressMssg);

        registerGCMToken();

        //Set the toolbar
        Toolbar famaexToolbar = (Toolbar) findViewById(R.id.famaexToolbar);
        setSupportActionBar(famaexToolbar);
        ActionBar sab = getSupportActionBar();

        if (sab != null) {
            sab.setDisplayShowHomeEnabled(true);
            sab.setDisplayUseLogoEnabled(true);
            sab.setIcon(R.drawable.padded_logo);
        }

        Utils.setPortaitOrientation(activity);

        updateData();


        //--------------------notification
        if (getIntent().hasExtra("com.gabrielguzman.android.famaex.Message")) {
            showEditDialog(getIntent().getBundleExtra("com.gabrielguzman.android.famaex.Message"),
                    getIntent().getIntExtra("com.gabrielguzman.android.famaex.Srid", 37));
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Check type of intent filter
                if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)) {
                    //Registration success
                    String token = intent.getStringExtra("token");
                    //Toast.makeText(getApplicationContext(), "GCM token:" + token, Toast.LENGTH_LONG).show();
                } else if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)) {
                    //Registration error
                    Toast.makeText(getApplicationContext(), "Error al registrar en GCM", Toast.LENGTH_LONG).show();
/*                } else {
                    //Tobe define*/
                }
            }
        };
    }


    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
        //--------------------notification
        if (getIntent().hasExtra("message")) {
            showEditDialog(getIntent().getBundleExtra("message"), getIntent().getIntExtra("srid", 0));
        }


        updateData();

    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    private void setAvailableServices() {

        mProgressOverlay = findViewById(R.id.actServprogressBar);
        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        avServList = new ArrayList<>();
        avServCardList = new ArrayList<>();

        availableServicesRetry = 0;

        ServiceEndPointInterface serviceAvailableEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

        final User userAvailable = new User(mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), null, null, null, null, null, null, null, null, null);

        Call<com.gabrielguzman.android.famaex.web.models.Service> callAvailable = serviceAvailableEndPointInterface.availableServices(RestClient.CONTENT_TYPE, userAvailable);
        callAvailable.enqueue(new Callback<com.gabrielguzman.android.famaex.web.models.Service>() {
            @Override
            public void onResponse(Call<com.gabrielguzman.android.famaex.web.models.Service> call, Response<com.gabrielguzman.android.famaex.web.models.Service> response) {

                com.gabrielguzman.android.famaex.web.models.Service servicesAvailable;

                if (response.body() != null) {

                    servicesAvailable = response.body();

                    if (Utils.verifyOKTAG(servicesAvailable, activity.getApplicationContext())) {

                        avServTotalQty.setText(String.valueOf(servicesAvailable.getData().size()));
                        int lenght = 0;

                        for (Data data : servicesAvailable.getData()) {

                            Service service = new Service();
                            service.setServiceRqID(data.getService_request_id());
                            service.setServiceName(data.getService());
                            service.setSubService(data.getSubservice());


                            if (data.getNte_provider() != null) {

                                if (data.getNte_provider().intValue() != 0) {
                                    service.setNteProvider(data.getNte_provider().intValue());
                                } else {
                                    service.setNteProvider(0);
                                }

                            } else {

                                service.setNteProvider(0);

                            }

                            if (data.getService_per_mts2().equals(Utils.HOURS_TAG)) {

                                service.setPrice(data.getTotal_price_mt2().intValue());
                                service.setTypePrice(true);

                            } else {

                                service.setPrice(data.getTechnical_hour_price().intValue());
                                service.setTypePrice(false);

                            }

                            if (data.getUrgent()) {

                                service.setPriority(Utils.URGENT_TAG);

                            } else {

                                service.setPriority("");

                            }


                            if (data.getComment_client() != null) {

                                service.setComments(data.getComment_client());


                            } else {

                                service.setComments(activity.getResources().getString(R.string.noClientComments));

                            }

                            ArrayList<DateService> dateServices = new ArrayList<DateService>();

                            int indexFor = 0;

                            Collections.sort(data.getPreference_selected(), new Comparator<String>() {
                                public int compare(String dateFirst, String dateSecond) {
                                    return dateFirst.compareTo(dateSecond);
                                }
                            });

                            for (String dateDate : data.getPreference_selected()) {

                                String startDateHourFor = dateDate.substring(0, 19);
                                String endDateHourFor = dateDate.substring(22, dateDate.length());

                                String[] dateStart = startDateHourFor.split(" ");
                                String[] dateEnd = endDateHourFor.split(" ");

                                DateService dateService = new DateService();

                                if (Utils.compareDates(dateStart[0], dateEnd[0]) == 2) {

                                    String[] startDateSplit = dateStart[0].split("-");
                                    String[] endDateSplit = dateEnd[0].split("-");

                                    Date startDateDate = new Date(Integer.parseInt(startDateSplit[2]), Integer.parseInt(startDateSplit[1]) - 1, Integer.parseInt(startDateSplit[0]));
                                    Date endDateDate = new Date(Integer.parseInt(endDateSplit[2]), Integer.parseInt(endDateSplit[1]) - 1, Integer.parseInt(endDateSplit[0]));

                                    Calendar cal1 = new GregorianCalendar();
                                    Calendar cal2 = new GregorianCalendar();

                                    cal1.setTime(startDateDate);
                                    cal2.setTime(endDateDate);

                                    int days = Utils.daysBetween(cal1.getTime(), cal2.getTime());

                                    for (int i = 0; i <= days; i++) {

                                        if (i == 0) {

                                            if (indexFor == 0) {

                                                dateService = new DateService(dateStart[0], dateStart[0], dateStart[1] + " " + dateStart[2], endTimeDefault, true);

                                            } else {

                                                dateService = new DateService(dateStart[0], dateStart[0], dateStart[1] + " " + dateStart[2], endTimeDefault, false);

                                            }

                                        } else if (i == days) {

                                            if (indexFor == 0) {

                                                dateService = new DateService(dateEnd[0], dateEnd[0], startTimeDefault, dateEnd[1] + " " + dateEnd[2], true);

                                            } else {

                                                dateService = new DateService(dateEnd[0], dateEnd[0], startTimeDefault, dateEnd[1] + " " + dateEnd[2], false);

                                            }


                                        } else {

                                            cal1.add(Calendar.DATE, 1);
                                            String dayCalString = cal1.get(Calendar.DAY_OF_MONTH) + "-" + String.valueOf(cal1.get(Calendar.MONTH) + 1) + "-" + String.valueOf(cal1.get(Calendar.YEAR) - 1900);

                                            if (indexFor == 0) {


                                                dateService = new DateService(dayCalString, dayCalString, startTimeDefault, endTimeDefault, true);

                                            } else {

                                                dateService = new DateService(dayCalString, dayCalString, startTimeDefault, endTimeDefault, false);

                                            }


                                        }

                                        indexFor++;

                                        dateServices.add(dateService);


                                    }


                                } else {

                                    if (indexFor == 0) {

                                        dateService = new DateService(dateStart[0], dateEnd[0], dateStart[1] + " " + dateStart[2], dateEnd[1] + " " + dateEnd[2], true);

                                    } else {

                                        dateService = new DateService(dateStart[0], dateEnd[0], dateStart[1] + " " + dateStart[2], dateEnd[1] + " " + dateEnd[2], false);

                                    }

                                    indexFor++;

                                    dateServices.add(dateService);


                                }


                            }

                            String startDateData = data.getPreference_selected().get(0);
                            String endDateData = data.getPreference_selected().get(data.getPreference_selected().size() - 1);


                            service.setPreferedDates(dateServices);

                            String startDateHour = startDateData.substring(0, 19);
                            String endDateHour = endDateData.substring(22, endDateData.length());

                            String[] dateStart = startDateHour.split(" ");
                            String[] dateEnd = endDateHour.split(" ");

                            service.setDateStart(dateStart[0]);
                            service.setDateFinish(dateEnd[0]);
                            service.setTimeStart(dateStart[1] + " " + dateStart[2]);
                            service.setTimeFinish(dateEnd[1] + " " + dateEnd[2]);


                            if (data.getVisibility_tariff().equals(Utils.NO_TAG)) {

                                service.setVisibility(false);

                            } else {

                                service.setVisibility(true);

                            }

                            service.setPostalCode(data.getCodigo_postal());

                            avServList.add(service);

                            if (lenght < 3) {

                                avServCardList.add(service);

                            }


                        }

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

                        ListView availableServices = (ListView) findViewById(R.id.avServList);
                        asca = new AvailableServicesCardAdapter(DashboardActivity.this, avServCardList, "C", activity);
                        asca.setCallback(DashboardActivity.this);
                        if (availableServices != null) {
                            availableServices.setAdapter(asca);
                            availableServices.setEmptyView(findViewById(R.id.emptyAvServView));
                        }

                        TextView avMoreButton = (TextView) findViewById(R.id.avServMoreButton);
                        if (avMoreButton != null) {
                            avMoreButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent avServicesIntent = new Intent(getBaseContext(), AvailableServicesActivity.class);
                                    avServicesIntent.putExtra(Utils.FAMAEX_AVAILABLE_SERIAZABLE, avServList);
                                    startActivity(avServicesIntent);
                                }
                            });
                        }

                        /*
                        if (mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false)) {

                            getUsersProviders();

                        }
                        */


                    } else {

                        displayError(servicesAvailable.getMessage(), AVAILABLE_SERVICES);

                    }


                } else {

                    availableServicesRetry++;

                    if (availableServicesRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.generic_error_message), AVAILABLE_SERVICES);

                    }

                }

            }

            @Override
            public void onFailure
                    (Call<com.gabrielguzman.android.famaex.web.models.Service> call, Throwable t) {

                availableServicesRetry++;

                if (availableServicesRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.generic_error_message), AVAILABLE_SERVICES);

                }

            }
        });


    }

    private void setHistoryServices() {

        mProgressOverlay3 = findViewById(R.id.compServProgressBar1);
        Famaex.animateView(mProgressOverlay3, View.VISIBLE, 1.0f, 200);

        mProgressOverlay4 = findViewById(R.id.compServProgressBar2);
        Famaex.animateView(mProgressOverlay4, View.VISIBLE, 1.0f, 200);

        visibilityTariff = true;

        compServList = new ArrayList<>();
        compServCardList = new ArrayList<>();

        historyServiceRetry = 0;

        ServiceEndPointInterface serviceAvailableEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

        final User userHistory = new User(mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), null, null, null, null, null, null, null, null, null);

        Call<History> callHistory = serviceAvailableEndPointInterface.serviceHistory(RestClient.CONTENT_TYPE, userHistory);
        callHistory.enqueue(new Callback<History>() {
            @Override
            public void onResponse(Call<History> call, Response<History> response) {

                History servicesHistory;

                if (response.body() != null) {

                    servicesHistory = response.body();

                    if (Utils.verifyOKTAG(servicesHistory, activity.getApplicationContext())) {

                        compServLength = 0;
                        compServPrice = 0;
                        rtQty = 0;
                        rtPoints = 0f;

                        int lenght = 0;

                        for (DataHistory dataHistory : servicesHistory.getData()) {

                            Service service = new Service();
                            Double servTotalPrice = 0.0;

                            if ((dataHistory.getServiceState().equals(Utils.COMPLETED_SERVICE_VALUE)) || (dataHistory.getServiceState().equals(Utils.CANCELED_SERVICE_VALUE))) {

                                service.setClientName(dataHistory.getClient());
                                service.setClientAddress(dataHistory.getAddress());
                                service.setServiceName(dataHistory.getService());
                                service.setProviderName(dataHistory.getProvider_name());

                                //materials cost

                                if (dataHistory.getProvider_price() != null) {

                                    servTotalPrice = dataHistory.getProvider_price();

                                }

                                if (dataHistory.getAssistant_price() != null) {

                                    servTotalPrice = dataHistory.getAssistant_price();

                                }

                                if (dataHistory.getTechnical_price() != null) {

                                    servTotalPrice = dataHistory.getTechnical_price();

                                }

                                service.setTotalPrice(servTotalPrice.intValue());

                                compServLength++;
                                compServPrice += servTotalPrice;

                                service.setServiceID(dataHistory.getServiceId());

                                if (dataHistory.getUrgent()) {

                                    service.setPriority(Utils.URGENT_TAG);

                                } else {

                                    service.setPriority("");

                                }

                                String date = dataHistory.getPreference_selected().get(0);

                                String startDateHour = date.substring(0, 19);
                                String endDateHour = date.substring(22, date.length());

                                String[] dateStart = startDateHour.split(" ");
                                String[] dateEnd = endDateHour.split(" ");

                                service.setDateStart(dateStart[0]);
                                service.setDateFinish(dateEnd[0]);
                                service.setTimeStart(dateStart[1] + " " + dateStart[2]);
                                service.setTimeFinish(dateEnd[1] + " " + dateEnd[2]);

                                if (dataHistory.getVisibility_tariff().equals(Utils.NO_TAG)) {

                                    service.setVisibility(false);
                                    visibilityTariff = false;

                                } else {

                                    service.setVisibility(true);

                                }

                                service.setDateArrival(dataHistory.getArrivalDateProvider());
                                service.setTimeArrival(dataHistory.getArrivalTimeProvider());


                                service.setCif(dataHistory.getCif());
                                service.setAdmin(dataHistory.getAdmin());
                                service.setStatus(dataHistory.getServiceState());

                                if (dataHistory.getComment_client() != null) {

                                    service.setComments(dataHistory.getComment_client());

                                } else {

                                    service.setComments(activity.getResources().getString(R.string.noClientComments));

                                }

                                if (dataHistory.getComment_provider() != null) {

                                    service.setProviderComment(dataHistory.getComment_provider());

                                } else {

                                    service.setProviderComment(activity.getResources().getString(R.string.noProviderComments));

                                }

                                if (dataHistory.getRating_comment() != null) {

                                    service.setClientRatingComment(dataHistory.getRating_comment());

                                } else {

                                    service.setClientRatingComment(activity.getResources().getString(R.string.noClientComments));

                                }

                                if (dataHistory.getRatingService() != null) {

                                    service.setRatingValue(dataHistory.getRatingService().intValue());
                                    rtQty++;
                                    rtPoints += dataHistory.getRatingService().intValue();


                                }


                                compServList.add(service);

                                if (lenght < 3) {

                                    lenght++;
                                    compServCardList.add(service);
                                }

                            }

                        }

                        Famaex.animateView(mProgressOverlay3, View.GONE, 0, 200);
                        Famaex.animateView(mProgressOverlay4, View.GONE, 0, 200);

                        TextView compServTotalQty = (TextView) findViewById(R.id.compTotalsServQty);
                        TextView compServTotalPrice = (TextView) findViewById(R.id.compTotalsServEarnings);
                        TextView rtAverage = (TextView) findViewById(R.id.rtCardRating);
                        RatingBar rtBar = (RatingBar) findViewById(R.id.rtCardRatingBar);

                        boolean state = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

                        if (state) {

                            compServTotalPrice.setVisibility(View.VISIBLE);

                        } else {

                            if (visibilityTariff) {

                                compServTotalPrice.setVisibility(View.VISIBLE);

                            } else {

                                compServTotalPrice.setVisibility(View.INVISIBLE);

                            }

                        }


                        if (compServTotalQty != null) {
                            compServTotalQty.setText(String.format("%d", compServLength));
                        }
                        if (compServTotalPrice != null) {
                            compServTotalPrice.setText(String.format("%d €", compServPrice));
                        }

                        float points = (rtQty == 0) ? 0 : rtPoints / rtQty;

                        if (rtAverage != null) {
                            rtAverage.setText(String.format("%.1f / 5,0", points));
                        }
                        if (rtBar != null) {
                            rtBar.setStepSize(0.5f);
                            rtBar.setRating(points);
                        }

                        ListView completedServices = (ListView) findViewById(R.id.compServList);
                        if (completedServices != null) {
                            completedServices.setAdapter(new FinishedServicesCardAdapter(DashboardActivity.this, compServCardList, "C"));
                            completedServices.setEmptyView(findViewById(R.id.emptyCompServView));
                        }


                        TextView compMoreButton = (TextView) findViewById(R.id.compServMoreButton);
                        if (compMoreButton != null) {
                            compMoreButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent compServicesIntent = new Intent(getBaseContext(), FinishedServicesActivity.class);
                                    compServicesIntent.putExtra(Utils.FAMAEX_FINISHED_SERIAZABLE, compServList);
                                    startActivity(compServicesIntent);
                                }
                            });
                        }


                    } else {

                        displayError(servicesHistory.getStatus(), HISTORY_SERVICES);

                    }


                } else {

                    historyServiceRetry++;

                    if (historyServiceRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.generic_error_message), HISTORY_SERVICES);

                    }

                }


            }

            @Override
            public void onFailure(Call<History> call, Throwable t) {

                historyServiceRetry++;

                if (historyServiceRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.generic_error_message), HISTORY_SERVICES);

                }

            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)) {


            FragmentManager fm = getFragmentManager();
            StsChangeQuestionFragment questionDialogFragment =
                    StsChangeQuestionFragment.newInstance("Servicio Ganado", serviceDetail, 1);

            questionDialogFragment.show(fm, "fragment_servicio_ganado");


        } else {

            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_permission_granted_error), Toast.LENGTH_SHORT).show();


        }


    }

    private void setWonServices() {

        mProgressOverlay2 = findViewById(R.id.wnServProgressBar);
        Famaex.animateView(mProgressOverlay2, View.VISIBLE, 1.0f, 200);

        wnServList = new ArrayList<>();
        wnServCardList = new ArrayList<>();

        wonServiceRetry = 0;

        ServiceEndPointInterface serviceWonEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

        final User userWon = new User(mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), null, null, null, null, null, null, null, null, null);

        Call<Approved> callHistory = serviceWonEndPointInterface.approvedServices(RestClient.CONTENT_TYPE, userWon);
        callHistory.enqueue(new Callback<Approved>() {
            @Override
            public void onResponse(Call<Approved> call, Response<Approved> response) {

                Approved approvedServices;

                if (response.body() != null) {

                    approvedServices = response.body();

                    if (Utils.verifyOKTAG(approvedServices, activity)) {

                        int lenght = 0;

                        for (DataApproved dataApproved : approvedServices.getData()) {

                            Service service = new Service();

                            service.setClientName(dataApproved.getClient_name());
                            service.setClientPhone(dataApproved.getClient_phone());
                            service.setClientAddress(dataApproved.getClient_address());
                            service.setServiceName(dataApproved.getService());
                            service.setSubService(dataApproved.getSubservice());

                            if (dataApproved.getNte_provider() != null) {

                                if (dataApproved.getNte_provider().intValue() != 0) {

                                    service.setNteProvider(dataApproved.getNte_provider().intValue());

                                } else {

                                    service.setNteProvider(0);

                                }

                            } else {

                                service.setNteProvider(0);

                            }

                            if (dataApproved.getComment_client() != null) {

                                service.setComments(dataApproved.getComment_client());

                            } else {

                                service.setComments(activity.getResources().getString(R.string.noClientComments));

                            }

                            if (dataApproved.getStatus_id() != null) {

                                service.setStatusID(dataApproved.getStatus_id());

                            }

                            service.setStatus(dataApproved.getStatus());

                            if (dataApproved.getService_per_mts2().equals(Utils.HOURS_TAG)) {

                                service.setPrice(dataApproved.getTotal_price_mt2().intValue());
                                service.setTypePrice(true);


                            } else {

                                int serviceTotal = 0;

                                if (dataApproved.getAssistant_hour_price_client() != null) {

                                    serviceTotal += dataApproved.getAssistant_hour_price_client().intValue();

                                }


                                if (dataApproved.getTechnical_hour_price_client() != null) {

                                    serviceTotal += dataApproved.getTechnical_hour_price_client().intValue();

                                }


                                service.setPrice(serviceTotal);
                                service.setTypePrice(false);


                            }


                            if (dataApproved.getService_request_id() != null) {

                                service.setServiceRqID(dataApproved.getService_request_id());

                            }


                            if (dataApproved.getUrgent()) {

                                service.setPriority(Utils.URGENT_TAG);

                            } else {

                                service.setPriority("");

                            }

                            String date = dataApproved.getPreference_selected().get(0);

                            String startDateHour = date.substring(0, 19);

                            String[] dateStart = startDateHour.split(" ");

                            service.setDateSelected(dateStart[0]);
                            service.setTimeSelected(dateStart[1] + " " + dateStart[2]);

                            if (dataApproved.getVisibility_tariff().equals(Utils.NO_TAG)) {

                                service.setVisibility(false);

                            } else {

                                service.setVisibility(true);

                            }

                            wnServList.add(service);

                            if (lenght < 3) {

                                lenght++;
                                wnServCardList.add(service);
                            }

                        }


                        Famaex.animateView(mProgressOverlay2, View.GONE, 0, 200);

                        ListView wonServices = (ListView) findViewById(R.id.wnServList);
                        WonServicesCardAdapter wsca = new WonServicesCardAdapter(DashboardActivity.this, wnServCardList, "C");
                        wsca.setCallback(DashboardActivity.this);
                        if (wonServices != null) {
                            wonServices.setAdapter(wsca);
                            wonServices.setEmptyView(findViewById(R.id.emptyWnServView));
                        }

                        TextView wnMoreButton = (TextView) findViewById(R.id.wnServMoreButton);
                        if (wnMoreButton != null) {
                            wnMoreButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent wnServicesIntent = new Intent(getBaseContext(), WonServicesActivity.class);
                                    wnServicesIntent.putExtra(Utils.FAMAEX_WON_SERIAZABLE, wnServList);
                                    startActivity(wnServicesIntent);
                                }
                            });
                        }


                    } else {

                        displayError(approvedServices.getStatus(), WON_SERVICES);

                    }


                } else {

                    wonServiceRetry++;

                    if (wonServiceRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.generic_error_message), WON_SERVICES);

                    }

                }


            }

            @Override
            public void onFailure(Call<Approved> call, Throwable t) {

                wonServiceRetry++;

                if (wonServiceRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.generic_error_message), WON_SERVICES);

                }


            }
        });


    }


    public void updateData() {

        //-------------------available

        setAvailableServices();

        //-------------------won

        setWonServices();

        //-------------------completed

        setHistoryServices();
    }


    public void checkPermission(Service service) {

        int permissions = 0;

        serviceDetail = service;

        if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) || (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) || (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    MANY_PERMISSIONS_REQUEST);


        } else {

            permissions = 1;

        }

        if (permissions == 1) {

            FragmentManager fm = getFragmentManager();
            StsChangeQuestionFragment questionDialogFragment =
                    StsChangeQuestionFragment.newInstance("Servicio Ganado", service, 1);

            questionDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    // mWnServTask = new WonServDownloadTask();
                    // mWnServTask.execute((Void) null);
                    //WonServicesDetailFragment.this.dismiss();
                }
            });
            questionDialogFragment.show(fm, "fragment_servicio_ganado");

        }

    }

    @Override
    public void avDialogDismissed() {
        updateData();
    }

    @Override
    public void wonDialogDismissed() {
        updateData();
    }

    @Override
    public void notifDialogDismissed() {
        updateData();

    }

    public void registerGCMToken() {

        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        //Check status of Google play service in device
        int resultCode = googleAPI.isGooglePlayServicesAvailable(getApplicationContext());
        if (ConnectionResult.SUCCESS != resultCode) {
            //Check type of error
            if (googleAPI.isUserResolvableError(resultCode)) {
                Toast.makeText(getApplicationContext(), getString(R.string.installGooglePlayServices), Toast.LENGTH_LONG).show();
                googleAPI.showErrorNotification(getApplicationContext(), resultCode);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.notCompatibleGooglePlayServices), Toast.LENGTH_LONG).show();
            }
        } else {
            //Start service
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);
        }

    }

    public void logout() {

        updateData();

        boolean admin = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

        if (admin) {

            actionRefresh.setEnabled(false);
            actionAbout.setEnabled(false);
            actionSettings.setEnabled(false);
            actionUsers.setEnabled(false);
            actionLogout.setEnabled(false);

        } else {

            actionRefresh.setEnabled(false);
            actionAbout.setEnabled(false);
            actionSettings.setEnabled(false);
            actionLogout.setEnabled(false);

        }


        Famaex.animateView(mProgressOverlayClose, View.VISIBLE, 1.0f, 200);
        progressMssg.setText(activity.getResources().getString(R.string.loading_close_session));

        SharedPreferences logoutPref = getSharedPreferences(AppSharedPreferences.SHARED_NAME, MODE_PRIVATE);

        TokenEndPointInterface tokenEndPointInterface = retrofit.create(TokenEndPointInterface.class);

        final Token token = new Token(logoutPref.getString(AppSharedPreferences.DEVICE_TOKEN, ""), logoutPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), Utils.PLATFORM_VALUE, null, null);

        logoutUserRetry = 0;

        Call<TokenResult> call = tokenEndPointInterface.deleteToken(RestClient.CONTENT_TYPE, token);
        call.enqueue(new Callback<TokenResult>() {
            @Override
            public void onResponse(Call<TokenResult> call, Response<TokenResult> response) {

                TokenResult tokenResult;

                if (response.body() != null) {

                    tokenResult = response.body();

                    SharedPreferences userPref = getSharedPreferences(AppSharedPreferences.SHARED_NAME, MODE_PRIVATE);

                    userPref.edit().putBoolean(AppSharedPreferences.LOGGED_USER, false).apply();
                    userPref.edit().putInt(AppSharedPreferences.PROVIDER_ID, 0).apply();
                    userPref.edit().putString(AppSharedPreferences.PROVIDER_NAME, "").apply();
                    userPref.edit().putString(AppSharedPreferences.PROVIDER_EMAIL, "").apply();
                    userPref.edit().putString(AppSharedPreferences.PROVIDER_COMPANY, "").apply();
                    userPref.edit().putBoolean(AppSharedPreferences.PROVIDER_ADMIN, false).apply();
                    userPref.edit().putString(AppSharedPreferences.DEVICE_TOKEN, "").apply();

                    Famaex.animateView(mProgressOverlayClose, View.GONE, 0, 200);

                    Intent loginIntent = new Intent(activity, LoginActivity.class);
                    startActivity(loginIntent);
                    finish();


                } else {

                    logoutUserRetry++;

                    if (logoutUserRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.logout_error_message), LOGOUT_SERVICE);

                        boolean admin = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

                        if (admin) {

                            actionRefresh.setEnabled(true);
                            actionAbout.setEnabled(true);
                            actionSettings.setEnabled(true);
                            actionUsers.setEnabled(true);
                            actionLogout.setEnabled(true);

                        } else {

                            actionRefresh.setEnabled(true);
                            actionAbout.setEnabled(true);
                            actionSettings.setEnabled(true);
                            actionLogout.setEnabled(true);

                        }

                    }

                }

            }

            @Override
            public void onFailure(Call<TokenResult> call, Throwable t) {

                logoutUserRetry++;

                if (logoutUserRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.logout_error_message), LOGOUT_SERVICE);

                    boolean admin = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

                    if (admin) {

                        actionRefresh.setEnabled(true);
                        actionAbout.setEnabled(true);
                        actionSettings.setEnabled(true);
                        actionUsers.setEnabled(true);
                        actionLogout.setEnabled(true);

                    } else {

                        actionRefresh.setEnabled(true);
                        actionAbout.setEnabled(true);
                        actionSettings.setEnabled(true);
                        actionLogout.setEnabled(true);

                    }

                }

            }
        });

    }

    /**
     * Sets the header data for the user
     *
     * @since 2016.12.07
     */
    public void setUserData() {

        TextView userAndCo = (TextView) findViewById(R.id.dashboardUserAndCo);
        TextView userType = (TextView) findViewById(R.id.dashboardUserRole);

        if (userAndCo != null) {
            userAndCo.setText(String.format("%s / %s",
                    mUserPref.getString(AppSharedPreferences.PROVIDER_NAME, activity.getResources().getString(R.string.global_no_name_user)),
                    mUserPref.getString(AppSharedPreferences.PROVIDER_COMPANY, activity.getResources().getString(R.string.global_no_company_user))));
        }
        if (userType != null) {
            userType.setText(mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false) ? activity.getResources().getString(R.string.global_admin_user) : activity.getResources().getString(R.string.global_normal_user));
        }
    }

    public void goToPasswordChange(@Nullable View v) {
        Intent psswChangeIntent = new Intent(this, ChangePasswordActivity.class);
        startActivity(psswChangeIntent);
    }

    private void showEditDialog(Bundle message, int srID) {

        FragmentManager fm = DashboardActivity.this.getFragmentManager();
        NotificationDetailFragment notificationDialogFragment =
                NotificationDetailFragment.newInstance("Notificacion", message, srID);
        notificationDialogFragment.show(fm, "fragment_notification");

    }

    public void goToUserActivity() {

        Intent userActivityIntent = new Intent(this, UserActivity.class);
        startActivity(userActivityIntent);

    }

    /*-------- Menu methods ---------*/
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        boolean admin = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

        if (admin) {

            getMenuInflater().inflate(R.menu.main_menu_admin, menu);

            actionRefresh = menu.getItem(0);
            actionAbout = menu.getItem(1);
            actionSettings = menu.getItem(2);
            actionUsers = menu.getItem(3);
            actionLogout = menu.getItem(4);


        } else {

            getMenuInflater().inflate(R.menu.main_menu, menu);

            actionRefresh = menu.getItem(0);
            actionAbout = menu.getItem(1);
            actionSettings = menu.getItem(2);
            actionLogout = menu.getItem(3);

        }

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {

            Intent aboutIntent = new Intent(this, AboutActivity.class);
            startActivity(aboutIntent);
            return true;

        } else if (id == R.id.action_settings) {

            goToPasswordChange(null);

        } else if (id == R.id.action_refresh) {

            updateData();

        } else if (id == R.id.action_users) {

            goToUserActivity();

        } else if (id == R.id.action_logout) {

            logout();

        }

        return super.onOptionsItemSelected(item);
    }

    private void displayError(String message, int operation) {

        switch (operation) {

            case (LOGOUT_SERVICE):

                Famaex.animateView(mProgressOverlayClose, View.GONE, 0, 200);

                break;

            case (AVAILABLE_SERVICES):

                Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

                avServTotalQty.setText(String.valueOf(0));

                ListView availableServices = (ListView) findViewById(R.id.avServList);
                AvailableServicesCardAdapter asca = new AvailableServicesCardAdapter(DashboardActivity.this, avServCardList, "C", activity);
                asca.setCallback(DashboardActivity.this);
                if (availableServices != null) {
                    availableServices.setAdapter(asca);
                    availableServices.setEmptyView(findViewById(R.id.emptyAvServView));
                }

                TextView avMoreButton = (TextView) findViewById(R.id.avServMoreButton);
                if (avMoreButton != null) {
                    avMoreButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent avServicesIntent = new Intent(getBaseContext(), AvailableServicesActivity.class);
                            avServicesIntent.putExtra(Utils.FAMAEX_AVAILABLE_SERIAZABLE, avServList);
                            startActivity(avServicesIntent);
                        }
                    });
                }

                break;

            case (HISTORY_SERVICES):

                Famaex.animateView(mProgressOverlay3, View.GONE, 0, 200);
                Famaex.animateView(mProgressOverlay4, View.GONE, 0, 200);

                compServLength = 0;
                compServPrice = 0;
                rtQty = 0;
                rtPoints = 0f;

                TextView compServTotalQty = (TextView) findViewById(R.id.compTotalsServQty);
                TextView compServTotalPrice = (TextView) findViewById(R.id.compTotalsServEarnings);
                TextView rtAverage = (TextView) findViewById(R.id.rtCardRating);
                RatingBar rtBar = (RatingBar) findViewById(R.id.rtCardRatingBar);

                if (compServTotalQty != null) {
                    compServTotalQty.setText(String.format("%d", compServLength));
                }
                if (compServTotalPrice != null) {
                    compServTotalPrice.setText(String.format("%d €", compServPrice));
                }

                float points = (rtQty == 0) ? 0 : rtPoints / rtQty;

                if (rtAverage != null) {
                    rtAverage.setText(String.format("%.1f / 5,0", points));
                }
                if (rtBar != null) {
                    rtBar.setStepSize(0.5f);
                    rtBar.setRating(points);
                }

                Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

                ListView completedServices = (ListView) findViewById(R.id.compServList);
                if (completedServices != null) {
                    completedServices.setAdapter(new FinishedServicesCardAdapter(DashboardActivity.this, compServCardList, "C"));
                    completedServices.setEmptyView(findViewById(R.id.emptyCompServView));
                }


                TextView compMoreButton = (TextView) findViewById(R.id.compServMoreButton);
                if (compMoreButton != null) {
                    compMoreButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent compServicesIntent = new Intent(getBaseContext(), FinishedServicesActivity.class);
                            compServicesIntent.putExtra(Utils.FAMAEX_FINISHED_SERIAZABLE, compServList);
                            startActivity(compServicesIntent);
                        }
                    });
                }

                break;

            case (WON_SERVICES):

                Famaex.animateView(mProgressOverlay2, View.GONE, 0, 200);

                Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

                ListView wonServices = (ListView) findViewById(R.id.wnServList);
                WonServicesCardAdapter wsca = new WonServicesCardAdapter(DashboardActivity.this, wnServCardList, "C");
                wsca.setCallback(DashboardActivity.this);
                if (wonServices != null) {
                    wonServices.setAdapter(wsca);
                    wonServices.setEmptyView(findViewById(R.id.emptyWnServView));
                }

                TextView wnMoreButton = (TextView) findViewById(R.id.wnServMoreButton);
                if (wnMoreButton != null) {
                    wnMoreButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent wnServicesIntent = new Intent(getBaseContext(), WonServicesActivity.class);
                            wnServicesIntent.putExtra(Utils.FAMAEX_WON_SERIAZABLE, wnServList);
                            startActivity(wnServicesIntent);
                        }
                    });
                }

                break;


            case (USERS_PROVIDERS):

                Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

                break;

            case (USERS_PROVIDERS_SILENT):

                Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

                break;

        }


    }
}