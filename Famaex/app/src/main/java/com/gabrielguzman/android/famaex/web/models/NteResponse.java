package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 19/05/17.
 */

public class NteResponse {

    private String status;

    public NteResponse() {
    }

    public NteResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
