package com.gabrielguzman.android.famaex.services.won;

import android.Manifest;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.Toast;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.status.StsChangeQuestionFragment;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.ServiceEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.Approved;
import com.gabrielguzman.android.famaex.web.models.DataApproved;
import com.gabrielguzman.android.famaex.web.models.User;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Activity that shows the complete list of won assigned services.
 *
 * @author Gabriel Guzman
 * @see AppCompatActivity
 * @since 2016.21.07
 */
public class WonServicesActivity extends AppCompatActivity implements WonServicesAdapter.UpdateCallback {

    private static int wonServiceRetry;
    public Retrofit retrofit;
    private SharedPreferences mUserPref;
    private ListView wonServices;
    private ArrayList<Service> wnServList;
    private WonServicesActivity activity;
    public static final String LOG_TAG = "WonServicesActivity";
    public static final int MANY_PERMISSIONS_REQUEST = 5000;
    private Service serviceDetail;
    public int resume;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.won_services_activity);

        mUserPref = getSharedPreferences(AppSharedPreferences.SHARED_NAME, MODE_PRIVATE);

        //Set the toolbar
        Toolbar famaexToolbar = (Toolbar) findViewById(R.id.famaexWnServToolbar);
        setSupportActionBar(famaexToolbar);
        ActionBar sab = getSupportActionBar();

        if (sab != null) {
            sab.setDisplayHomeAsUpEnabled(true);
        }

        resume = 0;

        activity = (WonServicesActivity) this;
        retrofit = RestClient.getRetrofitInstance();

        Utils.setPortaitOrientation(activity);

        //-------------------
        wnServList = (ArrayList<Service>) getIntent().getSerializableExtra(Utils.FAMAEX_WON_SERIAZABLE);

        wonServices = (ListView) findViewById(R.id.wnActList);
        wonServices.setAdapter(new WonServicesAdapter(WonServicesActivity.this, wnServList, "A"));
        wonServices.setEmptyView(findViewById(R.id.emptyWnServActView));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {


        if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)) {


            FragmentManager fm = getFragmentManager();
            StsChangeQuestionFragment questionDialogFragment =
                    StsChangeQuestionFragment.newInstance("Servicio Ganado", serviceDetail, 2);

            questionDialogFragment.show(fm, "fragment_servicio_ganado");


        } else {

            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_permission_granted_error), Toast.LENGTH_SHORT).show();


        }


    }

    public void checkPermission(Service service) {

        int permissions = 0;

        serviceDetail = service;

        if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) || (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) || (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    MANY_PERMISSIONS_REQUEST);


        } else {

            permissions = 1;

        }

        if (permissions == 1) {

            FragmentManager fm = getFragmentManager();
            StsChangeQuestionFragment questionDialogFragment =
                    StsChangeQuestionFragment.newInstance("Servicio Ganado", service, 2);

            questionDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    // mWnServTask = new WonServDownloadTask();
                    // mWnServTask.execute((Void) null);
                    //WonServicesDetailFragment.this.dismiss();
                }
            });
            questionDialogFragment.show(fm, "fragment_servicio_ganado");

        }

    }

    @Override
    protected void onResume() {

        super.onResume();


    }

    private void setWonServices() {

        wnServList = new ArrayList<>();

        wonServiceRetry = 0;

        ServiceEndPointInterface serviceWonEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

        final User userWon = new User(mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), null, null, null, null, null, null, null, null, null);

        Call<Approved> callHistory = serviceWonEndPointInterface.approvedServices(RestClient.CONTENT_TYPE, userWon);
        callHistory.enqueue(new Callback<Approved>() {
            @Override
            public void onResponse(Call<Approved> call, Response<Approved> response) {

                Approved approvedServices;

                if (response.body() != null) {

                    approvedServices = response.body();

                    if (Utils.verifyOKTAG(approvedServices, activity)) {

                        for (DataApproved dataApproved : approvedServices.getData()) {

                            Service service = new Service();

                            service.setClientName(dataApproved.getClient_name());
                            service.setClientPhone(dataApproved.getClient_phone());
                            service.setClientAddress(dataApproved.getClient_address());
                            service.setServiceName(dataApproved.getService());
                            service.setSubService(dataApproved.getSubservice());

                            if (dataApproved.getComment_client() != null) {

                                service.setComments(dataApproved.getComment_client());

                            } else {

                                service.setComments(activity.getResources().getString(R.string.noClientComments));

                            }

                            if (dataApproved.getStatus_id() != null) {

                                service.setStatusID(dataApproved.getStatus_id());

                            }

                            service.setStatus(dataApproved.getStatus());

                            if (dataApproved.getNte_provider() != null) {

                                if (dataApproved.getNte_provider().intValue() != 0) {

                                    service.setNteProvider(dataApproved.getNte_provider().intValue());

                                } else {

                                    service.setNteProvider(0);

                                }

                            } else {

                                service.setNteProvider(0);

                            }

                            if (dataApproved.getService_per_mts2().equals(Utils.HOURS_TAG)) {

                                service.setPrice(dataApproved.getTotal_price_mt2().intValue());
                                service.setTypePrice(true);


                            } else {

                                int serviceTotal = 0;

                                if (dataApproved.getAssistant_hour_price_client() != null) {

                                    serviceTotal += dataApproved.getAssistant_hour_price_client().intValue();

                                }


                                if (dataApproved.getTechnical_hour_price_client() != null) {

                                    serviceTotal += dataApproved.getTechnical_hour_price_client().intValue();

                                }


                                service.setPrice(serviceTotal);
                                service.setTypePrice(false);


                            }


                            if (dataApproved.getService_request_id() != null) {

                                service.setServiceRqID(dataApproved.getService_request_id());

                            }


                            if (dataApproved.getUrgent()) {

                                service.setPriority(Utils.URGENT_TAG);

                            } else {

                                service.setPriority("");

                            }

                            if (dataApproved.getVisibility_tariff().equals(Utils.NO_TAG)) {

                                service.setVisibility(false);

                            } else {

                                service.setVisibility(true);

                            }

                            String date = dataApproved.getPreference_selected().get(0);

                            String startDateHour = date.substring(0, 19);

                            String[] dateStart = startDateHour.split(" ");

                            service.setDateSelected(dateStart[0]);
                            service.setTimeSelected(dateStart[1] + " " + dateStart[2]);

                            wnServList.add(service);

                        }


                        WonServicesAdapter wsca = new WonServicesAdapter(WonServicesActivity.this, wnServList, "A");
                        wsca.setCallback(WonServicesActivity.this);
                        if (wonServices != null) {
                            wonServices.setAdapter(wsca);
                            wonServices.setEmptyView(findViewById(R.id.emptyWnServView));
                        }


                    } else {

                        displayError(approvedServices.getStatus());

                    }


                } else {

                    wonServiceRetry++;

                    if (wonServiceRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.generic_error_message));

                    }

                }


            }

            @Override
            public void onFailure(Call<Approved> call, Throwable t) {

                wonServiceRetry++;

                if (wonServiceRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.generic_error_message));

                }


            }
        });


    }

    public void displayError(String message) {

        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

        WonServicesAdapter wsca = new WonServicesAdapter(WonServicesActivity.this, wnServList, "A");
        wsca.setCallback(WonServicesActivity.this);
        if (wonServices != null) {
            wonServices.setAdapter(wsca);
            wonServices.setEmptyView(findViewById(R.id.emptyWnServView));
        }


    }

    public void updateData() {

        setWonServices();

    }

    @Override
    public void wonDialogDismissed() {
        updateData();
    }

}
