package com.gabrielguzman.android.famaex.web.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jose on 06/04/17.
 */

public class DataHistory {

    public static final String CIF_PROVEEDOR = "cif proveedor";
    public static final String DEMANDED_DATE = "demanded date";
    public static final String ACCEPTED_SERVICE_DATE = "accepted service date";
    public static final String ARRIVAL_DATE_PROVIDER = "arrival date provider";
    public static final String ARRIVAL_TIME_PROVIDER = "arrival time provider";
    public static final String SERVICE_STATE = "service state";
    public static final String SERVICE_ID = "service id";
    public static final String SERVICE_REQUEST_ID = "service_request id";
    public static final String RATING_SERVICE = "rating service";
    public static final String PRICE_HOUR = "price_hour";

    private String cif;
    private String client;
    private String user;
    private String admin;
    @SerializedName(CIF_PROVEEDOR)
    private String cifProveedor;
    private String provider;
    @SerializedName(DEMANDED_DATE)
    private String demandedDate;
    @SerializedName(ACCEPTED_SERVICE_DATE)
    private String acceptedServiceDate;
    private Boolean urgent;
    private List<String> preference_selected = null;
    @SerializedName(ARRIVAL_DATE_PROVIDER)
    private String arrivalDateProvider;
    @SerializedName(ARRIVAL_TIME_PROVIDER)
    private String arrivalTimeProvider;
    @SerializedName(SERVICE_STATE)
    private String serviceState;
    private String address;
    @SerializedName(SERVICE_ID)
    private Integer serviceId;
    @SerializedName(SERVICE_REQUEST_ID)
    private Integer serviceRequestId;
    private String service;
    private String comment_client;
    @SerializedName(RATING_SERVICE)
    private Double ratingService;
    private String rating_comment;
    private String comment_provider;
    @SerializedName(PRICE_HOUR)
    private String price_hour;
    private List<Object> material_costs = null;
    private Double technical_price;
    private Double assistant_price;
    private Double provider_price;
    private Integer displacement_fee;
    private String visibility_tariff;
    private String provider_name;

    public DataHistory() {
    }

    public DataHistory(String cif, String client, String user, String admin, String cifProveedor, String provider, String demandedDate, String acceptedServiceDate, Boolean urgent, List<String> preference_selected, String arrivalDateProvider, String arrivalTimeProvider, String serviceState, String address, Integer serviceId, Integer serviceRequestId, String service, String comment_client, Double ratingService, String rating_comment, String comment_provider, String price_hour, List<Object> material_costs, Double technical_price, Double assistant_price, Double provider_price, Integer displacement_fee, String visibility_tariff, String provider_name) {
        this.cif = cif;
        this.client = client;
        this.user = user;
        this.admin = admin;
        this.cifProveedor = cifProveedor;
        this.provider = provider;
        this.demandedDate = demandedDate;
        this.acceptedServiceDate = acceptedServiceDate;
        this.urgent = urgent;
        this.preference_selected = preference_selected;
        this.arrivalDateProvider = arrivalDateProvider;
        this.arrivalTimeProvider = arrivalTimeProvider;
        this.serviceState = serviceState;
        this.address = address;
        this.serviceId = serviceId;
        this.serviceRequestId = serviceRequestId;
        this.service = service;
        this.comment_client = comment_client;
        this.ratingService = ratingService;
        this.rating_comment = rating_comment;
        this.comment_provider = comment_provider;
        this.price_hour = price_hour;
        this.material_costs = material_costs;
        this.technical_price = technical_price;
        this.assistant_price = assistant_price;
        this.provider_price = provider_price;
        this.displacement_fee = displacement_fee;
        this.visibility_tariff = visibility_tariff;
        this.provider_name = provider_name;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getCifProveedor() {
        return cifProveedor;
    }

    public void setCifProveedor(String cifProveedor) {
        this.cifProveedor = cifProveedor;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getDemandedDate() {
        return demandedDate;
    }

    public void setDemandedDate(String demandedDate) {
        this.demandedDate = demandedDate;
    }

    public String getAcceptedServiceDate() {
        return acceptedServiceDate;
    }

    public void setAcceptedServiceDate(String acceptedServiceDate) {
        this.acceptedServiceDate = acceptedServiceDate;
    }

    public Boolean getUrgent() {
        return urgent;
    }

    public void setUrgent(Boolean urgent) {
        this.urgent = urgent;
    }

    public List<String> getPreference_selected() {
        return preference_selected;
    }

    public void setPreference_selected(List<String> preference_selected) {
        this.preference_selected = preference_selected;
    }

    public String getArrivalDateProvider() {
        return arrivalDateProvider;
    }

    public void setArrivalDateProvider(String arrivalDateProvider) {
        this.arrivalDateProvider = arrivalDateProvider;
    }

    public String getArrivalTimeProvider() {
        return arrivalTimeProvider;
    }

    public void setArrivalTimeProvider(String arrivalTimeProvider) {
        this.arrivalTimeProvider = arrivalTimeProvider;
    }

    public String getServiceState() {
        return serviceState;
    }

    public void setServiceState(String serviceState) {
        this.serviceState = serviceState;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getServiceRequestId() {
        return serviceRequestId;
    }

    public void setServiceRequestId(Integer serviceRequestId) {
        this.serviceRequestId = serviceRequestId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getComment_client() {
        return comment_client;
    }

    public void setComment_client(String comment_client) {
        this.comment_client = comment_client;
    }

    public Double getRatingService() {
        return ratingService;
    }

    public void setRatingService(Double ratingService) {
        this.ratingService = ratingService;
    }

    public String getRating_comment() {
        return rating_comment;
    }

    public void setRating_comment(String rating_comment) {
        this.rating_comment = rating_comment;
    }

    public String getComment_provider() {
        return comment_provider;
    }

    public void setComment_provider(String comment_provider) {
        this.comment_provider = comment_provider;
    }

    public String getPrice_hour() {
        return price_hour;
    }

    public void setPrice_hour(String price_hour) {
        this.price_hour = price_hour;
    }

    public List<Object> getMaterial_costs() {
        return material_costs;
    }

    public void setMaterial_costs(List<Object> material_costs) {
        this.material_costs = material_costs;
    }

    public Double getTechnical_price() {
        return technical_price;
    }

    public void setTechnical_price(Double technical_price) {
        this.technical_price = technical_price;
    }

    public Double getAssistant_price() {
        return assistant_price;
    }

    public void setAssistant_price(Double assistant_price) {
        this.assistant_price = assistant_price;
    }

    public Double getProvider_price() {
        return provider_price;
    }

    public void setProvider_price(Double provider_price) {
        this.provider_price = provider_price;
    }

    public Integer getDisplacement_fee() {
        return displacement_fee;
    }

    public void setDisplacement_fee(Integer displacement_fee) {
        this.displacement_fee = displacement_fee;
    }

    public String getVisibility_tariff() {
        return visibility_tariff;
    }

    public void setVisibility_tariff(String visibility_tariff) {
        this.visibility_tariff = visibility_tariff;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }
}
