package com.gabrielguzman.android.famaex.web.models;

import java.util.ArrayList;

/**
 * Created by jose on 18/04/17.
 */

public class UserSpecializationsRequest {

    private Integer id;
    private ArrayList<String> specializations;

    public UserSpecializationsRequest(Integer id, ArrayList<String> specializations) {
        this.id = id;
        this.specializations = specializations;
    }

    public UserSpecializationsRequest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<String> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(ArrayList<String> specializations) {
        this.specializations = specializations;
    }
}
