package com.gabrielguzman.android.famaex.services.won;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Custom adapter to display the lists of Services.
 *
 * @author Gabriel Guzmán
 * @see BaseAdapter
 * @since 2016.21.07
 */
public class WonServicesCardAdapter extends BaseAdapter {

    private static ArrayList<Service> searchArrayList;
    private LayoutInflater mInflater;
    private String mListIndicator;
    private SharedPreferences mUserPref;
    private int[] mViewIDs;
    private int mLayout;
    private Context mContext;
    private int[] wnCardViewIDs = {R.id.wnServClient, R.id.wnServName,
            R.id.wnServDate, R.id.wnServPrice, R.id.wnServStatus};
    private int[] wnActViewIDs = {R.id.actServiceName, R.id.actServiceDateRequested,
            R.id.actServicePostalCode, R.id.actServicePriority,
            R.id.actSubserviceName};
    private UpdateCallback callback;

    public WonServicesCardAdapter(Context context, ArrayList<Service> results, String ind) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
        mListIndicator = ind;
        mContext = context;

        mViewIDs = (ind.equals("A")) ? wnActViewIDs : wnCardViewIDs;
        mLayout = (ind.equals("A")) ? R.layout.available_services_activity_item : R.layout.won_services_card_item;
    }

    @Override
    public int getCount() {
        return searchArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(mLayout, parent, false);
            holder = new ViewHolder();
            holder.txtClient = (TextView) convertView.findViewById(mViewIDs[0]);
            holder.txtServiceName = (TextView) convertView.findViewById(mViewIDs[1]);
            holder.txtDateRequested = (TextView) convertView.findViewById(mViewIDs[2]);
            holder.txtPrice = (TextView) convertView.findViewById(mViewIDs[3]);
            holder.txtStatus = (TextView) convertView.findViewById(mViewIDs[4]);
            if (mListIndicator.equals("A"))
                holder.txtSubserviceName = (TextView) convertView.findViewById(mViewIDs[4]);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Service serv = searchArrayList.get(position);
        Calendar aDate = null;

        try {


            Log.v("dateeee ", "dateeee " + serv.getDateSelected());

            aDate = Famaex.dateToCalendar(Famaex.dateBackendFormat.parse(serv.getDateSelected()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mUserPref = mContext.getSharedPreferences(AppSharedPreferences.SHARED_NAME, mContext.MODE_PRIVATE);

        boolean state = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

        if (state) {

            holder.txtPrice.setVisibility(View.VISIBLE);

        } else {

            if (serv.isVisibility()) {

                holder.txtPrice.setVisibility(View.VISIBLE);

            } else {

                holder.txtPrice.setVisibility(View.INVISIBLE);

            }

        }

        holder.txtServiceName.setText(serv.getServiceName());
        holder.txtDateRequested.setText(String.format("El día %s/%s/%s a las %s",
                aDate.get(Calendar.DAY_OF_MONTH), aDate.get(Calendar.MONTH) + 1, aDate.get(Calendar.YEAR),
                serv.getTimeSelected().length() > 6 ? serv.getTimeSelected().substring(0, 8) : "--"));
        if (serv.getTypePrice()){
            holder.txtPrice.setText(String.format("%d €/Mt²", serv.getPrice()));
        }else{
            holder.txtPrice.setText(String.format("%d €/h", serv.getPrice()));
        }
        holder.txtClient.setText(serv.getClientName());

        if (mListIndicator.equals("A"))
            holder.txtSubserviceName.setText(serv.getSubService());


        holder.txtStatus.setText(serv.getStatus());

        if (serv.getStatus().equals("En proceso")) {
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        } else if (serv.getStatus().equals("Finalizado")) {
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
        } else {
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_red_light));
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditDialog(serv);
            }
        });
        return convertView;
    }

    private void showEditDialog(Service serv) {

        FragmentManager fm = ((Activity) mContext).getFragmentManager();
        WonServicesDetailFragment wonServicesDetailFragment = WonServicesDetailFragment.newInstance("Servicio ganado", serv, 1);

        wonServicesDetailFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (callback != null) {
                    callback.wonDialogDismissed();
                }
            }
        });
        wonServicesDetailFragment.show(fm, "fragment_won_service_card");
    }

    public void setCallback(UpdateCallback callback) {

        this.callback = callback;
    }

    public interface UpdateCallback {

        public void wonDialogDismissed();
    }

    static class ViewHolder {
        TextView txtServiceName;
        TextView txtDateRequested;
        TextView txtPrice;
        TextView txtClient;
        TextView txtSubserviceName;
        TextView txtStatus;

    }
}
