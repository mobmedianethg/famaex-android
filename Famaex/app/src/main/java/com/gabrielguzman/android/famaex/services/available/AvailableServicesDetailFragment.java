package com.gabrielguzman.android.famaex.services.available;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.services.available.adapter.UsersProvidersApdater;
import com.gabrielguzman.android.famaex.services.available.interfaces.AvailableServicesDetailFragmentInterface;
import com.gabrielguzman.android.famaex.services.available.interfaces.UsersProvidersInterface;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.ServiceEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.DataUsersProviders;
import com.gabrielguzman.android.famaex.web.models.TakeService;
import com.gabrielguzman.android.famaex.web.models.UsersProvidersRequest;
import com.gabrielguzman.android.famaex.web.models.UsersProvidersResponse;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Shows a full screen dialog for the available service detail.
 *
 * @author Gabriel Guzmán
 * @see DialogFragment
 * @since 2016.12.07
 */
public class AvailableServicesDetailFragment extends DialogFragment implements AvailableServicesDetailFragmentInterface, UsersProvidersInterface {

    public static final String LOG_TAG = "AvailableServicesDetailFragment";
    //  private Spinner mMinuteSpinner;
    private static int applyServiceRetry;
    public Retrofit retrofit;
    private TextView mServName;
    private TextView mSubservice;
    private TextView mPrice;
    private TextView mPriceTag;
    private TextView mPriceMeter;
    private TextView mPriceMeterTag;
    private TextView mPriceJourney;
    private TextView mPriceJourneyTag;
    private TextView mNTE;
    private TextView mNTETag;
    private TextView mPostalCode;
    private TextView mClientComment;
    private LinearLayout availableServicesDetailFragmentItemContainerHour;
    private TextView avDetFrgDateStartHour;
    private TextView avDetFrgTimeStartHour;
    private TextView avDetFrgDateFinishHour;
    private TextView avDetFrgTimeFinishHour;
    private TextView availableServicesDetailFragmentMoreTextview;
    private TextView mDateStart;
    private TextView mTimeStart;
    private TextView mDateFinish;
    private TextView mTimeFinish;
    private TextView mPropDay;
    private TextView mPropHour;
    private Spinner mDaySpinner;
    private Spinner mHourSpinner;
    private AvailableServicesDetailFragment fragment;
    private CheckBox mCustomDate;
    private LinearLayout mDateSelection;
    private LinearLayout mDateProposal;
    private DatePickerDialog.OnDateSetListener datePickerListener;
    private TimePickerDialog.OnTimeSetListener timePickerListener;
    private ImageView availableServicesDetailFragmentUrgentIcon;
    private Button mApplyButton;
    private Button mDiscardButton;
    private boolean selectedTime;
    private Button mCloseButton;
    private View mProgressOverlay;
    private LinearLayout mASection;
    private LinearLayout mSSection;
    private Service mService;
    private Dialog dialogViewHours;
    private com.gabrielguzman.android.famaex.web.models.Service serviceResponse;
    private DialogInterface.OnDismissListener onDismissListener;
    private SharedPreferences mUserPref;
    //private RecyclerView availableServicesDetailFragmentRecyclerview;
    private com.gabrielguzman.android.famaex.services.available.adapter.AvailableServicesAdapter availableServicesAdapter;
    //private RecyclerView.LayoutManager layoutManager;
    private Dialog dialogView;
    private ImageView dialogUsersProvidersRefreshIcon;
    private LinearLayout dialogUsersProvidersLoading;
    private LinearLayout dialogUsersProvidersContent;
    private RecyclerView dialogUsersProviders;
    private static int getUsersProvidersRetry;
    private ArrayList<DataUsersProviders> usersProviders;
    private UsersProvidersApdater usersProvidersApdater;
    private View va;
    private Context mContext;

    public AvailableServicesDetailFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }


    public void showProvidersList(final Service serv) {

        dialogUsersProvidersLoading.setVisibility(View.GONE);
        dialogUsersProvidersContent.setVisibility(View.VISIBLE);

        dialogUsersProviders = (RecyclerView) dialogView.findViewById(R.id.dialog_users_providers_recyclers_view);

        for (DataUsersProviders dataUsers : usersProviders) {

            dataUsers.setSelect(false);

        }

        mUserPref.edit().putInt(AppSharedPreferences.PROVIDER_SERVICE_SELECTED, 0).apply();

        usersProvidersApdater = new UsersProvidersApdater(usersProviders, mContext, fragment);
        dialogUsersProviders.setAdapter(usersProvidersApdater);
        dialogUsersProviders.setLayoutManager(new LinearLayoutManager(mContext));

        dialogUsersProvidersRefreshIcon = (ImageView) dialogView.findViewById(R.id.dialog_users_providers_refresh_icon);
        dialogUsersProvidersRefreshIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                getUsersProviders(serv, 0);

            }
        });

        TextView dialogUsersProvidersCancel = (TextView) dialogView.findViewById(R.id.dialog_users_providers_cancel);

        dialogUsersProvidersCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                usersProvidersApdater.clearList();
                mUserPref.edit().putInt(AppSharedPreferences.PROVIDER_SERVICE_SELECTED, 0).apply();
                dialogView.dismiss();

            }
        });


        TextView dialogUsersProvidersAccept = (TextView) dialogView.findViewById(R.id.dialog_users_providers_accept);
        dialogUsersProvidersAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mUserPref.getInt(AppSharedPreferences.PROVIDER_SERVICE_SELECTED, 0) != 0) {

                    dialogView.dismiss();
                    applyService();


                } else {

                    Toast.makeText(mContext, mContext.getResources().getString(R.string.select_user_provider), Toast.LENGTH_SHORT).show();

                }

            }
        });


        //dialogView.show();

    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        mContext = activity;
    }

    public void getUsersProviders(final Service service, int firsTime) {

        dialogUsersProvidersLoading.setVisibility(View.VISIBLE);
        dialogUsersProvidersContent.setVisibility(View.GONE);

        if (firsTime == 1) {

            dialogView.show();

        }

        getUsersProvidersRetry = 0;

        ServiceEndPointInterface serviceUsersEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

        final UsersProvidersRequest usersProvidersRequest = new UsersProvidersRequest(mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0));

        Call<UsersProvidersResponse> callUsers = serviceUsersEndPointInterface.getUsersProviders(RestClient.CONTENT_TYPE, usersProvidersRequest);
        callUsers.enqueue(new Callback<UsersProvidersResponse>() {
            @Override
            public void onResponse(Call<UsersProvidersResponse> call, Response<UsersProvidersResponse> response) {

                UsersProvidersResponse usersProvidersResponse;

                if (response.body() != null) {

                    usersProvidersResponse = response.body();

                    if (Utils.verifyOKTAG(usersProvidersResponse, getActivity())) {

                        int position = 0;

                        for (DataUsersProviders dataUserProvider : usersProvidersResponse.getData()) {

                            dataUserProvider.setSelect(false);

                            if (position == usersProvidersResponse.getData().size() - 1) {

                                dataUserProvider.setLast(1);

                            } else {

                                dataUserProvider.setLast(0);
                                position++;


                            }

                        }

                        usersProviders = usersProvidersResponse.getData();
                        showProvidersList(service);


                    } else {

                        dialogView.dismiss();
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                    }


                } else {

                    getUsersProvidersRetry++;

                    if (getUsersProvidersRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        dialogView.dismiss();
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                    }

                }

            }

            @Override
            public void onFailure
                    (Call<UsersProvidersResponse> call, Throwable t) {

                getUsersProvidersRetry++;

                if (getUsersProvidersRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    dialogView.dismiss();
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                }

            }
        });


    }

    public static AvailableServicesDetailFragment newInstance(String title, Service serv) {
        AvailableServicesDetailFragment frag = new AvailableServicesDetailFragment();
        Bundle args = new Bundle();
        args.putString(Utils.TITTLE_TAG, title);
        args.putSerializable(Utils.SERVICE_TAG, serv);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        return inflater.inflate(R.layout.available_services_detail_fragment, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        mServName = (TextView) view.findViewById(R.id.avDetFrgServName);
        mSubservice = (TextView) view.findViewById(R.id.avDetFrgSubserv);
        mPriceTag = (TextView) view.findViewById(R.id.avDetFrgPriceTextview);
        mPrice = (TextView) view.findViewById(R.id.avDetFrgPrice);
        mPriceMeterTag = (TextView) view.findViewById(R.id.avDetFrgPriceMeterTextview);
        mPriceMeter = (TextView) view.findViewById(R.id.avDetFrgPriceMeter);
        mPriceJourneyTag = (TextView) view.findViewById(R.id.avDetFrgPriceJourneyTextview);
        mPriceJourney = (TextView) view.findViewById(R.id.avDetFrgPriceJourney);
        mNTETag = (TextView) view.findViewById(R.id.avDetFrgNTETextview);
        mNTE = (TextView) view.findViewById(R.id.avDetFrgNTE);
        mPostalCode = (TextView) view.findViewById(R.id.avDetFrgPostalCode);
        mClientComment = (TextView) view.findViewById(R.id.avDetFrgClientComment);
        availableServicesDetailFragmentUrgentIcon = (ImageView) view.findViewById(R.id.available_services_detail_fragment_urgent_icon);
        //mDateStart = (TextView) view.findViewById(R.id.avDetFrgDateStart);
        //mDateFinish = (TextView) view.findViewById(R.id.avDetFrgDateFinish);
        //mTimeFinish = (TextView) view.findViewById(R.id.avDetFrgTimeFinish);
        //mTimeStart = (TextView) view.findViewById(R.id.avDetFrgTimeStart);
        //availableServicesDetailFragmentRecyclerview = (RecyclerView) view.findViewById(R.id.available_services_detail_fragment_recyclerview);

        fragment = this;


        dialogView = new Dialog(mContext);
        dialogView.setCancelable(false);
        dialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogView.setContentView(R.layout.dialog_users_providers);

        dialogUsersProvidersLoading = (LinearLayout) dialogView.findViewById(R.id.dialog_users_providers_loading_content);
        dialogUsersProvidersContent = (LinearLayout) dialogView.findViewById(R.id.dialog_users_providers_content);

        selectedTime = false;

        availableServicesDetailFragmentMoreTextview = (TextView) view.findViewById(R.id.available_services_detail_fragment_more_textview);

        availableServicesDetailFragmentItemContainerHour = (LinearLayout) view.findViewById(R.id.available_services_detail_fragment_item_container_hour);
        avDetFrgDateStartHour = (TextView) view.findViewById(R.id.avDetFrgDateStart_hour);
        avDetFrgTimeStartHour = (TextView) view.findViewById(R.id.avDetFrgTimeStart_hour);
        avDetFrgDateFinishHour = (TextView) view.findViewById(R.id.avDetFrgDateFinish_hour);
        avDetFrgTimeFinishHour = (TextView) view.findViewById(R.id.avDetFrgTimeFinish_hour);

        mDaySpinner = (Spinner) view.findViewById(R.id.avDetFrgDaySpinner);
        mHourSpinner = (Spinner) view.findViewById(R.id.avDetFrgHourSpinner);
        //    mMinuteSpinner = (Spinner) view.findViewById(R.id.avDetFrgMinuteSpinner);
        mCustomDate = (CheckBox) view.findViewById(R.id.avDetFrgCheck);
        mDateSelection = (LinearLayout) view.findViewById(R.id.avDetFrgDateSelection);
        mDateProposal = (LinearLayout) view.findViewById(R.id.avDetFrgDateProposal);
        mDateProposal.setVisibility(View.GONE);
        mApplyButton = (Button) view.findViewById(R.id.avDetFrgApplyButton);
        mDiscardButton = (Button) view.findViewById(R.id.avDetFrgDiscardButton);

        fragment = this;

        //layoutManager = new LinearLayoutManager(getActivity());

        // Fetch arguments from bundle and set title
        String title = getArguments().getString(Utils.TITTLE_TAG, Utils.ALERT_TAG);
        getDialog().setTitle(title);

        mService = (Service) getArguments().getSerializable(Utils.SERVICE_TAG);

        if (mService.getPriority().equals("Urgente")) {

            availableServicesDetailFragmentUrgentIcon.setVisibility(View.VISIBLE);

        } else {

            availableServicesDetailFragmentUrgentIcon.setVisibility(View.GONE);

        }


        mUserPref = getActivity().getSharedPreferences(AppSharedPreferences.SHARED_NAME, getActivity().MODE_PRIVATE);


        boolean state = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

        if (mService != null) {

            if (mService.getPreferedDates() != null) {

                if (mService.getPreferedDates().size() > 1) {

                    availableServicesDetailFragmentMoreTextview.setVisibility(View.VISIBLE);
                    availableServicesDetailFragmentItemContainerHour.setVisibility(View.VISIBLE);

                    selectedTime = true;


                } else {

                    availableServicesDetailFragmentMoreTextview.setVisibility(View.GONE);
                    selectedTime = true;

                }

            } else {

                availableServicesDetailFragmentMoreTextview.setVisibility(View.GONE);
                selectedTime = true;

            }

        } else {

            availableServicesDetailFragmentMoreTextview.setVisibility(View.GONE);
            selectedTime = true;

        }

        if (state) {
            if (mService.getTypePrice()) {
                mPriceMeterTag.setVisibility(View.VISIBLE);
                mPriceMeter.setVisibility(View.VISIBLE);
                mPriceMeter.setText(String.format("%d €", mService.getPrice()));
            } else {
                mPriceTag.setVisibility(View.VISIBLE);
                mPrice.setVisibility(View.VISIBLE);
                mPrice.setText(String.format("%d €", mService.getPrice()));
            }

        } else {

            if (mService.isVisibility()) {

                if (mService.getTypePrice()) {
                    mPriceMeterTag.setVisibility(View.VISIBLE);
                    mPriceMeter.setVisibility(View.VISIBLE);
                    mPriceMeter.setText(String.format("%d €", mService.getPrice()));
                } else {
                    mPriceTag.setVisibility(View.VISIBLE);
                    mPrice.setVisibility(View.VISIBLE);
                    mPrice.setText(String.format("%d €", mService.getPrice()));
                }

            }

        }

        if (mService.getNteProvider() != 0) {

            mNTE.setText(String.format("%d €", mService.getNteProvider()));
            mNTETag.setVisibility(View.VISIBLE);
            mNTE.setVisibility(View.VISIBLE);

        }

        mServName.setText(mService.getServiceName());
        mSubservice.setText(mService.getSubService());
        mPostalCode.setText(mService.getPostalCode());
        mClientComment.setText(mService.getComments());

        retrofit = RestClient.getRetrofitInstance();

        Calendar startDate = null, finishDate = null, startTime = null, finishTime = null;

        try {
            startDate = Famaex.dateToCalendar(Famaex.dateBackendFormat.parse(mService.getPreferedDates().get(0).getStartDate()));
            finishDate = Famaex.dateToCalendar(Famaex.dateBackendFormat.parse(mService.getPreferedDates().get(0).getEndDate()));
            startTime = Famaex.dateToCalendar(Famaex.timeServerAppFormatEnglish.parse(mService.getPreferedDates().get(0).getStartTime().substring(0, 8)));
            finishTime = Famaex.dateToCalendar(Famaex.timeServerAppFormatEnglish.parse(mService.getPreferedDates().get(0).getEndTime().substring(0, 8)));

            Date dateStart = Famaex.timeServerAppFormat.parse(mService.getPreferedDates().get(0).getStartTime().substring(0, 8));
            Date dateEnd = Famaex.timeServerAppFormat.parse(mService.getPreferedDates().get(0).getEndTime().substring(0, 8));

            String startTimeString = Famaex.timeAppFormat.format(dateStart);
            String endTimeString = Famaex.timeAppFormat.format(dateEnd);

            startTime = Famaex.dateToCalendar(Famaex.timeAppFormat.parse(startTimeString));
            finishTime = Famaex.dateToCalendar(Famaex.timeAppFormat.parse(endTimeString));

            avDetFrgDateStartHour.setText(Famaex.dateAppFormat.format(Famaex.dateBackendFormat.parse(mService.getPreferedDates().get(0).getStartDate())));
            avDetFrgDateFinishHour.setText(Famaex.dateAppFormat.format(Famaex.dateBackendFormat.parse(mService.getPreferedDates().get(0).getEndDate())));

            avDetFrgTimeStartHour.setText(mService.getPreferedDates().get(0).getStartTime().substring(0, 8));
            avDetFrgTimeFinishHour.setText(mService.getPreferedDates().get(0).getEndTime().substring(0, 8));


        } catch (ParseException e) {
            e.printStackTrace();


            avDetFrgDateStartHour.setText(getActivity().getApplicationContext().getResources().getString(R.string.completeDatePlaceholder));
            avDetFrgDateFinishHour.setText(getActivity().getApplicationContext().getResources().getString(R.string.completeDatePlaceholder));

            avDetFrgTimeStartHour.setText("");
            avDetFrgTimeFinishHour.setText("");

        }

        //availableServicesAdapter = new com.gabrielguzman.android.famaex.services.available.adapter.AvailableServicesAdapter(mService.getPreferedDates(), getActivity().getApplicationContext(), fragment);
        //availableServicesDetailFragmentRecyclerview.setAdapter(availableServicesAdapter);
        //availableServicesDetailFragmentRecyclerview.setLayoutManager(layoutManager);

        if (selectedTime == true) {

            setSpinnersValues(startDate, finishDate, startTime, finishTime);

        }

        mCustomDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mDateSelection.setVisibility(View.GONE);
                    mDateProposal.setVisibility(View.VISIBLE);
                } else {
                    mDateSelection.setVisibility(View.VISIBLE);
                    mDateProposal.setVisibility(View.GONE);
                }
            }
        });


        mPropDay = (TextView) view.findViewById(R.id.avDetFrgPropDay);
        mPropHour = (TextView) view.findViewById(R.id.avDetFrgPropHour);


        availableServicesDetailFragmentMoreTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (mService != null) {

                    if (mService.getPreferedDates() != null) {

                        if (mService.getPreferedDates().size() > 1) {

                            displayHoursDialog();


                        }

                    }

                }


            }
        });


        mPropDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar current = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.datePicker,
                        datePickerListener, current.get(Calendar.YEAR), current.get(Calendar.MONTH),
                        current.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });

        mPropHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar current = Calendar.getInstance();
                TimePickerDialog dialog = new TimePickerDialog(getActivity(), R.style.datePicker,
                        timePickerListener, current.get(Calendar.HOUR_OF_DAY), current.get(Calendar.MINUTE),
                        false);
                dialog.show();
            }
        });

        datePickerListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                mPropDay.setText(Famaex.dateAppFormat.format(cal.getTime()));
                mService.setDateSelected(Famaex.dateBackendFormat.format(cal.getTime()));
            }
        };

        timePickerListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar cal = new GregorianCalendar(2016, 8, 21, hourOfDay, minute);
                mPropHour.setText(Famaex.timeServerAppFormat.format(cal.getTime()));
                mService.setTimeSelected(Famaex.timeBackendFormat.format(cal.getTime()));
            }
        };

        va = view;

        mApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Boolean empty = false;

                if (mCustomDate.isChecked()) {

                    if (mPropDay.getText().toString().substring(0, 1).equals("S") ||
                            mPropHour.getText().toString().substring(0, 1).equals("S")) {
                        empty = true;
                    } else {
                        try {
                            mService.setDateSelected(Famaex.dateBackendFormat.format(
                                    Famaex.dateAppFormat.parse(mPropDay.getText().toString())));


                            mService.setTimeSelected(Famaex.timeBackendFormat.format(
                                    Famaex.timeServerAppFormat.parse(
                                            mPropHour.getText().toString())));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    try {
                        mService.setDateSelected(Famaex.dateBackendFormat.format(
                                Famaex.dateAppFormat.parse(mDaySpinner.getSelectedItem().toString())));
                        mService.setTimeSelected(Famaex.timeBackendFormat.format(
                                Famaex.timeServerAppFormat.parse(
                                        mHourSpinner.getSelectedItem().toString())));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (empty) {
                    Toast.makeText(getActivity(), "Debe seleccionar valores de fecha y hora", Toast.LENGTH_SHORT).show();
                } else {

                    if (selectedTime) {

                        getUsersProviders(mService, 1);

                    } else {

                        Toast.makeText(getActivity(), "Debe seleccionar valores de fecha y hora", Toast.LENGTH_SHORT).show();

                    }


                }
            }
        });

        mDiscardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AvailableServicesDetailFragment.this.dismiss();
            }
        });
    }


    public void applyService() {

        mASection = (LinearLayout) va.findViewById(R.id.avDetFrgASection);
        mSSection = (LinearLayout) va.findViewById(R.id.avDetFrgSuccessSection);
        mCloseButton = (Button) va.findViewById(R.id.avDetFrgCloseButton);
        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AvailableServicesDetailFragment.this.dismiss();
            }
        });


        mProgressOverlay = va.findViewById(R.id.progress_overlay);
        TextView progressMssge = (TextView) va.findViewById(R.id.progressMssg);
        progressMssge.setText(getActivity().getResources().getString(R.string.loading_sending_apply));
        Famaex.animateView(mASection, View.GONE, 0, 200);
        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        applyServiceRetry = 0;

        ServiceEndPointInterface serviceEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

        TakeService takeService = new TakeService(mUserPref.getInt(AppSharedPreferences.PROVIDER_SERVICE_SELECTED, 0),
                mService.getServiceRqID(),
                mService.getDateSelected(),
                mService.getTimeSelected());


        Call<com.gabrielguzman.android.famaex.web.models.Service> call = serviceEndPointInterface.applyService(RestClient.CONTENT_TYPE, takeService);
        call.enqueue(new Callback<com.gabrielguzman.android.famaex.web.models.Service>() {
            @Override
            public void onResponse(Call<com.gabrielguzman.android.famaex.web.models.Service> call, Response<com.gabrielguzman.android.famaex.web.models.Service> response) {

                if (response.body() != null) {

                    serviceResponse = response.body();

                    if (Utils.verifyOKTAG(serviceResponse, getActivity().getApplicationContext())) {


                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                        mSSection.setVisibility(View.VISIBLE);

                        mUserPref.edit().putInt(AppSharedPreferences.PROVIDER_SERVICE_SELECTED, 0).apply();

                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.apply_service_success_message), Toast.LENGTH_SHORT).show();


                    } else {

                        displayError(serviceResponse.getStatus());

                    }


                } else {

                    applyServiceRetry++;

                    if (applyServiceRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(getActivity().getResources().getString(R.string.apply_service_error_message));

                    }

                }

            }

            @Override
            public void onFailure(Call<com.gabrielguzman.android.famaex.web.models.Service> call, Throwable t) {

                applyServiceRetry++;

                if (applyServiceRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(getActivity().getResources().getString(R.string.apply_service_error_message));

                }

            }
        });


    }


    public void displayHoursDialog() {

        dialogViewHours = new Dialog(getActivity());
        dialogViewHours.setCancelable(true);
        dialogViewHours.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogViewHours.setContentView(R.layout.dialog_services_hours);
        RecyclerView dialogUsersProviders = (RecyclerView) dialogViewHours.findViewById(R.id.dialog_users_providers_recyclers_view);

        availableServicesAdapter = new com.gabrielguzman.android.famaex.services.available.adapter.AvailableServicesAdapter(mService.getPreferedDates(), getActivity().getApplicationContext(), fragment);
        dialogUsersProviders.setAdapter(availableServicesAdapter);
        dialogUsersProviders.setLayoutManager(new LinearLayoutManager(getActivity()));

        TextView dialogUsersProvidersCancel = (TextView) dialogViewHours.findViewById(R.id.dialog_users_providers_cancel);


        dialogUsersProvidersCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialogViewHours.dismiss();

            }
        });

        dialogViewHours.show();


    }

    @Override

    public void onResume() {

        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();

        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

        // Call super onResume after sizing
        super.onResume();

    }

    public void setSpinnersValues(Calendar startDate, Calendar finishDate,
                                  Calendar startTime, Calendar finishTime) {


        ArrayList<String> dates = new ArrayList<>();

        Calendar dat = startDate;


        while ((dat.after(startDate) || dat.equals(startDate)) &&
                (dat.before(finishDate) || dat.equals(finishDate))) {

            dates.add(dat.get(Calendar.DAY_OF_MONTH) + "/" + dat.getDisplayName(Calendar.MONTH, Calendar.SHORT, new Locale("es", "ES")) + "/" + dat.get(Calendar.YEAR));
            dat.add(Calendar.DAY_OF_MONTH, 1);

        }
        ArrayAdapter<String> daysAdapter =
                new ArrayAdapter<String>(getActivity(), R.layout.famaex_spinner_item, dates);
        mDaySpinner.setAdapter(daysAdapter);

        Calendar hou = startTime;
        ArrayList<String> hours = new ArrayList<>();
        ArrayList<String> hoursFormat = new ArrayList<>();


        while ((hou.after(startTime) || hou.equals(startTime)) &&
                (hou.before(finishTime) || hou.equals(finishTime))) {

            hours.add(hou.get(Calendar.HOUR_OF_DAY) + ":"
                    + (hou.get(Calendar.MINUTE) < 10 ? "0" + hou.get(Calendar.MINUTE) : hou.get(Calendar.MINUTE)));

            hou.add(Calendar.MINUTE, 30);

        }

        for (String hourArray : hours) {

            String[] hourSplit = hourArray.split(":");

            if (hourSplit[0].equals("0") || hourSplit[0].equals("1") || hourSplit[0].equals("2") || hourSplit[0].equals("3") || hourSplit[0].equals("4") || hourSplit[0].equals("5") || hourSplit[0].equals("6") || hourSplit[0].equals("7") || hourSplit[0].equals("8") || hourSplit[0].equals("9") || hourSplit[0].equals("10") || hourSplit[0].equals("11")) {

                hoursFormat.add(hourArray + " AM");

            } else if (hourSplit[0].equals("12")) {

                hoursFormat.add("12:" + hourSplit[1] + " PM");

            } else if (hourSplit[0].equals("13")) {

                hoursFormat.add("1:" + hourSplit[1] + " PM");

            } else if (hourSplit[0].equals("14")) {

                hoursFormat.add("2:" + hourSplit[1] + " PM");

            } else if (hourSplit[0].equals("15")) {

                hoursFormat.add("3:" + hourSplit[1] + " PM");

            } else if (hourSplit[0].equals("16")) {

                hoursFormat.add("4:" + hourSplit[1] + " PM");

            } else if (hourSplit[0].equals("17")) {

                hoursFormat.add("5:" + hourSplit[1] + " PM");

            } else if (hourSplit[0].equals("18")) {

                hoursFormat.add("6:" + hourSplit[1] + " PM");

            } else if (hourSplit[0].equals("19")) {

                hoursFormat.add("7:" + hourSplit[1] + " PM");

            } else if (hourSplit[0].equals("20")) {

                hoursFormat.add("8:" + hourSplit[1] + " PM");

            } else if (hourSplit[0].equals("21")) {

                hoursFormat.add("9:" + hourSplit[1] + " PM");

            } else if (hourSplit[0].equals("22")) {

                hoursFormat.add("10:" + hourSplit[1] + " PM");

            } else if (hourSplit[0].equals("23")) {

                hoursFormat.add("11:" + hourSplit[1] + " PM");

            }


        }

        ArrayAdapter<String> hoursAdapter =
                new ArrayAdapter<String>(getActivity(), R.layout.famaex_spinner_item, hoursFormat);
        mHourSpinner.setAdapter(hoursAdapter);

    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }

    private void displayError(String message) {

        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

        mUserPref.edit().putInt(AppSharedPreferences.PROVIDER_SERVICE_SELECTED, 0).apply();

        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        Famaex.animateView(mASection, View.VISIBLE, 1.0f, 200);

    }

    @Override
    public void onDateSlected(View view, int position) {

        Calendar startDate = null, finishDate = null, startTime = null, finishTime = null;

        try {

            startDate = Famaex.dateToCalendar(Famaex.dateBackendFormat.parse(mService.getPreferedDates().get(position).getStartDate()));
            finishDate = Famaex.dateToCalendar(Famaex.dateBackendFormat.parse(mService.getPreferedDates().get(position).getEndDate()));
            Date dateStart = Famaex.timeServerAppFormatEnglish.parse(mService.getPreferedDates().get(position).getStartTime().substring(0, 8));
            Date dateEnd = Famaex.timeServerAppFormatEnglish.parse(mService.getPreferedDates().get(position).getEndTime().substring(0, 8));

            String startTimeString = Famaex.timeAppFormat.format(dateStart);
            String endTimeString = Famaex.timeAppFormat.format(dateEnd);


            startTime = Famaex.dateToCalendar(Famaex.timeAppFormat.parse(startTimeString));
            finishTime = Famaex.dateToCalendar(Famaex.timeAppFormat.parse(endTimeString));

            avDetFrgDateStartHour.setText(Famaex.dateAppFormat.format(Famaex.dateBackendFormat.parse(mService.getPreferedDates().get(position).getStartDate())));
            avDetFrgDateFinishHour.setText(Famaex.dateAppFormat.format(Famaex.dateBackendFormat.parse(mService.getPreferedDates().get(position).getEndDate())));

            avDetFrgTimeStartHour.setText(mService.getPreferedDates().get(position).getStartTime().substring(0, 8));
            avDetFrgTimeFinishHour.setText(mService.getPreferedDates().get(position).getEndTime().substring(0, 8));


        } catch (ParseException e) {
            e.printStackTrace();

            avDetFrgDateStartHour.setText(getActivity().getApplicationContext().getResources().getString(R.string.completeDatePlaceholder));
            avDetFrgDateFinishHour.setText(getActivity().getApplicationContext().getResources().getString(R.string.completeDatePlaceholder));

            avDetFrgTimeStartHour.setText("");
            avDetFrgTimeFinishHour.setText("");

        }

        selectedTime = true;
        availableServicesDetailFragmentItemContainerHour.setVisibility(View.VISIBLE);

        availableServicesAdapter.updateDateSelection(position);

        dialogViewHours.dismiss();

        setSpinnersValues(startDate, finishDate, startTime, finishTime);

    }

    @Override
    public void onUserSelected(View view, int position) {

        usersProvidersApdater.updateElements(position);

    }
}
