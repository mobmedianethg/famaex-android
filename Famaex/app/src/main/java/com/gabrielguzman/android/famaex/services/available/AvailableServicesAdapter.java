package com.gabrielguzman.android.famaex.services.available;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.services.available.interfaces.UsersProvidersInterface;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Custom adapter to display the lists of Services.
 *
 * @author Gabriel Guzmán
 * @see BaseAdapter
 * @since 2016.25.06
 */
public class AvailableServicesAdapter extends BaseAdapter {

    private static ArrayList<Service> searchArrayList;
    private LayoutInflater mInflater;
    private String mListIndicator;
    private SharedPreferences mUserPref;
    private int[] mViewIDs;
    private int mLayout;
    private Context mContext;

    private AvailableServicesActivity activity;
    private int[] avCardViewIDs = {R.id.serviceName, R.id.serviceDateRequested,
            R.id.actServicePostalCode, R.id.servicePriority, R.id.actServicePrice};
    private int[] avActViewIDs = {R.id.actServiceName, R.id.actServiceDateRequested,
            R.id.actServicePostalCode, R.id.actServicePriority, R.id.actServicePrice,
            R.id.actSubserviceName};
    private UpdateCallback callback;

    public AvailableServicesAdapter(Context context, ArrayList<Service> results, String ind, AvailableServicesActivity activity) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
        mListIndicator = ind;
        mContext = context;
        this.activity = activity;

        mViewIDs = (ind.equals("A")) ? avActViewIDs : avCardViewIDs;
        mLayout = (ind.equals("A")) ? R.layout.available_services_activity_item : R.layout.available_services_card_item;
    }


    @Override
    public int getCount() {
        return searchArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(mLayout, parent, false);
            holder = new ViewHolder();
            holder.txtServiceName = (TextView) convertView.findViewById(mViewIDs[0]);
            holder.txtDateRequested = (TextView) convertView.findViewById(mViewIDs[1]);
            holder.txtPostalCode = (TextView) convertView.findViewById(mViewIDs[2]);
            holder.txtPriority = (ImageView) convertView.findViewById(mViewIDs[3]);
            holder.txtPrice = (TextView) convertView.findViewById(mViewIDs[4]);
            if (mListIndicator.equals("A"))
                holder.txtSubserviceName = (TextView) convertView.findViewById(mViewIDs[5]);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Service serv = searchArrayList.get(position);

        Calendar sDate = null, fDate = null;
        try {
            sDate = Famaex.dateToCalendar(Famaex.dateBackendFormat.parse(serv.getDateStart()));
            fDate = Famaex.dateToCalendar(Famaex.dateBackendFormat.parse(serv.getDateFinish()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.txtServiceName.setText(serv.getServiceName());
        holder.txtDateRequested.setText(String.format("Entre el %s/%s/%s y %s/%s/%s",
                sDate.get(Calendar.DAY_OF_MONTH), sDate.get(Calendar.MONTH) + 1, sDate.get(Calendar.YEAR),
                fDate.get(Calendar.DAY_OF_MONTH), fDate.get(Calendar.MONTH) + 1, fDate.get(Calendar.YEAR)));
        holder.txtPostalCode.setText(String.format("Código Postal: %s", serv.getPostalCode()));

        if (serv.getPriority().equals("Urgente"))
            holder.txtPriority.setVisibility(View.VISIBLE);
        else
            holder.txtPriority.setVisibility(View.GONE);

        //holder.txtPriority.setText(serv.getPriority());
        holder.txtPrice.setText(String.format("%d €", serv.getPrice()));

        if (mListIndicator.equals("A"))
            holder.txtSubserviceName.setText(serv.getSubService());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showEditDialog(serv);


            }
        });
        return convertView;
    }

    private void showEditDialog(Service serv) {

        FragmentManager fm = ((Activity) mContext).getFragmentManager();
        AvailableServicesDetailFragment availableDialogFragment =
                AvailableServicesDetailFragment.newInstance("Aplicar al servicio", serv);

        availableDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (callback != null) {
                    callback.wonDialogDismissed();
                }
            }
        });

        availableDialogFragment.show(fm, "fragment_av_service_activity");

    }

    public void setCallback(UpdateCallback callback) {
        this.callback = callback;
    }

    public interface UpdateCallback {
        public void wonDialogDismissed();
    }


    static class ViewHolder {
        TextView txtServiceName;
        TextView txtDateRequested;
        TextView txtPostalCode;
        ImageView txtPriority;
        TextView txtSubserviceName;
        TextView txtPrice;

    }
}
