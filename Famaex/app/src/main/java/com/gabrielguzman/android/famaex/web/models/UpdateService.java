package com.gabrielguzman.android.famaex.web.models;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by jose on 07/04/17.
 */

public class UpdateService {

    private Integer service_request_id;
    private Integer status_id;
    private ArrayList<File> image;
    private String comment_provider;
    private Integer material_costs;
    private Integer technical_price;
    private Integer assistant_price;

    public UpdateService() {
    }

    public UpdateService(Integer service_request_id, Integer status_id, ArrayList<File> image, String comment_provider, Integer material_costs, Integer technical_price, Integer assistant_price) {
        this.service_request_id = service_request_id;
        this.status_id = status_id;
        this.image = image;
        this.comment_provider = comment_provider;
        this.material_costs = material_costs;
        this.technical_price = technical_price;
        this.assistant_price = assistant_price;
    }

    public Integer getService_request_id() {
        return service_request_id;
    }

    public void setService_request_id(Integer service_request_id) {
        this.service_request_id = service_request_id;
    }

    public Integer getStatus_id() {
        return status_id;
    }

    public void setStatus_id(Integer status_id) {
        this.status_id = status_id;
    }

    public ArrayList<File> getImage() {
        return image;
    }

    public void setImage(ArrayList<File> image) {
        this.image = image;
    }

    public String getComment_provider() {
        return comment_provider;
    }

    public void setComment_provider(String comment_provider) {
        this.comment_provider = comment_provider;
    }

    public Integer getMaterial_costs() {
        return material_costs;
    }

    public void setMaterial_costs(Integer material_costs) {
        this.material_costs = material_costs;
    }

    public Integer getTechnical_price() {
        return technical_price;
    }

    public void setTechnical_price(Integer technical_price) {
        this.technical_price = technical_price;
    }

    public Integer getAssistant_price() {
        return assistant_price;
    }

    public void setAssistant_price(Integer assistant_price) {
        this.assistant_price = assistant_price;
    }
}
