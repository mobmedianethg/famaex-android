package com.gabrielguzman.android.famaex.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.web.models.Approved;
import com.gabrielguzman.android.famaex.web.models.DetailResponse;
import com.gabrielguzman.android.famaex.web.models.History;
import com.gabrielguzman.android.famaex.web.models.NteResponse;
import com.gabrielguzman.android.famaex.web.models.Service;
import com.gabrielguzman.android.famaex.web.models.SpecializationsResponse;
import com.gabrielguzman.android.famaex.web.models.TariffResponse;
import com.gabrielguzman.android.famaex.web.models.TokenResult;
import com.gabrielguzman.android.famaex.web.models.UpdateResponse;
import com.gabrielguzman.android.famaex.web.models.User;
import com.gabrielguzman.android.famaex.web.models.UserRegisterResponse;
import com.gabrielguzman.android.famaex.web.models.UsersProvidersResponse;

import java.io.ByteArrayOutputStream;
import java.util.Date;

/**
 * Created by jose on 29/03/17.
 */

public class Utils {

    public static final String LOG_TAG = "Utils";
    public static final int LOGIN_USER = 1;
    public static final int RECOVER_USER = 2;
    public static final int CHANGE_USER = 3;
    public static final String PLATFORM_VALUE = "android";
    public static final String HOURS_TAG = "SI";
    public static final String URGENT_TAG = "Urgente";
    public static final String FAMAEX_AVAILABLE_SERIAZABLE = "avServices";
    public static final String FAMAEX_FINISHED_SERIAZABLE = "compServices";
    public static final String FAMAEX_WON_SERIAZABLE = "wnServices";
    public static final String RESUME_ACTIVITY = "onResume";

    public static final String TITTLE_TAG = "title";
    public static final String ALERT_TAG = "Alerta";
    public static final String SERVICE_TAG = "service";

    public static final String NO_TAG = "NO";

    public static final String ACCEPTED_SERVICE_VALUE = "Aceptado";
    public static final String ON_WAY_SERVICE_VALUE = "En camino";
    public static final String ON_PROCESS_SERVICE_VALUE = "En proceso";
    public static final String COMPLETED_SERVICE_VALUE = "Finalizado";
    public static final String CANCELED_SERVICE_VALUE = "Cancelado";

    public static final String IMAGE_ARRAY_TAG = "image[]";


    public static void setPortaitOrientation(Activity activity) {
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

    }


    public static  int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    public static int compareDates(String startDate, String endDate) {

        int result = 0;

        try {

            Log.v(LOG_TAG, "startDate " + startDate);
            Log.v(LOG_TAG, "endDate " + endDate);

            String[] startDateSplit = startDate.split("-");
            String[] endDateSplit = endDate.split("-");

            Date startDateDate = new Date(Integer.parseInt(startDateSplit[2]), Integer.parseInt(startDateSplit[1]) - 1, Integer.parseInt(startDateSplit[0]));
            Date endDateDate = new Date(Integer.parseInt(endDateSplit[2]), Integer.parseInt(endDateSplit[1]) - 1, Integer.parseInt(endDateSplit[0]));

            if (startDateDate.compareTo(endDateDate)>0) {
                Log.v(LOG_TAG, "Date1 is after Date2");
                result = 1;
            }

            if (startDateDate.compareTo(endDateDate)<0) {
                Log.v(LOG_TAG, "Date1 is before Date2");
                result = 2;
            }

            if (startDateDate.equals(endDateDate)) {
                Log.v(LOG_TAG, "Date1 is equal Date2");
                result = 0;

            }


            Log.v(LOG_TAG, "-----------------------------");

        } catch (Exception e) {

            Log.e(LOG_TAG, e.getLocalizedMessage());

        }

        return result;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static String getRealPathFromURI(Uri uri, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static boolean verifyEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean verifyOKTAG(User result, int option, Activity activity) {

        boolean response;


        if ((result.getStatus().equals(activity.getResources().getString(R.string.global_success))) && (option == LOGIN_USER)) {

            response = true;

        } else if ((result.getStatus().equals(activity.getResources().getString(R.string.global_recover_success))) && (option == RECOVER_USER)) {

            response = true;

        } else if ((result.getStatus().equals(activity.getResources().getString(R.string.global_change_success))) && (option == CHANGE_USER)) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }

    public static boolean verifyOKTAG(UsersProvidersResponse result, Activity activity) {

        boolean response;


        if (result.getStatus().equals(activity.getResources().getString(R.string.global_success))) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }

    public static boolean verifyOKTAG(TariffResponse result, Activity activity) {

        boolean response;


        if (result.getStatus().equals(activity.getResources().getString(R.string.global_success)) || (result.getStatus().equals(activity.getResources().getString(R.string.global_success_visibility)))) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }

    public static boolean verifyOKTAG(SpecializationsResponse result, Activity activity) {

        boolean response;


        if (result.getStatus().equals(activity.getResources().getString(R.string.specializations_avalaible_success)) || (result.getStatus().equals(activity.getResources().getString(R.string.specializations_avalaible_user_success))) || (result.getStatus().equals(activity.getResources().getString(R.string.update_specializations_avalaible_user_success)))) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }


    public static boolean verifyOKTAG(UserRegisterResponse result, Activity activity) {

        boolean response;


        if (result.getStatus().equals(activity.getResources().getString(R.string.global_success))) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }


    public static boolean verifyOKTAG(DetailResponse result, Activity activity) {

        boolean response;


        if (result.getStatus().equals(activity.getResources().getString(R.string.global_success))) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }


    public static boolean verifyOKTAG(Approved result, Activity activity) {

        boolean response;


        if (result.getStatus().equals(activity.getResources().getString(R.string.global_success))) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }


    public static boolean verifyOKTAG(NteResponse result, Activity activity) {

        boolean response;


        if (result.getStatus().equals(activity.getResources().getString(R.string.global_success))) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }


    public static boolean verifyOKTAG(UpdateResponse result, Activity activity) {

        boolean response;


        if (result.getStatus().equals(activity.getResources().getString(R.string.global_success_update))) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }


    public static boolean verifyOKTAG(TokenResult result, Context context) {

        boolean response;

        if ((result.getStatus().equals(context.getResources().getString(R.string.global_success))) || (result.getStatus().equals(context.getResources().getString(R.string.logout_message_success)))) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }


    public static boolean verifyOKTAG(Service result, Context context) {

        boolean response;

        if ((result.getStatus().equals(context.getResources().getString(R.string.global_success))) || (result.getStatus().equals(context.getResources().getString(R.string.apply_succes_tag)))) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }

    public static boolean verifyOKTAG(History result, Context context) {

        boolean response;

        if (result.getStatus().equals(context.getResources().getString(R.string.global_success))) {

            response = true;

        } else {

            response = false;

        }

        return response;
    }

    public static boolean noContainsNumbers(String str) {

        boolean result = true;

        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                result = false;
                break;
            }
        }
        return result;
    }

    public static boolean containsOnlyNumbers(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i)))
                return false;
        }
        return true;
    }


}
