package com.gabrielguzman.android.famaex.web.models;

import java.util.ArrayList;

/**
 * Created by jose on 05/04/17.
 */

public class DataDetail {

    public static final String DATE_ARRIVAL_DATE = "arrival date provider";
    public static final String TIME_ARRIVAL_DATE = "arrival time provider";
    private String cif;
    private String company;
    private String client_name;
    private String client_phone;
    private String client_address;
    private String codigo_postal;
    private Boolean urgent;
    private ArrayList<String> preference_selected;
    private String service_status;
    private Integer service_request_id;
    private String service;
    private String subservice;
    private String service_per_mts2;
    private Double price_hour_technical_provider;
    private Double price_hour_assistant_provider;
    private Double price_hour_technical_client;
    private Double price_hour_assistant_client;
    private String comment_client;
    private Integer nte_provider;
    private Integer nte_client;
    private Double mts2_place;
    private Double price_mts2;
    private Double total_price_mt2;
    private Double price_provider;
    private String comment_provider;
    private String rating_comment;
    private String dateArrivalDate;
    private String timeArrivalDate;
    private String visibility_tariff;

    public DataDetail() {
    }

    public DataDetail(String cif, String company, String client_name, String client_phone, String client_address, String codigo_postal, Boolean urgent, ArrayList<String> preference_selected, String service_status, Integer service_request_id, String service, String subservice, String service_per_mts2, Double price_hour_technical_provider, Double price_hour_assistant_provider, Double price_hour_technical_client, Double price_hour_assistant_client, String comment_client, Integer nte_provider, Integer nte_client, Double mts2_place, Double price_mts2, Double total_price_mt2, Double price_provider, String comment_provider, String rating_comment, String dateArrivalDate, String timeArrivalDate, String visibility_tariff) {
        this.cif = cif;
        this.company = company;
        this.client_name = client_name;
        this.client_phone = client_phone;
        this.client_address = client_address;
        this.codigo_postal = codigo_postal;
        this.urgent = urgent;
        this.preference_selected = preference_selected;
        this.service_status = service_status;
        this.service_request_id = service_request_id;
        this.service = service;
        this.subservice = subservice;
        this.service_per_mts2 = service_per_mts2;
        this.price_hour_technical_provider = price_hour_technical_provider;
        this.price_hour_assistant_provider = price_hour_assistant_provider;
        this.price_hour_technical_client = price_hour_technical_client;
        this.price_hour_assistant_client = price_hour_assistant_client;
        this.comment_client = comment_client;
        this.nte_provider = nte_provider;
        this.nte_client = nte_client;
        this.mts2_place = mts2_place;
        this.price_mts2 = price_mts2;
        this.total_price_mt2 = total_price_mt2;
        this.price_provider = price_provider;
        this.comment_provider = comment_provider;
        this.rating_comment = rating_comment;
        this.dateArrivalDate = dateArrivalDate;
        this.timeArrivalDate = timeArrivalDate;
        this.visibility_tariff = visibility_tariff;
    }

    public String getClient_phone() {
        return client_phone;
    }

    public void setClient_phone(String client_phone) {
        this.client_phone = client_phone;
    }

    public String getClient_address() {
        return client_address;
    }

    public void setClient_address(String client_address) {
        this.client_address = client_address;
    }

    public String getService_status() {
        return service_status;
    }

    public void setService_status(String service_status) {
        this.service_status = service_status;
    }

    public Double getPrice_hour_technical_provider() {
        return price_hour_technical_provider;
    }

    public void setPrice_hour_technical_provider(Double price_hour_technical_provider) {
        this.price_hour_technical_provider = price_hour_technical_provider;
    }

    public Double getPrice_hour_assistant_provider() {
        return price_hour_assistant_provider;
    }

    public void setPrice_hour_assistant_provider(Double price_hour_assistant_provider) {
        this.price_hour_assistant_provider = price_hour_assistant_provider;
    }

    public Double getPrice_hour_technical_client() {
        return price_hour_technical_client;
    }

    public void setPrice_hour_technical_client(Double price_hour_technical_client) {
        this.price_hour_technical_client = price_hour_technical_client;
    }

    public Double getPrice_hour_assistant_client() {
        return price_hour_assistant_client;
    }

    public void setPrice_hour_assistant_client(Double price_hour_assistant_client) {
        this.price_hour_assistant_client = price_hour_assistant_client;
    }

    public Double getPrice_provider() {
        return price_provider;
    }

    public void setPrice_provider(Double price_provider) {
        this.price_provider = price_provider;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getCodigo_postal() {
        return codigo_postal;
    }

    public void setCodigo_postal(String codigo_postal) {
        this.codigo_postal = codigo_postal;
    }

    public Boolean getUrgent() {
        return urgent;
    }

    public void setUrgent(Boolean urgent) {
        this.urgent = urgent;
    }

    public ArrayList<String> getPreference_selected() {
        return preference_selected;
    }

    public void setPreference_selected(ArrayList<String> preference_selected) {
        this.preference_selected = preference_selected;
    }

    public Integer getService_request_id() {
        return service_request_id;
    }

    public void setService_request_id(Integer service_request_id) {
        this.service_request_id = service_request_id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getSubservice() {
        return subservice;
    }

    public void setSubservice(String subservice) {
        this.subservice = subservice;
    }

    public String getService_per_mts2() {
        return service_per_mts2;
    }

    public void setService_per_mts2(String service_per_mts2) {
        this.service_per_mts2 = service_per_mts2;
    }

    public String getComment_client() {
        return comment_client;
    }

    public void setComment_client(String comment_client) {
        this.comment_client = comment_client;
    }

    public Integer getNte_provider() {
        return nte_provider;
    }

    public void setNte_provider(Integer nte_provider) {
        this.nte_provider = nte_provider;
    }

    public Integer getNte_client() {
        return nte_client;
    }

    public void setNte_client(Integer nte_client) {
        this.nte_client = nte_client;
    }

    public Double getMts2_place() {
        return mts2_place;
    }

    public void setMts2_place(Double mts2_place) {
        this.mts2_place = mts2_place;
    }

    public Double getPrice_mts2() {
        return price_mts2;
    }

    public void setPrice_mts2(Double price_mts2) {
        this.price_mts2 = price_mts2;
    }

    public Double getTotal_price_mt2() {
        return total_price_mt2;
    }

    public void setTotal_price_mt2(Double total_price_mt2) {
        this.total_price_mt2 = total_price_mt2;
    }

    public String getComment_provider() {
        return comment_provider;
    }

    public void setComment_provider(String comment_provider) {
        this.comment_provider = comment_provider;
    }

    public String getRating_comment() {
        return rating_comment;
    }

    public void setRating_comment(String rating_comment) {
        this.rating_comment = rating_comment;
    }

    public String getDateArrivalDate() {
        return dateArrivalDate;
    }

    public void setDateArrivalDate(String dateArrivalDate) {
        this.dateArrivalDate = dateArrivalDate;
    }

    public String getTimeArrivalDate() {
        return timeArrivalDate;
    }

    public void setTimeArrivalDate(String timeArrivalDate) {
        this.timeArrivalDate = timeArrivalDate;
    }

    public String getVisibility_tariff() {
        return visibility_tariff;
    }

    public void setVisibility_tariff(String visibility_tariff) {
        this.visibility_tariff = visibility_tariff;
    }
}
