package com.gabrielguzman.android.famaex.status;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.dashboard.DashboardActivity;
import com.gabrielguzman.android.famaex.models.Picture;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.services.won.WonServicesActivity;
import com.gabrielguzman.android.famaex.status.adapter.RecyclerViewPicturesAdapter;
import com.gabrielguzman.android.famaex.status.interfaces.EditPhotoStatusChangeInterface;
import com.gabrielguzman.android.famaex.utils.Famaex;
import com.gabrielguzman.android.famaex.utils.ImagePicker;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.ServiceEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.NteRequest;
import com.gabrielguzman.android.famaex.web.models.NteResponse;
import com.gabrielguzman.android.famaex.web.models.UpdateResponse;
import com.gabrielguzman.android.famaex.web.models.UpdateService;

import java.io.File;
import java.text.Normalizer;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Shows a full screen dialog for the service culmination confirmation
 *
 * @author Gabriel Guzmán
 * @see DialogFragment
 * @since 2016.31.07
 */
public class StsChangeQuestionFragment extends DialogFragment implements EditPhotoStatusChangeInterface, View.OnClickListener {

    public static final String LOG_TAG = "StsChangeQuestionFragment";
    //This number represents the requestCode for the startActivityForResult
    private static final int PICTURE_TAKE = 5001;
    private static final int PICK_FIRST_IMAGE_ID = 1989;
    private static final int PICK_SECOND_IMAGE_ID = 1996;
    private static int updateRetry;
    private static int nteExcededRetry;
    private NteResponse nteResponse;
    public Retrofit retrofit;
    private TextView mTitle;
    private TextView mPhotoTitle;
    private EditText mTechHours;
    private EditText mHelpHours;
    private EditText mMatCost;
    private EditText mProviderComment;
    private Button mYesButton;
    private Button mNotYetButton;
    private Button mProblemButton;
    private Button mReturnButton;
    private Button mUpdatebutton;
    private LinearLayout mButtonSection;
    private LinearLayout mContactSection;
    private LinearLayout mPhotoSection;
    private LinearLayout mExtraFieldsSection;
    private LinearLayout mUpdButSection;
    private LinearLayout mProgressOverlay;
    private ScrollView mScrollArea;
    private int pictureTime;
    private int mStatus;
    private int mSRID;
    private StsChangeQuestionFragment fragment;
    private RecyclerView stsChangeQuestionFragmentRecyclerview;
    private ImageView qsPhoto;
    private File fileFirst;
    private File fileSecond;
    private ArrayList<Picture> pictures;
    private Uri uriImageFirst;
    private Uri uriImageSecond;
    private UpdateResponse updateResponse;
    private View mView;
    private int resumeActivity;
    private View.OnClickListener mDismissListener = new View.OnClickListener() {
        public void onClick(View v) {
            StsChangeQuestionFragment.this.dismiss();
        }
    };
    private boolean mPhotoLoaded = false;
    private DialogInterface.OnDismissListener onDismissListener;
    private int positionToEdit;
    private RecyclerViewPicturesAdapter recyclerViewPicturesAdapter;
    private LinearLayoutManager layoutManager;

    public StsChangeQuestionFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static StsChangeQuestionFragment newInstance(String title, Service serv, int resume) {
        StsChangeQuestionFragment frag = new StsChangeQuestionFragment();
        Bundle args = new Bundle();
        args.putString(Utils.TITTLE_TAG, title);
        args.putSerializable(Utils.SERVICE_TAG, serv);
        args.putInt(Utils.RESUME_ACTIVITY, resume);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        return inflater.inflate(R.layout.sts_change_question_fragment, container);
    }

    public void updateNteExceded(int serviceRequestId, int hoursToWork, int quantityOfTechnicals, int quantityOfAssitants, int materialCostEstimated) {

        nteExcededRetry = 0;
        ServiceEndPointInterface serviceEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

        final NteRequest nteRequest = new NteRequest(serviceRequestId, hoursToWork, quantityOfTechnicals, quantityOfAssitants, materialCostEstimated);


        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        Call<NteResponse> call = serviceEndPointInterface.updateNteExceded(RestClient.CONTENT_TYPE, nteRequest);
        call.enqueue(new Callback<NteResponse>() {
            @Override
            public void onResponse(Call<NteResponse> call, Response<NteResponse> response) {

                if (response.body() != null) {

                    nteResponse = response.body();

                    if (Utils.verifyOKTAG(nteResponse, getActivity())) {


                        /*
                        * When NTE RESPONSE OK TODO
                         */


                    } else {

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                        Toast.makeText(getActivity(), nteResponse.getStatus(), Toast.LENGTH_SHORT).show();

                    }


                } else {

                    nteExcededRetry++;

                    if (nteExcededRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                    }

                }

            }

            @Override
            public void onFailure(Call<NteResponse> call, Throwable t) {

                nteExcededRetry++;

                if (nteExcededRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                }

            }
        });


    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        mTitle = (TextView) view.findViewById(R.id.qsTitle);
        mPhotoTitle = (TextView) view.findViewById(R.id.qsPhotoTitle);
        mYesButton = (Button) view.findViewById(R.id.qsYesButton);
        mNotYetButton = (Button) view.findViewById(R.id.qsNotYetButton);
        mReturnButton = (Button) view.findViewById(R.id.qsBack);
        mUpdatebutton = (Button) view.findViewById(R.id.qsUpdateStatus);
        mProblemButton = (Button) view.findViewById(R.id.qsProblemButton);
        mButtonSection = (LinearLayout) view.findViewById(R.id.qsButtonSection);
        mContactSection = (LinearLayout) view.findViewById(R.id.qsContactSection);
        mPhotoSection = (LinearLayout) view.findViewById(R.id.qsPhotoSection);
        mExtraFieldsSection = (LinearLayout) view.findViewById(R.id.qsExtraFieldsSection);
        mUpdButSection = (LinearLayout) view.findViewById(R.id.qsUpdateBtSection);
        mProgressOverlay = (LinearLayout) view.findViewById(R.id.qsStsChgProgressBar);
        mScrollArea = (ScrollView) view.findViewById(R.id.qsScrollArea);

        stsChangeQuestionFragmentRecyclerview = (RecyclerView) view.findViewById(R.id.sts_change_question_fragment_recyclerview);
        qsPhoto = (ImageView) view.findViewById(R.id.qsPhoto);

        pictures = new ArrayList<Picture>();

        recyclerViewPicturesAdapter = new RecyclerViewPicturesAdapter(pictures, getActivity().getApplicationContext(), this);
        stsChangeQuestionFragmentRecyclerview.setAdapter(recyclerViewPicturesAdapter);

        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        stsChangeQuestionFragmentRecyclerview.setLayoutManager(layoutManager);


        qsPhoto.setOnClickListener(this);

        mView = view;


        resumeActivity = getArguments().getInt(Utils.RESUME_ACTIVITY);

        retrofit = RestClient.getRetrofitInstance();

        // Fetch arguments from bundle and set title
        String title = getArguments().getString(Utils.TITTLE_TAG, Utils.ALERT_TAG);
        Service service = (Service) getArguments().getSerializable(Utils.SERVICE_TAG);

        fragment = this;

        if (service != null) {
            mStatus = service.getStatusID();
            mSRID = service.getServiceRqID();
        }

        switch (mStatus) {
            case 2:
                mTitle.setText(R.string.onWayTitle);
                break;
            case 3:
                mTitle.setText(R.string.arrivedTitle);
                break;
            case 4:
                break;
            default:
                break;
        }

        mYesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mButtonSection.setVisibility(View.GONE);

                switch (mStatus) {
                    case 2:
                        updateStatus();
                        break;
                    case 3:
                        mScrollArea.setVisibility(View.VISIBLE);
                        mPhotoSection.setVisibility(View.VISIBLE);

                        mUpdButSection.setVisibility(View.VISIBLE);

                        break;

                    case 4:
                        mScrollArea.setVisibility(View.VISIBLE);
                        mPhotoSection.setVisibility(View.VISIBLE);

                        mExtraFieldsSection.setVisibility(View.VISIBLE);
                        mUpdButSection.setVisibility(View.VISIBLE);
                        mTechHours = (EditText) mView.findViewById(R.id.qsTechHours);
                        mHelpHours = (EditText) mView.findViewById(R.id.qsHelpHours);
                        mMatCost = (EditText) mView.findViewById(R.id.qsMaterialsPrice);
                        mProviderComment = (EditText) mView.findViewById(R.id.qsProviderComment);
                        mPhotoTitle.setText(getActivity().getResources().getString(R.string.complete_picture_tag));
                        break;
                    default:
                        break;
                }


                mUpdatebutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mPhotoLoaded) {
                            switch (mStatus) {
                                case 3:
                                    updateStatus();
                                    break;
                                case 4:
                                    if ((mTechHours.getText().length() > 0) && (mHelpHours.getText().length() > 0) &&
                                            (mMatCost.getText().length() > 0)) {
                                        updateStatus();
                                    } else {
                                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.aditional_costs_required), Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                default:
                                    break;
                            }
                        } else {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.required_picture), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                mReturnButton.setOnClickListener(mDismissListener);

            }
        });

        mNotYetButton.setOnClickListener(mDismissListener);

        mProblemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mButtonSection.setVisibility(View.GONE);
                mContactSection.setVisibility(View.VISIBLE);
            }
        });


    }

    @Override
    public void onResume() {

        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();

        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

        // Call super onResume after sizing
        super.onResume();

    }

    /**
     * Method that shows a list of apps to select from and pick a photo.
     */
    public void onPickImage(int position) {

        positionToEdit = position;

        pictureTime = 0;

        Intent chooseImageIntent = ImagePicker.getPickImageIntent(getActivity());
        startActivityForResult(chooseImageIntent, PICTURE_TAKE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (pictureTime == 0) {

            switch (requestCode) {

                case (PICTURE_TAKE):

                    if (resultCode != 0) {

                        pictureTime = 1;

                        mPhotoLoaded = true;
                        Bitmap bitmap = ImagePicker.getImageFromResult(getActivity(), resultCode, data);

                        if (positionToEdit == -1) {

                            Picture newPicture = new Picture(bitmap);
                            pictures.add(newPicture);

                            recyclerViewPicturesAdapter.notifyDataSetChanged();


                        } else {

                            pictures.get(positionToEdit).setBitmapImage(bitmap);
                            recyclerViewPicturesAdapter.updateElements(positionToEdit, bitmap);

                            positionToEdit = -1;

                        }

                    }


                    break;
            }


        }
    }

    public void updateStatus() {

        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        UpdateService updateService = new UpdateService();

        String matCost = null;
        String techHours = null;
        String helpHours = null;
        String provComment = null;

        if (mStatus == 4) {
            matCost = mMatCost.getText().toString();
            techHours = mTechHours.getText().toString();
            helpHours = mHelpHours.getText().toString();
            provComment = Normalizer.normalize(mProviderComment.getText().toString(), Normalizer.Form.NFD)
                    .replaceAll("[^\\p{ASCII}]", "");
        }


        if (matCost != null) {

            updateService.setMaterial_costs(Integer.parseInt(matCost));

        } else {

            updateService.setMaterial_costs(null);

        }


        if (techHours != null) {

            updateService.setTechnical_price(Integer.parseInt(techHours));

        } else {

            updateService.setTechnical_price(null);

        }

        if (helpHours != null) {

            updateService.setAssistant_price(Integer.parseInt(helpHours));

        } else {

            updateService.setAssistant_price(null);

        }

        if (provComment != null) {

            updateService.setComment_provider(provComment);

        } else {

            updateService.setMaterial_costs(null);

        }


        if (pictures == null) {

            updateService.setImage(null);

        }

        updateService.setService_request_id(mSRID);
        updateService.setStatus_id(mStatus + 1);

        updateRetry = 0;

        if (updateService.getStatus_id() == 3) {

            ServiceEndPointInterface serviceEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

            Call<UpdateResponse> call = serviceEndPointInterface.updateService(RestClient.CONTENT_TYPE, updateService);
            call.enqueue(new Callback<UpdateResponse>() {
                @Override
                public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {

                    if (response.body() != null) {

                        updateResponse = response.body();

                        if (Utils.verifyOKTAG(updateResponse, getActivity())) {

                            if (resumeActivity == 1) {

                                DashboardActivity activity = (DashboardActivity) getActivity();
                                activity.updateData();

                            } else {

                                WonServicesActivity activity = (WonServicesActivity) getActivity();
                                activity.updateData();

                            }

                            StsChangeQuestionFragment.this.dismiss();


                        } else {

                            Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                            Toast.makeText(getActivity(), updateResponse.getStatus(), Toast.LENGTH_SHORT).show();

                        }


                    } else {

                        updateRetry++;

                        if (updateRetry < 3) {

                            call.clone().enqueue(this);

                        } else {

                            Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                        }

                    }

                }

                @Override
                public void onFailure(Call<UpdateResponse> call, Throwable t) {

                    updateRetry++;

                    if (updateRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                    }

                }
            });

        } else if (updateService.getStatus_id() == 4) {


            ServiceEndPointInterface serviceEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

            ArrayList<MultipartBody.Part> arrayPics = new ArrayList<MultipartBody.Part>();

            for (Picture fileUpload : pictures) {

                Uri uriPicture = Utils.getImageUri(getActivity().getApplicationContext(), fileUpload.getBitmapImage());
                String filePath = Utils.getRealPathFromURI(uriPicture, getActivity());

                File filePicture = new File(filePath);

                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(getActivity().getContentResolver().getType(uriPicture)),
                                filePicture
                        );

                MultipartBody.Part body =
                        MultipartBody.Part.createFormData(Utils.IMAGE_ARRAY_TAG, filePicture.getName(), requestFile);

                arrayPics.add(body);


            }

            String serviceRequestIdString = String.valueOf(updateService.getService_request_id());
            RequestBody serviceRequestId =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, serviceRequestIdString);

            String statusIdString = String.valueOf(updateService.getStatus_id());
            RequestBody statusId =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, statusIdString);


            Call<UpdateResponse> call = serviceEndPointInterface.updateServiceMultipart(serviceRequestId, statusId, arrayPics);
            call.enqueue(new Callback<UpdateResponse>() {
                @Override
                public void onResponse(Call<UpdateResponse> call,
                                       Response<UpdateResponse> response) {

                    if (response.body() != null) {

                        if (response.body() != null) {

                            updateResponse = response.body();

                            if (Utils.verifyOKTAG(updateResponse, getActivity())) {

                                if (resumeActivity == 1) {

                                    DashboardActivity activity = (DashboardActivity) getActivity();
                                    activity.updateData();

                                } else {

                                    WonServicesActivity activity = (WonServicesActivity) getActivity();
                                    activity.updateData();

                                }


                                StsChangeQuestionFragment.this.dismiss();


                            } else {

                                Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                                Toast.makeText(getActivity(), updateResponse.getStatus(), Toast.LENGTH_SHORT).show();

                            }


                        } else {

                            updateRetry++;

                            if (updateRetry < 3) {

                                call.clone().enqueue(this);

                            } else {

                                Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                            }

                        }

                    } else {

                        updateRetry++;

                        if (updateRetry < 3) {

                            call.clone().enqueue(this);

                        } else {

                            Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                        }

                    }

                }

                @Override
                public void onFailure(Call<UpdateResponse> call, Throwable t) {

                    updateRetry++;

                    if (updateRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                    }


                }
            });


        } else if (updateService.getStatus_id() == 5) {


            ServiceEndPointInterface serviceEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

            ArrayList<MultipartBody.Part> arrayPics = new ArrayList<MultipartBody.Part>();

            for (Picture fileUpload : pictures) {

                Uri uriPicture = Utils.getImageUri(getActivity().getApplicationContext(), fileUpload.getBitmapImage());
                String filePath = Utils.getRealPathFromURI(uriPicture, getActivity());

                File filePicture = new File(filePath);

                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(getActivity().getContentResolver().getType(uriPicture)),
                                filePicture
                        );

                MultipartBody.Part body =
                        MultipartBody.Part.createFormData(Utils.IMAGE_ARRAY_TAG, filePicture.getName(), requestFile);

                arrayPics.add(body);


            }

            String serviceRequestIdString = String.valueOf(updateService.getService_request_id());
            RequestBody serviceRequestId =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, serviceRequestIdString);

            String statusIdString = String.valueOf(updateService.getStatus_id());
            RequestBody statusId =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, statusIdString);

            RequestBody commentString =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, updateService.getComment_provider());

            RequestBody materialCost =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, String.valueOf(updateService.getMaterial_costs()));

            RequestBody technicalPrice =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, String.valueOf(updateService.getTechnical_price()));


            RequestBody assistancePrice =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, String.valueOf(updateService.getAssistant_price()));


            Call<UpdateResponse> call = serviceEndPointInterface.updateServiceMultipartFinish(serviceRequestId, statusId, commentString, materialCost, technicalPrice, assistancePrice, arrayPics);
            call.enqueue(new Callback<UpdateResponse>() {
                @Override
                public void onResponse(Call<UpdateResponse> call,
                                       Response<UpdateResponse> response) {

                    if (response.body() != null) {

                        if (response.body() != null) {

                            updateResponse = response.body();

                            if (Utils.verifyOKTAG(updateResponse, getActivity())) {

                                if (resumeActivity == 1) {

                                    DashboardActivity activity = (DashboardActivity) getActivity();
                                    activity.updateData();

                                } else {

                                    WonServicesActivity activity = (WonServicesActivity) getActivity();
                                    activity.updateData();

                                }


                                StsChangeQuestionFragment.this.dismiss();


                            } else {

                                Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                                Toast.makeText(getActivity(), updateResponse.getStatus(), Toast.LENGTH_SHORT).show();

                            }


                        } else {

                            updateRetry++;

                            if (updateRetry < 3) {

                                call.clone().enqueue(this);

                            } else {

                                Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                            }

                        }

                    } else {

                        updateRetry++;

                        if (updateRetry < 3) {

                            call.clone().enqueue(this);

                        } else {

                            Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                        }

                    }

                }

                @Override
                public void onFailure(Call<UpdateResponse> call, Throwable t) {

                    updateRetry++;

                    if (updateRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.generic_error_message), Toast.LENGTH_SHORT).show();

                    }


                }
            });


        }

    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }

    @Override
    public void onEditPhoto(View view, int position) {

        onPickImage(position);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case (R.id.qsPhoto):

                if (pictures.size() < 8) {

                    onPickImage(-1);

                } else {

                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.maximum_pictures_taken_error), Toast.LENGTH_SHORT).show();

                }

                break;

        }

    }
}