package com.gabrielguzman.android.famaex.login.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.login.interfaces.RegisterSpecializationsInterface;

/**
 * Created by macpro1 on 6/12/16.
 */
public class SpecializationViewHolder extends RecyclerView.ViewHolder {

    private TextView specializationViewHolderNameTextview;
    private ImageView specializationViewHolderCheckImageview;
    private View specializationViewHolderViewHolderDivider;
    private LinearLayout specializationItemViewHolderContainer;

    public SpecializationViewHolder(final View itemView, final RegisterSpecializationsInterface registerSpecializationsInterface) {
        super(itemView);

        specializationViewHolderNameTextview = (TextView) itemView.findViewById(R.id.specialization_item_view_holder_item_name_textview);
        specializationViewHolderCheckImageview = (ImageView) itemView.findViewById(R.id.specialization_item_view_holder_item_icon_imageview);
        specializationViewHolderViewHolderDivider = itemView.findViewById(R.id.specialization_item_view_holder_divider);
        specializationItemViewHolderContainer = (LinearLayout) itemView.findViewById(R.id.specialization_item_view_holder_container);

        specializationItemViewHolderContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                registerSpecializationsInterface.onSpecializationSelect(v, getAdapterPosition());

            }
        });


    }

    public TextView getSpecializationViewHolderNameTextview() {
        return specializationViewHolderNameTextview;
    }

    public void setSpecializationViewHolderNameTextview(TextView specializationViewHolderNameTextview) {
        this.specializationViewHolderNameTextview = specializationViewHolderNameTextview;
    }

    public ImageView getSpecializationViewHolderCheckImageview() {
        return specializationViewHolderCheckImageview;
    }

    public void setSpecializationViewHolderCheckImageview(ImageView specializationViewHolderCheckImageview) {
        this.specializationViewHolderCheckImageview = specializationViewHolderCheckImageview;
    }

    public View getSpecializationViewHolderViewHolderDivider() {
        return specializationViewHolderViewHolderDivider;
    }

    public void setSpecializationViewHolderViewHolderDivider(View specializationViewHolderViewHolderDivider) {
        this.specializationViewHolderViewHolderDivider = specializationViewHolderViewHolderDivider;
    }

    public LinearLayout getSpecializationItemViewHolderContainer() {
        return specializationItemViewHolderContainer;
    }

    public void setSpecializationItemViewHolderContainer(LinearLayout specializationItemViewHolderContainer) {
        this.specializationItemViewHolderContainer = specializationItemViewHolderContainer;
    }
}
