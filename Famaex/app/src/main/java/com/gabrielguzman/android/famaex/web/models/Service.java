package com.gabrielguzman.android.famaex.web.models;

import java.util.ArrayList;

/**
 * Created by jose on 05/04/17.
 */

public class Service {

    private String status;
    private String message;
    private ArrayList<Data> data;

    public Service() {
    }

    public Service(String status, String message, ArrayList<Data> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }
}
