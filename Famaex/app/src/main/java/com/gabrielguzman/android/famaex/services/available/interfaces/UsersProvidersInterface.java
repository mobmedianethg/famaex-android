package com.gabrielguzman.android.famaex.services.available.interfaces;

import android.view.View;

/**
 * Created by jose on 24/04/17.
 */

public interface UsersProvidersInterface {

    public void onUserSelected(View view, int position);

}
