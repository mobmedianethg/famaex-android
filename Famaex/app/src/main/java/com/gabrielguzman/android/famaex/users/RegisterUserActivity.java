package com.gabrielguzman.android.famaex.users;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.UserEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.SpecializationsResponse;
import com.gabrielguzman.android.famaex.web.models.User;
import com.gabrielguzman.android.famaex.web.models.UserRegister;
import com.gabrielguzman.android.famaex.web.models.UserRegisterResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegisterUserActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG = "RegisterUserActivity";
    private static int registerUserRetry;
    private static int userSpecializationsRetry;
    private EditText activityRegisterUserEmailEdittext;
    private EditText activityRegisterUserNameEdittext;
    private EditText activityRegisterUserPasswordEdittext;
    private EditText activityRegisterUserPhoneEdittext;
    private EditText activityRegisterUserStreetEdittext;
    private EditText activityRegisterUserNumberEdittext;
    private EditText activityRegisterUserNifEdittext;
    private EditText activityRegisterUserPostalCodeEdittext;
    private EditText activityRegisterUserStateEdittext;
    private EditText activityRegisterUserCountryEdittext;
    private TextView activityRegisterUserRegisterButton;
    private Retrofit retrofit;
    private RegisterUserActivity activity;
    private Toolbar activityRegisterUserToolbar;
    private View mProgressOverlay;
    private TextView progressMssg;
    private UserRegisterResponse userRegisterResponse;
    private SpecializationsResponse specializationsResponse;
    private SharedPreferences mUserPref;
    private ArrayList<String> specializationSelected;
    private static final int REGISTER_SERVICE = 1;
    private static final int USER_SPECIALIZATION_SERVICE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        retrofit = RestClient.getRetrofitInstance();
        activity = this;
        Utils.setPortaitOrientation(activity);
        mUserPref = getSharedPreferences(AppSharedPreferences.SHARED_NAME, MODE_PRIVATE);

        activityRegisterUserToolbar = (Toolbar) findViewById(R.id.activity_register_user_toolbar);
        setSupportActionBar(activityRegisterUserToolbar);

        ActionBar actionBar = getSupportActionBar();

        // Enable the Up button
        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(true);

        }

        activityRegisterUserEmailEdittext = (EditText) findViewById(R.id.activity_register_user_email_edittext);
        activityRegisterUserNameEdittext = (EditText) findViewById(R.id.activity_register_user_name_edittext);
        activityRegisterUserPasswordEdittext = (EditText) findViewById(R.id.activity_register_user_password_edittext);
        activityRegisterUserPhoneEdittext = (EditText) findViewById(R.id.activity_register_user_phone_edittext);
        activityRegisterUserStreetEdittext = (EditText) findViewById(R.id.activity_register_user_street_edittext);
        activityRegisterUserNumberEdittext = (EditText) findViewById(R.id.activity_register_user_number_edittext);
        activityRegisterUserNifEdittext = (EditText) findViewById(R.id.activity_register_user_nif_edittext);
        activityRegisterUserPostalCodeEdittext = (EditText) findViewById(R.id.activity_register_user_postal_code_edittext);
        activityRegisterUserStateEdittext = (EditText) findViewById(R.id.activity_register_user_state_edittext);
        activityRegisterUserCountryEdittext = (EditText) findViewById(R.id.activity_register_user_country_edittext);
        activityRegisterUserRegisterButton = (TextView) findViewById(R.id.activity_register_user_register_button);
        mProgressOverlay = findViewById(R.id.progress_overlay);
        progressMssg = (TextView) findViewById(R.id.progressMssg);


        activityRegisterUserRegisterButton.setOnClickListener(this);

        getUserSpecializations();


    }

    private void getUserSpecializations() {

        activityRegisterUserRegisterButton.setEnabled(false);

        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        userSpecializationsRetry = 0;
        UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);
        specializationSelected = new ArrayList<String>();

        final User userSpecializations = new User(mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), null, null, null, null, null, null, null, null, null);

        Call<SpecializationsResponse> call = userEndPointInterface.userSpecializations(RestClient.CONTENT_TYPE, userSpecializations);
        call.enqueue(new Callback<SpecializationsResponse>() {
            @Override
            public void onResponse(Call<SpecializationsResponse> call, Response<SpecializationsResponse> response) {


                if (response.body() != null) {

                    specializationsResponse = response.body();

                    if (Utils.verifyOKTAG(specializationsResponse, activity)) {

                        specializationSelected = specializationsResponse.getSpecialization();

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

                        activityRegisterUserRegisterButton.setEnabled(true);


                    } else {

                        displayError(specializationsResponse.getStatus(), USER_SPECIALIZATION_SERVICE);

                    }


                } else {

                    userSpecializationsRetry++;

                    if (userSpecializationsRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.global_network_error), USER_SPECIALIZATION_SERVICE);

                    }

                }

            }

            @Override
            public void onFailure(Call<SpecializationsResponse> call, Throwable t) {

                userSpecializationsRetry++;

                if (userSpecializationsRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.global_network_error), USER_SPECIALIZATION_SERVICE);

                }

            }
        });


    }


    public void displayError(String message, int serviceOption) {

        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();

        switch (serviceOption) {

            case (REGISTER_SERVICE):

                activityRegisterUserRegisterButton.setEnabled(true);

                break;


            case (USER_SPECIALIZATION_SERVICE):

                activityRegisterUserRegisterButton.setEnabled(true);

                break;


        }

    }

    private boolean validateRegisterForm() {

        boolean result = true;

        activityRegisterUserEmailEdittext.setError(null);
        activityRegisterUserNameEdittext.setError(null);
        activityRegisterUserPasswordEdittext.setError(null);
        activityRegisterUserPhoneEdittext.setError(null);
        activityRegisterUserStreetEdittext.setError(null);
        activityRegisterUserNumberEdittext.setError(null);
        activityRegisterUserNifEdittext.setError(null);
        activityRegisterUserPostalCodeEdittext.setError(null);
        activityRegisterUserStateEdittext.setError(null);
        activityRegisterUserCountryEdittext.setError(null);

        if (activityRegisterUserEmailEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterUserEmailEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.verifyEmail(activityRegisterUserEmailEdittext.getText().toString())) {

            result = false;
            activityRegisterUserEmailEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (activityRegisterUserNameEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterUserNameEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.noContainsNumbers(activityRegisterUserNameEdittext.getText().toString())) {

            result = false;
            activityRegisterUserNameEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (activityRegisterUserPasswordEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterUserPasswordEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (activityRegisterUserPasswordEdittext.getText().toString().length() < 6) {

            result = false;
            activityRegisterUserPasswordEdittext.setError(activity.getResources().getString(R.string.invalid_password_lenght));

        } else if (activityRegisterUserStreetEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterUserStreetEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (activityRegisterUserNumberEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterUserNumberEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.containsOnlyNumbers(activityRegisterUserNumberEdittext.getText().toString())) {

            result = false;
            activityRegisterUserNumberEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));


        } else if (activityRegisterUserNifEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterUserNifEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.containsOnlyNumbers(activityRegisterUserNifEdittext.getText().toString())) {

            result = false;
            activityRegisterUserNifEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (activityRegisterUserPostalCodeEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterUserPostalCodeEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.containsOnlyNumbers(activityRegisterUserPostalCodeEdittext.getText().toString())) {

            result = false;
            activityRegisterUserPostalCodeEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (activityRegisterUserStateEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterUserStateEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.noContainsNumbers(activityRegisterUserStateEdittext.getText().toString())) {

            result = false;
            activityRegisterUserStateEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (activityRegisterUserCountryEdittext.getText().toString().length() == 0) {

            result = false;
            activityRegisterUserCountryEdittext.setError(activity.getResources().getString(R.string.errorFieldRequired));

        } else if (!Utils.noContainsNumbers(activityRegisterUserCountryEdittext.getText().toString())) {

            result = false;
            activityRegisterUserCountryEdittext.setError(activity.getResources().getString(R.string.invalid_mail_format));

        } else if (specializationSelected == null) {

            result = false;
            Toast.makeText(getBaseContext(), activity.getResources().getString(R.string.activity_register_user_no_specializations), Toast.LENGTH_SHORT).show();

            getUserSpecializations();

        } else if (specializationSelected.size() == 0) {

            result = false;
            Toast.makeText(getBaseContext(), activity.getResources().getString(R.string.activity_register_user_no_specializations), Toast.LENGTH_SHORT).show();

            getUserSpecializations();

        }

        return result;


    }


    private void registerUser() {

        activityRegisterUserRegisterButton.setEnabled(false);

        registerUserRetry = 0;
        UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);

        progressMssg.setText(activity.getResources().getString(R.string.loading_register_user));

        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        ArrayList<Object> company = new ArrayList<Object>();
        ArrayList<String> address = new ArrayList<String>();

        company.add(mUserPref.getString(AppSharedPreferences.PROVIDER_COMPANY_CIF, ""));
        company.add(mUserPref.getString(AppSharedPreferences.PROVIDER_COMPANY, ""));
        company.add(new Integer(mUserPref.getInt(AppSharedPreferences.PROVIDER_COMPANY_NUMBER_EMPLOYEES, 0)));
        company.add(new Integer(mUserPref.getInt(AppSharedPreferences.PROVIDER_COMPANY_NUMBER_SELF_EMPLOYED, 0)));
        company.add(new Boolean(mUserPref.getBoolean(AppSharedPreferences.PROVIDER_COMPANY_LEGAL_TOPICS, false)));
        company.add(new Boolean(mUserPref.getBoolean(AppSharedPreferences.PROVIDER_COMPANY_QUALITY_FILTER, false)));

        address.add(activityRegisterUserStreetEdittext.getText().toString());
        address.add(activityRegisterUserNumberEdittext.getText().toString());
        address.add(activityRegisterUserPostalCodeEdittext.getText().toString());
        address.add(activityRegisterUserStateEdittext.getText().toString());
        address.add(activityRegisterUserCountryEdittext.getText().toString());

        UserRegister userRegister = new UserRegister(true, activityRegisterUserNameEdittext.getText().toString(), activityRegisterUserEmailEdittext.getText().toString(), activityRegisterUserPasswordEdittext.getText().toString(), activityRegisterUserPhoneEdittext.getText().toString(), company, address, activityRegisterUserNifEdittext.getText().toString(), specializationSelected);


        Call<UserRegisterResponse> call = userEndPointInterface.registerUser(RestClient.CONTENT_TYPE, userRegister);
        call.enqueue(new Callback<UserRegisterResponse>() {
            @Override
            public void onResponse(Call<UserRegisterResponse> call, Response<UserRegisterResponse> response) {


                if (response.body() != null) {

                    userRegisterResponse = response.body();

                    if (Utils.verifyOKTAG(userRegisterResponse, activity)) {

                        activityRegisterUserEmailEdittext.setText("");
                        activityRegisterUserNameEdittext.setText("");
                        activityRegisterUserPasswordEdittext.setText("");
                        activityRegisterUserPhoneEdittext.setText("");
                        activityRegisterUserStreetEdittext.setText("");
                        activityRegisterUserNumberEdittext.setText("");
                        activityRegisterUserNifEdittext.setText("");
                        activityRegisterUserPostalCodeEdittext.setText("");
                        activityRegisterUserStateEdittext.setText("");
                        activityRegisterUserCountryEdittext.setText("");

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

                        Toast.makeText(getBaseContext(), activity.getResources().getString(R.string.activity_register_success), Toast.LENGTH_LONG).show();


                    } else {

                        displayError(userRegisterResponse.getStatus(), REGISTER_SERVICE);

                    }


                } else {

                    registerUserRetry++;

                    if (registerUserRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.global_network_error), REGISTER_SERVICE);

                    }

                }

            }

            @Override
            public void onFailure(Call<UserRegisterResponse> call, Throwable t) {

                registerUserRetry++;

                if (registerUserRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.global_network_error), REGISTER_SERVICE);

                }

            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case (R.id.activity_register_user_register_button):

                if (validateRegisterForm()) {

                    Utils.hideSoftKeyboard(activity);

                    registerUser();

                }


                break;

        }


    }
}
