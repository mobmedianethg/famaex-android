package com.gabrielguzman.android.famaex.login.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.login.interfaces.RegisterSpecializationsInterface;
import com.gabrielguzman.android.famaex.login.viewholder.SpecializationViewHolder;
import com.gabrielguzman.android.famaex.models.Specialization;

import java.util.ArrayList;

/**
 * Created by macpro1 on 6/12/16.
 */
public class RecyclerViewSpecializationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String LOG_TAG = "RecyclerViewSpecializationAdapter";
    private ArrayList<Specialization> specializations;
    private Context context;
    private LayoutInflater layoutInflater;
    private RegisterSpecializationsInterface registerSpecializationsInterface;

    public RecyclerViewSpecializationAdapter(ArrayList<Specialization> specializations, Context context, RegisterSpecializationsInterface registerSpecializationsInterface) {
        this.specializations = specializations;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(this.context);
        this.registerSpecializationsInterface = registerSpecializationsInterface;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.specialization_item_view_holder, parent, false);
        SpecializationViewHolder holder = new SpecializationViewHolder(view, this.registerSpecializationsInterface);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final SpecializationViewHolder viewHolder = (SpecializationViewHolder) holder;
        Specialization specialization = specializations.get(position);

        if (specialization.getLast() == 1) {

            viewHolder.getSpecializationViewHolderViewHolderDivider().setVisibility(View.GONE);

        } else {

            viewHolder.getSpecializationViewHolderViewHolderDivider().setVisibility(View.VISIBLE);

        }

        if (specialization.getSelected() == true) {

            viewHolder.getSpecializationViewHolderCheckImageview().setVisibility(View.VISIBLE);

        } else {

            viewHolder.getSpecializationViewHolderCheckImageview().setVisibility(View.GONE);

        }

        viewHolder.getSpecializationViewHolderNameTextview().setText(specialization.getName());


    }

    public void updateElements(int position) {

        if (this.specializations.get(position).getSelected() == true) {

            this.specializations.get(position).setSelected(false);

        } else {

            this.specializations.get(position).setSelected(true);

        }

        this.notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return this.specializations.size();
    }

}
