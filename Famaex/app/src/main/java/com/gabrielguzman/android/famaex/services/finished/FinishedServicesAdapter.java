package com.gabrielguzman.android.famaex.services.finished;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Custom adapter to display the lists of Services.
 *
 * @author Gabriel Guzmán
 * @see BaseAdapter
 * @since 2016.21.07
 */
public class FinishedServicesAdapter extends BaseAdapter {

    private static ArrayList<Service> searchArrayList;
    private LayoutInflater mInflater;
    private String mListIndicator;
    private SharedPreferences mUserPref;
    private int[] mViewIDs;
    private int mLayout;
    private Context mContext;
    private int[] compCardViewIDs = {R.id.compServClient, R.id.compServName,
            R.id.compServDate, R.id.compServPrice,
            R.id.compServPaid};
    private int[] compActViewIDs = {R.id.compActServClient, R.id.compActServName,
            R.id.compActServDate, R.id.compActServPrice,
            R.id.compActServPaid, R.id.compActServRating,
            R.id.compActServStatus,
            R.id.actServiceUserProviderNameContainer, R.id.actServiceUserProviderName};

    public FinishedServicesAdapter(Context context, ArrayList<Service> results, String ind) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
        mListIndicator = ind;
        mContext = context;

        mViewIDs = (ind.equals("A")) ? compActViewIDs : compCardViewIDs;
        mLayout = (ind.equals("A")) ? R.layout.finished_services_activity_item : R.layout.finished_services_card_item;
    }

    @Override
    public int getCount() {
        return searchArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(mLayout, parent, false);
            holder = new ViewHolder();
            holder.txtClient = (TextView) convertView.findViewById(mViewIDs[0]);
            holder.txtServiceName = (TextView) convertView.findViewById(mViewIDs[1]);
            holder.txtDateArrival = (TextView) convertView.findViewById(mViewIDs[2]);
            holder.txtPrice = (TextView) convertView.findViewById(mViewIDs[3]);
            holder.imvPaid = (ImageView) convertView.findViewById(mViewIDs[4]);
            if (mListIndicator.equals("A"))
                holder.rtRating = (RatingBar) convertView.findViewById(mViewIDs[5]);
            holder.txtStatus = (TextView) convertView.findViewById(mViewIDs[6]);
            holder.providerNameContainer = (LinearLayout) convertView.findViewById(mViewIDs[7]);
            holder.providerNameTextview = (TextView) convertView.findViewById(mViewIDs[8]);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Service serv = searchArrayList.get(position);

        Calendar aDate = null;

        try {

            Log.v("dateeee ", "dateeee " + serv.getDateSelected());


            aDate = Famaex.dateToCalendar(Famaex.dateBackendBackwardFormat.parse(serv.getDateArrival()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mUserPref = mContext.getSharedPreferences(AppSharedPreferences.SHARED_NAME, mContext.MODE_PRIVATE);

        boolean state = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

        if (state) {

            holder.txtPrice.setVisibility(View.VISIBLE);
            holder.providerNameTextview.setVisibility(View.VISIBLE);

            holder.providerNameTextview.setText("Proveedor Responsable: " + serv.getProviderName());

        } else {

            holder.providerNameTextview.setVisibility(View.GONE);

            if (serv.isVisibility()) {

                holder.txtPrice.setVisibility(View.VISIBLE);

            } else {

                holder.txtPrice.setVisibility(View.INVISIBLE);

            }

        }

        holder.txtServiceName.setText(serv.getServiceName());

        String hourUnformatted = serv.getTimeArrival();
        String timeTextview = "--";

        if (hourUnformatted.length() > 6) {

            String[] timeSecond = serv.getTimeArrival().split(":");

            try {

                timeTextview = timeSecond[0] + ":" + timeSecond[1] + " " + timeSecond[2].substring(3);

            } catch (Exception e) {
                timeTextview = "--";

            }


        } else {

            timeTextview = "--";

        }


        holder.txtDateArrival.setText(String.format("%s/%s/%s %s",
                aDate.get(Calendar.DAY_OF_MONTH), aDate.get(Calendar.MONTH) + 1, aDate.get(Calendar.YEAR),
                timeTextview));

        holder.txtPrice.setText(String.format("%d €", serv.getTotalPrice()));
        holder.txtClient.setText(serv.getClientName());
        /*
        if (serv.getPaid())
            holder.imvPaid.setVisibility(View.VISIBLE);
        else
            holder.imvPaid.setVisibility(View.GONE);*/

        if (mListIndicator.equals("A"))
            holder.rtRating.setRating(serv.getRatingValue());

        holder.txtStatus.setText(serv.getStatus());

        if (serv.getStatus().equals("En proceso")) {
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        } else if (serv.getStatus().equals("Finalizado")) {
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
        } else {
            holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_red_light));
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditDialog(serv);
            }
        });
        return convertView;
    }

    private void showEditDialog(Service serv) {

        FragmentManager fm = ((Activity) mContext).getFragmentManager();
        FinishedServicesDetailFragment finishedDialogFragment =
                FinishedServicesDetailFragment.newInstance("Detalles de servicio", serv);
        finishedDialogFragment.show(fm, "fragment_comp_service_activity");
    }

    static class ViewHolder {
        TextView txtServiceName;
        TextView txtDateArrival;
        TextView txtPrice;
        TextView txtClient;
        TextView txtStatus;
        RatingBar rtRating;
        ImageView imvPaid;
        LinearLayout providerNameContainer;
        TextView providerNameTextview;

    }
}
