package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 24/04/17.
 */

public class DataUsersProviders {

    private Integer user_id;
    private String nif;
    private String name;
    private String email;
    private Boolean is_admin;
    private Integer admin_id;
    private String specializations;
    private Boolean select;
    private Integer last;

    public DataUsersProviders() {
    }

    public DataUsersProviders(Integer user_id, String nif, String name, String email, Boolean is_admin, Integer admin_id, String specializations, Boolean select, Integer last) {
        this.user_id = user_id;
        this.nif = nif;
        this.name = name;
        this.email = email;
        this.is_admin = is_admin;
        this.admin_id = admin_id;
        this.specializations = specializations;
        this.select = select;
        this.last = last;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(Boolean is_admin) {
        this.is_admin = is_admin;
    }

    public Integer getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(Integer admin_id) {
        this.admin_id = admin_id;
    }

    public String getSpecializations() {
        return specializations;
    }

    public void setSpecializations(String specializations) {
        this.specializations = specializations;
    }

    public Boolean getSelect() {
        return select;
    }

    public void setSelect(Boolean select) {
        this.select = select;
    }

    public Integer getLast() {
        return last;
    }

    public void setLast(Integer last) {
        this.last = last;
    }
}
