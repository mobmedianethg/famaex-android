package com.gabrielguzman.android.famaex.models;

/**
 * Created by jose on 11/04/17.
 */

public class Specialization {

    private String name;
    private Boolean selected;
    private int last;

    public Specialization() {
    }

    public Specialization(String name, Boolean selected, int last) {
        this.name = name;
        this.selected = selected;
        this.last = last;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }
}
