package com.gabrielguzman.android.famaex.notification;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.dashboard.DashboardActivity;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.ServiceEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.DataDetail;
import com.gabrielguzman.android.famaex.web.models.DetailResponse;
import com.gabrielguzman.android.famaex.web.models.ServiceDetail;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

/**
 * Shows a full screen dialog for the service detail.
 *
 * @author Gabriel Guzmán
 * @see DialogFragment
 * @since 2016.31.07
 */
public class NotificationDetailFragment extends DialogFragment implements Callback<DetailResponse> {

    private static int serviceDetailRetry;
    private TextView mServName;
    private TextView mSubservice;
    private TextView mPrice;
    private TextView mPriceTag;
    private TextView mPriceMeter;
    private TextView mPriceMeterTag;
    private TextView mNTE;
    private TextView mNTETag;
    private Service mService;
    private TextView mClient;
    private TextView mAddress;
    private TextView mPhone;
    private TextView mTitle;
    private TextView mMessage;
    private int mServReqID;
    private LinearLayout mProgressOverlay;
    private RelativeLayout mNoServ;
    private int mSts = 0;
    private Button mCancelButton;
    private Button mActionButton;
    private UpdateCallback callback;
    private DetailResponse detailResponse;
    private Retrofit retrofit;
    private SharedPreferences mUserPref;


    public NotificationDetailFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static NotificationDetailFragment newInstance(String title, Bundle message, int srID) {
        NotificationDetailFragment frag = new NotificationDetailFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putBundle("message", message);
        args.putInt("srid", srID);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        return inflater.inflate(R.layout.notification_detail_fragment, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        String title = getArguments().getBundle("message").getString("title");
        String body = getArguments().getBundle("message").getString("body");
        mServReqID = getArguments().getInt("srid");
        mServName = (TextView) view.findViewById(R.id.NotDetFrgServName);
        mSubservice = (TextView) view.findViewById(R.id.NotDetFrgSubservice);
        mPriceTag = (TextView) view.findViewById(R.id.NotDetFrgPriceTag);
        mPrice = (TextView) view.findViewById(R.id.NotDetFrgPrice);
        mPriceMeterTag = (TextView) view.findViewById(R.id.NotDetFrgPriceMeterTag);
        mPriceMeter = (TextView) view.findViewById(R.id.NotDetFrgPriceMeter);
        mNTETag = (TextView) view.findViewById(R.id.NotDetFrgNTETag);
        mNTE = (TextView) view.findViewById(R.id.NotDetFrgNTE);
        mClient = (TextView) view.findViewById(R.id.NotDetFrgClientName);
        mPhone = (TextView) view.findViewById(R.id.NotDetFrgPhone);
        mAddress = (TextView) view.findViewById(R.id.NotDetFrgAddress);
        mTitle = (TextView) view.findViewById(R.id.NotDetFrgTitle);
        mMessage = (TextView) view.findViewById(R.id.NotDetFrgMessage);
        mProgressOverlay = (LinearLayout) view.findViewById(R.id.NotDetProgressBar);
        mNoServ = (RelativeLayout) view.findViewById(R.id.emptyNotDetView);
        mActionButton = (Button) view.findViewById(R.id.notifActionButton);
        mCancelButton = (Button) view.findViewById(R.id.notifCloseButton);

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mUserPref = getActivity().getSharedPreferences(AppSharedPreferences.SHARED_NAME, MODE_PRIVATE);

        retrofit = RestClient.getRetrofitInstance();

        mTitle.setText(title);
        mMessage.setText(body);

        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        serviceDetailRetry = 0;

        ServiceEndPointInterface serviceEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

        ServiceDetail serviceDetail = new ServiceDetail(mServReqID);

        Call<DetailResponse> call = serviceEndPointInterface.serviceDetail(RestClient.CONTENT_TYPE, serviceDetail);
        call.enqueue(this);


    }

    @Override

    public void onResume() {

        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();

        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

        // Call super onResume after sizing
        super.onResume();

    }

    private void showEditDialog(Service serv) {

        dismiss();

        Intent homeIntent = new Intent(getActivity().getBaseContext(), DashboardActivity.class);
        getActivity().finish();
        startActivity(homeIntent);
    }

    public void setCallback(UpdateCallback callback) {

        this.callback = callback;
    }

    @Override
    public void onResponse(Call<DetailResponse> call, Response<DetailResponse> response) {

        detailResponse = response.body();

        if (response.body() != null) {

            if (Utils.verifyOKTAG(detailResponse, getActivity())) {

                for (DataDetail dataDetail : detailResponse.getData()) {

                    Service service = new Service();

                    if (dataDetail.getService_status().equals(Utils.COMPLETED_SERVICE_VALUE)) {

                        service.setClientName(dataDetail.getClient_name());
                        service.setClientAddress(dataDetail.getClient_address());
                        service.setServiceName(dataDetail.getService());
                        service.setSubService(dataDetail.getSubservice());

                        int serviceTotal = 0;

                        if (dataDetail.getService_per_mts2().equals(Utils.HOURS_TAG)) {

                            service.setTotalPrice(dataDetail.getTotal_price_mt2().intValue());
                            service.setTotalPrice(dataDetail.getTotal_price_mt2().intValue());


                        } else {

                            if (dataDetail.getPrice_provider() != null) {

                                serviceTotal += dataDetail.getPrice_provider().intValue();
                                service.setPrice(dataDetail.getPrice_provider().intValue());

                            }

                            if (dataDetail.getPrice_hour_technical_provider() != null) {

                                serviceTotal += dataDetail.getPrice_hour_technical_provider().intValue();

                            }

                            if (dataDetail.getPrice_hour_assistant_provider() != null) {

                                serviceTotal += dataDetail.getPrice_hour_assistant_provider().intValue();

                            }

                            service.setTotalPrice(serviceTotal);


                        }

                        if (dataDetail.getUrgent()) {

                            service.setPriority(Utils.URGENT_TAG);

                        } else {

                            service.setPriority("");
                        }


                        if (dataDetail.getVisibility_tariff() != null) {

                            if (dataDetail.getVisibility_tariff().equals(Utils.NO_TAG)) {

                                service.setVisibility(false);

                            } else {

                                service.setVisibility(true);

                            }

                        } else {

                            service.setVisibility(true);

                        }

                        service.setDateArrival(dataDetail.getDateArrivalDate());
                        service.setTimeArrival(dataDetail.getTimeArrivalDate());

                        String date = dataDetail.getPreference_selected().get(0);

                        String startDateHour = date.substring(0, 19);
                        String endDateHour = date.substring(22, date.length());

                        String[] dateStart = startDateHour.split(" ");
                        String[] dateEnd = endDateHour.split(" ");

                        service.setDateStart(dateStart[0]);
                        service.setDateFinish(dateEnd[0]);
                        service.setTimeStart(dateStart[1] + " " + dateStart[2]);
                        service.setTimeFinish(dateEnd[1] + " " + dateEnd[2]);

                        service.setDateArrival(dataDetail.getDateArrivalDate());
                        service.setTimeArrival(dataDetail.getTimeArrivalDate());


                        service.setCif(dataDetail.getCif());

                        if (dataDetail.getComment_client() != null) {

                            service.setComments(dataDetail.getComment_client());

                        } else {

                            service.setComments(getActivity().getResources().getString(R.string.noClientComments));
                        }

                        if (dataDetail.getComment_provider() != null) {

                            service.setProviderComment(dataDetail.getComment_provider());

                        } else {

                            service.setProviderComment(getActivity().getResources().getString(R.string.noProviderComments));
                        }

                        if (dataDetail.getRating_comment() != null) {

                            service.setClientRatingComment(dataDetail.getRating_comment());

                        } else {

                            service.setClientRatingComment(getActivity().getResources().getString(R.string.noClientComments));

                        }

                        mSts = 5;

                        mService = service;


                    } else {


                        service.setClientName(dataDetail.getClient_name());
                        service.setClientAddress(dataDetail.getClient_address());
                        service.setServiceName(dataDetail.getService());
                        service.setSubService(dataDetail.getSubservice());


                        int serviceTotal = 0;

                        if (dataDetail.getNte_provider() != null) {

                            if (dataDetail.getNte_provider().intValue() != 0) {

                                service.setNteProvider(dataDetail.getNte_provider().intValue());

                            } else {

                                service.setNteProvider(0);

                            }

                        } else {

                            service.setNteProvider(0);

                        }

                        if (dataDetail.getService_per_mts2().equals(Utils.HOURS_TAG)) {

                            service.setTotalPrice(dataDetail.getTotal_price_mt2().intValue());
                            service.setTotalPrice(dataDetail.getTotal_price_mt2().intValue());
                            service.setTypePrice(true);


                        } else {

                            if (dataDetail.getPrice_provider() != null) {

                                serviceTotal += dataDetail.getPrice_provider().intValue();
                                service.setPrice(dataDetail.getPrice_provider().intValue());

                            }

                            if (dataDetail.getPrice_hour_technical_provider() != null) {

                                serviceTotal += dataDetail.getPrice_hour_technical_provider().intValue();

                            }

                            if (dataDetail.getPrice_hour_assistant_provider() != null) {

                                serviceTotal += dataDetail.getPrice_hour_assistant_provider().intValue();

                            }

                            service.setTotalPrice(serviceTotal);
                            service.setTypePrice(false);


                        }

                        if (dataDetail.getService_request_id() != null) {

                            service.setServiceRqID(dataDetail.getService_request_id());

                        }

                        if (dataDetail.getUrgent()) {

                            service.setPriority(Utils.URGENT_TAG);

                        } else {

                            service.setPriority("");
                        }


                        if (dataDetail.getVisibility_tariff() != null) {

                            if (dataDetail.getVisibility_tariff().equals(Utils.NO_TAG)) {

                                service.setVisibility(false);

                            } else {

                                service.setVisibility(true);

                            }

                        } else {

                            service.setVisibility(true);

                        }


                        String date = dataDetail.getPreference_selected().get(0);

                        String startDateHour = date.substring(0, 19);
                        String endDateHour = date.substring(22, date.length());

                        String[] dateStart = startDateHour.split(" ");
                        String[] dateEnd = endDateHour.split(" ");

                        service.setDateStart(dateStart[0]);
                        service.setDateFinish(dateEnd[0]);
                        service.setTimeStart(dateStart[1] + " " + dateStart[2]);
                        service.setTimeFinish(dateEnd[1] + " " + dateEnd[2]);

                        service.setClientAddress(dataDetail.getClient_address());

                        if (dataDetail.getService_status().equals(Utils.ACCEPTED_SERVICE_VALUE)) {

                            mSts = 2;

                        } else if (dataDetail.getService_status().equals(Utils.ON_WAY_SERVICE_VALUE)) {

                            mSts = 3;

                        } else if (dataDetail.getService_status().equals(Utils.ON_PROCESS_SERVICE_VALUE)) {

                            mSts = 4;
                        }


                        mService = service;

                    }


                }


                mUserPref = getActivity().getSharedPreferences(AppSharedPreferences.SHARED_NAME, getActivity().MODE_PRIVATE);

                boolean state = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

                if (state) {
                    if (mService.getTypePrice()) {
                        mPriceMeterTag.setVisibility(View.VISIBLE);
                        mPriceMeter.setVisibility(View.VISIBLE);
                        mPriceMeter.setText(String.format("%d €", mService.getPrice()));
                    } else {
                        mPriceTag.setVisibility(View.VISIBLE);
                        mPrice.setVisibility(View.VISIBLE);
                        mPrice.setText(String.format("%d €", mService.getPrice()));
                    }

                } else {

                    if (mService.isVisibility()) {

                        if (mService.getTypePrice()) {
                            mPriceMeterTag.setVisibility(View.VISIBLE);
                            mPriceMeter.setVisibility(View.VISIBLE);
                            mPriceMeter.setText(String.format("%d €", mService.getPrice()));
                        } else {
                            mPriceTag.setVisibility(View.VISIBLE);
                            mPrice.setVisibility(View.VISIBLE);
                            mPrice.setText(String.format("%d €", mService.getPrice()));
                        }

                    }

                }

                Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
                mServName.setText(mService.getServiceName());
                mSubservice.setText(mService.getSubService());
                mClient.setText(mService.getClientName());
                mAddress.setText(((mSts < 2) ? "Para ver esto deben asignarte el servicio" :
                        mService.getClientAddress()));
                mPhone.setText(((mSts <= 2) || (mSts >= 5) ? "Podrás verlo cuando estés en camino" :
                        mService.getClientPhone()));

                if (mSts < 2) {
                    mActionButton.setVisibility(View.VISIBLE);
                    mActionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showEditDialog(mService);
                        }
                    });
                }


            } else {

                displayError(detailResponse.getStatus());

            }


        } else {

            serviceDetailRetry++;

            if (serviceDetailRetry < 3) {

                call.clone().enqueue(this);

            } else {

                displayError(getActivity().getResources().getString(R.string.apply_service_error_message));

            }

        }

    }


    @Override
    public void onFailure(Call<DetailResponse> call, Throwable t) {

        serviceDetailRetry++;

        if (serviceDetailRetry < 3) {

            call.clone().enqueue(this);

        } else {

            displayError(getActivity().getResources().getString(R.string.apply_service_error_message));

        }

    }

    public void displayError(String message) {

        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

        mNoServ.setVisibility(View.VISIBLE);


    }

    public interface UpdateCallback {
        public void notifDialogDismissed();
    }


}
