package com.gabrielguzman.android.famaex.web.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jose on 20/04/17.
 */

public class UserTariff {

    public static final String VISIBILITY_TARIFF = "visibility tariff";

    private String nif;
    private String name;
    private String email;
    @SerializedName(VISIBILITY_TARIFF)
    private Boolean visibilityTariff;

    public UserTariff(String nif, String name, String email, Boolean visibilityTariff) {
        this.nif = nif;
        this.name = name;
        this.email = email;
        this.visibilityTariff = visibilityTariff;
    }

    public UserTariff() {
    }

    public void setVisibilityTariff(Boolean visibilityTariff) {
        this.visibilityTariff = visibilityTariff;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getVisibilityTariff() {
        return visibilityTariff;
    }
}
