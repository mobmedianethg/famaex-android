package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 08/04/17.
 */

public class UserRegisterResponse {

    private String status;
    private Integer id;

    public UserRegisterResponse() {
    }

    public UserRegisterResponse(String status, Integer id) {
        this.status = status;
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
