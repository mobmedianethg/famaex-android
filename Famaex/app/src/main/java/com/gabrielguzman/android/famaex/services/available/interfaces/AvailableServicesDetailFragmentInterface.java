package com.gabrielguzman.android.famaex.services.available.interfaces;

import android.view.View;

/**
 * Created by jose on 25/04/17.
 */

public interface AvailableServicesDetailFragmentInterface {

    public void onDateSlected(View view, int position);

}
