package com.gabrielguzman.android.famaex.web.models;

import java.util.List;

/**
 * Created by jose on 07/04/17.
 */

public class DataApproved {

    private String client_name;
    private String client_phone;
    private String client_address;
    private String service;
    private String subservice;
    private Integer service_request_id;
    private Boolean urgent;
    private Integer status_id;
    private String status;
    private String service_per_mts2;
    private List<String> preference_selected;
    private String comment_client;
    private Integer displacement_fee;
    private Integer nte_provider;
    private Integer nte_client;
    private Double technical_hour_price_provider;
    private Double assistant_hour_price_provider;
    private Double technical_hour_price_client;
    private Double assistant_hour_price_client;
    private Double margin_from_cost;
    private Double price_mts2;
    private Double total_price_mt2;
    private String visibility_tariff;

    public DataApproved() {
    }

    public DataApproved(String client_name, String client_phone, String client_address, String service, String subservice, Integer service_request_id, Boolean urgent, Integer status_id, String status, String service_per_mts2, List<String> preference_selected, String comment_client, Integer displacement_fee, Integer nte_provider, Integer nte_client, Double technical_hour_price_provider, Double assistant_hour_price_provider, Double technical_hour_price_client, Double assistant_hour_price_client, Double margin_from_cost, Double price_mts2, Double total_price_mt2, String visibility_tariff) {
        this.client_name = client_name;
        this.client_phone = client_phone;
        this.client_address = client_address;
        this.service = service;
        this.subservice = subservice;
        this.service_request_id = service_request_id;
        this.urgent = urgent;
        this.status_id = status_id;
        this.status = status;
        this.service_per_mts2 = service_per_mts2;
        this.preference_selected = preference_selected;
        this.comment_client = comment_client;
        this.displacement_fee = displacement_fee;
        this.nte_provider = nte_provider;
        this.nte_client = nte_client;
        this.technical_hour_price_provider = technical_hour_price_provider;
        this.assistant_hour_price_provider = assistant_hour_price_provider;
        this.technical_hour_price_client = technical_hour_price_client;
        this.assistant_hour_price_client = assistant_hour_price_client;
        this.margin_from_cost = margin_from_cost;
        this.price_mts2 = price_mts2;
        this.total_price_mt2 = total_price_mt2;
        this.visibility_tariff = visibility_tariff;
    }


    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getClient_phone() {
        return client_phone;
    }

    public void setClient_phone(String client_phone) {
        this.client_phone = client_phone;
    }

    public String getClient_address() {
        return client_address;
    }

    public void setClient_address(String client_address) {
        this.client_address = client_address;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getSubservice() {
        return subservice;
    }

    public void setSubservice(String subservice) {
        this.subservice = subservice;
    }

    public Integer getService_request_id() {
        return service_request_id;
    }

    public void setService_request_id(Integer service_request_id) {
        this.service_request_id = service_request_id;
    }

    public Boolean getUrgent() {
        return urgent;
    }

    public void setUrgent(Boolean urgent) {
        this.urgent = urgent;
    }

    public Integer getStatus_id() {
        return status_id;
    }

    public void setStatus_id(Integer status_id) {
        this.status_id = status_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getService_per_mts2() {
        return service_per_mts2;
    }

    public void setService_per_mts2(String service_per_mts2) {
        this.service_per_mts2 = service_per_mts2;
    }

    public List<String> getPreference_selected() {
        return preference_selected;
    }

    public void setPreference_selected(List<String> preference_selected) {
        this.preference_selected = preference_selected;
    }

    public String getComment_client() {
        return comment_client;
    }

    public void setComment_client(String comment_client) {
        this.comment_client = comment_client;
    }

    public Integer getDisplacement_fee() {
        return displacement_fee;
    }

    public void setDisplacement_fee(Integer displacement_fee) {
        this.displacement_fee = displacement_fee;
    }

    public Integer getNte_provider() {
        return nte_provider;
    }

    public void setNte_provider(Integer nte_provider) {
        this.nte_provider = nte_provider;
    }

    public Integer getNte_client() {
        return nte_client;
    }

    public void setNte_client(Integer nte_client) {
        this.nte_client = nte_client;
    }

    public Double getTechnical_hour_price_provider() {
        return technical_hour_price_provider;
    }

    public void setTechnical_hour_price_provider(Double technical_hour_price_provider) {
        this.technical_hour_price_provider = technical_hour_price_provider;
    }

    public Double getAssistant_hour_price_provider() {
        return assistant_hour_price_provider;
    }

    public void setAssistant_hour_price_provider(Double assistant_hour_price_provider) {
        this.assistant_hour_price_provider = assistant_hour_price_provider;
    }

    public Double getTechnical_hour_price_client() {
        return technical_hour_price_client;
    }

    public void setTechnical_hour_price_client(Double technical_hour_price_client) {
        this.technical_hour_price_client = technical_hour_price_client;
    }

    public Double getAssistant_hour_price_client() {
        return assistant_hour_price_client;
    }

    public void setAssistant_hour_price_client(Double assistant_hour_price_client) {
        this.assistant_hour_price_client = assistant_hour_price_client;
    }

    public Double getMargin_from_cost() {
        return margin_from_cost;
    }

    public void setMargin_from_cost(Double margin_from_cost) {
        this.margin_from_cost = margin_from_cost;
    }

    public Double getPrice_mts2() {
        return price_mts2;
    }

    public void setPrice_mts2(Double price_mts2) {
        this.price_mts2 = price_mts2;
    }

    public Double getTotal_price_mt2() {
        return total_price_mt2;
    }

    public void setTotal_price_mt2(Double total_price_mt2) {
        this.total_price_mt2 = total_price_mt2;
    }

    public String getVisibility_tariff() {
        return visibility_tariff;
    }

    public void setVisibility_tariff(String visibility_tariff) {
        this.visibility_tariff = visibility_tariff;
    }
}
