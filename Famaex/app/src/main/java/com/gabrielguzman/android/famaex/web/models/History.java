package com.gabrielguzman.android.famaex.web.models;

import java.util.ArrayList;

/**
 * Created by jose on 06/04/17.
 */

public class History {

    private String status;
    private String message;
    private ArrayList<DataHistory> data;

    public History() {
    }

    public History(String status, String message, ArrayList<DataHistory> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DataHistory> getData() {
        return data;
    }

    public void setData(ArrayList<DataHistory> data) {
        this.data = data;
    }

}
