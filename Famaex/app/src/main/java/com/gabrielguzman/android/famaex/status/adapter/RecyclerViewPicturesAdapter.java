package com.gabrielguzman.android.famaex.status.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.models.Picture;
import com.gabrielguzman.android.famaex.status.interfaces.EditPhotoStatusChangeInterface;
import com.gabrielguzman.android.famaex.status.viewholder.StsChangeQuestionViewHolderItemFragment;

import java.util.ArrayList;

/**
 * Created by macpro1 on 6/12/16.
 */
public class RecyclerViewPicturesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String LOG_TAG = "RecyclerViewPicturesAdapter";
    private ArrayList<Picture> pictures;
    private Context context;
    private LayoutInflater layoutInflater;
    private EditPhotoStatusChangeInterface editPhotoStatusChangeInterface;

    public RecyclerViewPicturesAdapter(ArrayList<Picture> pictures, Context context, EditPhotoStatusChangeInterface editPhotoStatusChangeInterface) {
        this.pictures = pictures;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(this.context);
        this.editPhotoStatusChangeInterface = editPhotoStatusChangeInterface;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.sts_change_question_viewholder_item_fragment, parent, false);
        StsChangeQuestionViewHolderItemFragment holder = new StsChangeQuestionViewHolderItemFragment(view, this.editPhotoStatusChangeInterface);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final StsChangeQuestionViewHolderItemFragment viewHolder = (StsChangeQuestionViewHolderItemFragment) holder;
        Picture picture = pictures.get(position);

        viewHolder.getStsChangeQuestionviewholderItemFragmentImage().setImageBitmap(picture.getBitmapImage());


    }

    public void updateElements(int position, Bitmap updatePicture) {

        this.pictures.get(position).setBitmapImage(updatePicture);

        this.notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return this.pictures.size();
    }

}
