package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 31/03/17.
 */

public class Token {

    private String register_id;
    private int id;
    private String device;
    private String register_old;
    private String register_new;

    public Token(String register_id, int id, String device, String register_old, String register_new) {
        this.register_id = register_id;
        this.id = id;
        this.device = device;
        this.register_old = register_old;
        this.register_new = register_new;
    }

    public Token() {
    }

    public String getRegister_id() {
        return register_id;
    }

    public void setRegister_id(String register_id) {
        this.register_id = register_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getRegister_old() {
        return register_old;
    }

    public void setRegister_old(String register_old) {
        this.register_old = register_old;
    }

    public String getRegister_new() {
        return register_new;
    }

    public void setRegister_new(String register_new) {
        this.register_new = register_new;
    }
}
