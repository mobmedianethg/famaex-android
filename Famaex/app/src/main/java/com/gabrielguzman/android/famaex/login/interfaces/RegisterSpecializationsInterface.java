package com.gabrielguzman.android.famaex.login.interfaces;

import android.view.View;

/**
 * Created by jose on 25/06/16.
 */
public interface RegisterSpecializationsInterface {

    public void onSpecializationSelect(View view, int position);

}
