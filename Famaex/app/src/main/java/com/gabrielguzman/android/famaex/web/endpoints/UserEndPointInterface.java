package com.gabrielguzman.android.famaex.web.endpoints;

import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.web.models.SpecializationsResponse;
import com.gabrielguzman.android.famaex.web.models.TariffRequest;
import com.gabrielguzman.android.famaex.web.models.TariffResponse;
import com.gabrielguzman.android.famaex.web.models.User;
import com.gabrielguzman.android.famaex.web.models.UserRegister;
import com.gabrielguzman.android.famaex.web.models.UserRegisterResponse;
import com.gabrielguzman.android.famaex.web.models.UserSpecializationsRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by macpro1 on 3/20/17.
 */

public interface UserEndPointInterface {

    public static final String LOG_TAG = "UserEndPointInterface";

    public static final String LOGIN_USER_URL = "providers/LoginProvider";
    public static final String RECOVER_PASSWORD_URL = "providers/RecoveryPassword";
    public static final String CHANGE_PASSWORD_URL = "providers/ResetPassword";
    public static final String REGISTER_USER_URL = "providers/RegisterProvider";
    public static final String AVAILABLE_SPECIALIZATIONS = "provider_service_requests/SpecializationsAvailable";
    public static final String PROVIDERS_SPECIALIZATIONS_URL = "provider_service_requests/ProviderSpecializations";
    public static final String UPDATE_USER_SPECIALIZATIONS_URL = "provider_service_requests/UpdateSpecializations";
    public static final String GET_TARIFF_VISIBILITY = "providers/CurrentVisibilityTariff";
    public static final String UPDATE_TARIFF_VISIBILITY = "providers/VisibilityTariff";


    @POST(LOGIN_USER_URL)
    Call<User> loginUser(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body User user);

    @PUT(RECOVER_PASSWORD_URL)
    Call<User> recoverPassword(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body User user);

    @PUT(CHANGE_PASSWORD_URL)
    Call<User> changePassword(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body User user);

    @POST(REGISTER_USER_URL)
    Call<UserRegisterResponse> registerUser(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body UserRegister user);

    @GET(AVAILABLE_SPECIALIZATIONS)
    Call<SpecializationsResponse> categories(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType);

    @POST(PROVIDERS_SPECIALIZATIONS_URL)
    Call<SpecializationsResponse> userSpecializations(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body User user);

    @PUT(UPDATE_USER_SPECIALIZATIONS_URL)
    Call<SpecializationsResponse> updateUserSpecializations(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body UserSpecializationsRequest userSpecializationsRequest);

    @POST(GET_TARIFF_VISIBILITY)
    Call<TariffResponse> getTariffVisibility(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body TariffRequest tariffRequest);

    @POST(UPDATE_TARIFF_VISIBILITY)
    Call<TariffResponse> updateTariffVisibility(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body TariffRequest tariffRequest);


}
