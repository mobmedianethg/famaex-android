package com.gabrielguzman.android.famaex.services.won;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.dashboard.DashboardActivity;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Shows a full screen dialog for the available service detail.
 *
 * @author Gabriel Guzmán
 * @see DialogFragment
 * @since 2016.12.07
 */
public class WonServicesDetailFragment extends DialogFragment {

    public static final String LOG_TAG = "WonServicesDetailFragment";
    private SharedPreferences mUserPref;
    private TextView mServName;
    private TextView mSubservice;
    private TextView mPrice;
    private TextView mPriceTag;
    private TextView mPriceMeter;
    private TextView mPriceMeterTag;
    private TextView mPriceJourney;
    private TextView mPriceJourneyTag;
    private TextView mNTE;
    private TextView mNTETag;
    private TextView mDateSelected;
    private TextView mTimeSelected;
    private Service mService;
    private TextView mClient;
    private TextView mAddress;
    private TextView mClientComment;
    private TextView mPhone;
    private TextView mStsTitle;
    private Button mSts;
    private Button mStsChange;
    private LinearLayout mPhoneSection;
    private int mServReqID;
    private int acitivty;
    private DialogInterface.OnDismissListener onDismissListener;

    public WonServicesDetailFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static WonServicesDetailFragment newInstance(String title, Service serv, int activity) {
        WonServicesDetailFragment frag = new WonServicesDetailFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putSerializable("service", serv);
        args.putInt("activity", activity);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        return inflater.inflate(R.layout.won_services_detail_fragment, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        mServName = (TextView) view.findViewById(R.id.wnDetFrgServName);
        mSubservice = (TextView) view.findViewById(R.id.wnDetFrgSubservice);
        mPrice = (TextView) view.findViewById(R.id.wnDetFrgPrice);
        mPriceTag = (TextView) view.findViewById(R.id.wnDetFrgPriceTag);
        mPriceMeter = (TextView) view.findViewById(R.id.wnDetFrgPriceMeter);
        mPriceMeterTag = (TextView) view.findViewById(R.id.wnDetFrgPriceMeterTag);
        mPriceJourney = (TextView) view.findViewById(R.id.wnDetFrgPriceJourney);
        mPriceJourneyTag = (TextView) view.findViewById(R.id.wnDetFrgPriceJourneyTag);
        mNTE = (TextView) view.findViewById(R.id.wnDetFrgNTE);
        mNTETag = (TextView) view.findViewById(R.id.wnDetFrgNTETag);
        mClient = (TextView) view.findViewById(R.id.wnDetFrgClientName);
        mPhone = (TextView) view.findViewById(R.id.wnDetFrgPhone);
        mAddress = (TextView) view.findViewById(R.id.wnDetFrgAddress);
        mClientComment = (TextView) view.findViewById(R.id.wnDetFrgClientComment);
        mDateSelected = (TextView) view.findViewById(R.id.wncompDetFrgSelDate);
        mTimeSelected = (TextView) view.findViewById(R.id.wnDetFrgSelTime);
        mStsTitle = (TextView) view.findViewById(R.id.wnDetFrgChgStsTitle);
        mSts = (Button) view.findViewById(R.id.wnDetFrgSts);
        mStsChange = (Button) view.findViewById(R.id.wnDetFrgChangeStsButton);
        mPhoneSection = (LinearLayout) view.findViewById(R.id.wnPhoneSection);

        // Fetch arguments from bundle and set title
        // String title = getArguments().getString("title", "Alerta");
        // getDialog().setTitle(title);

        mUserPref = getActivity().getSharedPreferences(AppSharedPreferences.SHARED_NAME, Context.MODE_PRIVATE);
        mService = (Service) getArguments().getSerializable("service");
        acitivty = (int) getArguments().getSerializable("activity");

        mUserPref = getActivity().getSharedPreferences(AppSharedPreferences.SHARED_NAME, getActivity().MODE_PRIVATE);

        boolean state = mUserPref.getBoolean(AppSharedPreferences.PROVIDER_ADMIN, false);

        if (state) {

            mSubservice.setVisibility(View.VISIBLE);

        } else {

            if (mService.isVisibility()) {

                mSubservice.setVisibility(View.VISIBLE);

            } else {

                mSubservice.setVisibility(View.INVISIBLE);

            }

        }

        if (mService.getTypePrice()) {
            mPriceMeterTag.setVisibility(View.VISIBLE);
            mPriceMeter.setVisibility(View.VISIBLE);
            mPriceMeter.setText(String.format("%d €", mService.getPrice()));
        } else {
            mPriceTag.setVisibility(View.VISIBLE);
            mPrice.setVisibility(View.VISIBLE);
            mPrice.setText(String.format("%d €", mService.getPrice()));
        }

        if (mService.getNteProvider() != 0) {

            mNTE.setText(String.format("%d €", mService.getNteProvider()));
            mNTETag.setVisibility(View.VISIBLE);
            mNTE.setVisibility(View.VISIBLE);

        }

        mServReqID = mService.getServiceRqID();
        mServName.setText(mService.getServiceName());
        mSubservice.setText(mService.getSubService());
        mClient.setText(mService.getClientName());

        int status = mService.getStatusID();

        Calendar current = Calendar.getInstance(), combinedDate = null;

       /* try {
            combinedDate = Famaex.dateToCalendar(Famaex.combinedAppFormat.parse(String.format("%s%s",mService.getDateSelected(),mService.getTimeSelected())));
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        //if (current.after(combinedDate)){
        if ((status == 3) || (status == 4)) {
            mPhoneSection.setVisibility(View.VISIBLE);
            SpannableString spanStr = new SpannableString(mService.getClientPhone());
            spanStr.setSpan(new UnderlineSpan(), 0, spanStr.length(), 0);
            mPhone.setText(spanStr);
            mPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
                    intent.setData(Uri.parse("tel:" + mService.getClientPhone()));
                    startActivity(intent);
                }
            });

        } else {
            mPhoneSection.setVisibility(View.GONE);
        }

        mAddress.setText(mService.getClientAddress());
        mClientComment.setText(mService.getComments());

        try {
            mDateSelected.setText(Famaex.dateAppFormat.format(Famaex.dateBackendFormat.parse(mService.getDateSelected())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            mTimeSelected.setText(mService.getTimeSelected().substring(0, 8));
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }


        setStatusOptions(status);

        mStsChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditDialog(mService);
            }
        });

    }

    @Override

    public void onResume() {

        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();

        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

        // Call super onResume after sizing
        super.onResume();

    }

    private void showEditDialog(Service serv) {


        if (acitivty == 1) {

            DashboardActivity parent = (DashboardActivity) getActivity();
            parent.checkPermission(serv);

        } else {

            WonServicesActivity parent = (WonServicesActivity) getActivity();
            parent.checkPermission(serv);

        }

        WonServicesDetailFragment.this.dismiss();

        /*
        FragmentManager fm = getActivity().getFragmentManager();
        StsChangeQuestionFragment questionDialogFragment =
                StsChangeQuestionFragment.newInstance("Servicio Ganado", serv);

        questionDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // mWnServTask = new WonServDownloadTask();
                // mWnServTask.execute((Void) null);
                WonServicesDetailFragment.this.dismiss();
            }
        });
        questionDialogFragment.show(fm, "fragment_servicio_ganado");
        //WonServicesDetailFragment.this.dismiss();

        */

    }

    /**
     * Sets values for the info presented to the user
     *
     * @param sts The service status ID.
     */
    public void setStatusOptions(int sts) {

        switch (sts) {
            //ACCEPTED
            case 2:
                mSts.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_light));
                mStsTitle.setText(R.string.onWayTitle);
                mSts.setText(mService.getStatus());
                break;
            //ON THE WAY
            case 3:
                mSts.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_light));
                mStsTitle.setText(R.string.arrivedTitle);
                mSts.setText(mService.getStatus());
                break;
            //IN PROCESS
            case 4:
                break;
            //FINISHED
            case 5:
                mSts.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.holo_green_light));
                mStsTitle.setText(R.string.completedTitle);
                mSts.setText(mService.getStatus());
                mStsChange.setVisibility(View.GONE);
                break;
            //CANCELED
            case 6:
                mSts.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_light));
                mStsTitle.setText(R.string.canceledTitle);
                mSts.setText(mService.getStatus());
                mStsChange.setVisibility(View.GONE);
                break;
            default:
                mSts.setVisibility(View.GONE);
                mStsTitle.setText(R.string.serviceProblemTitle);
                mStsChange.setVisibility(View.GONE);
                break;
        }
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }
}
