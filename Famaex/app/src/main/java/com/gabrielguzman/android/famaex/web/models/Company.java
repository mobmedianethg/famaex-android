package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 30/03/17.
 */

public class Company {

    private Integer id;
    private String cif;
    private String company;
    private Integer number_employees;
    private Integer number_self_employed;
    private Boolean revised_legal_topics;
    private Boolean quality_filter;

    public Company(Integer id, String cif, String company, Integer number_employees, Integer number_self_employed, Boolean revised_legal_topics, Boolean quality_filter) {
        this.id = id;
        this.cif = cif;
        this.company = company;
        this.number_employees = number_employees;
        this.number_self_employed = number_self_employed;
        this.revised_legal_topics = revised_legal_topics;
        this.quality_filter = quality_filter;
    }

    public Company() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Integer getNumber_employees() {
        return number_employees;
    }

    public void setNumber_employees(Integer number_employees) {
        this.number_employees = number_employees;
    }

    public Integer getNumber_self_employed() {
        return number_self_employed;
    }

    public void setNumber_self_employed(Integer number_self_employed) {
        this.number_self_employed = number_self_employed;
    }

    public Boolean getRevised_legal_topics() {
        return revised_legal_topics;
    }

    public void setRevised_legal_topics(Boolean revised_legal_topics) {
        this.revised_legal_topics = revised_legal_topics;
    }

    public Boolean getQuality_filter() {
        return quality_filter;
    }

    public void setQuality_filter(Boolean quality_filter) {
        this.quality_filter = quality_filter;
    }
}
