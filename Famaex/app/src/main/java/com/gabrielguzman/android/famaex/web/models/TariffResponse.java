package com.gabrielguzman.android.famaex.web.models;

import java.util.ArrayList;

/**
 * Created by jose on 20/04/17.
 */

public class TariffResponse {

    private String status;
    private String message;
    private ArrayList<UserTariff> data;

    public TariffResponse(String status, String message, ArrayList<UserTariff> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public TariffResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<UserTariff> getData() {
        return data;
    }

    public void setData(ArrayList<UserTariff> data) {
        this.data = data;
    }
}
