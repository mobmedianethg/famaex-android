package com.gabrielguzman.android.famaex.services.available;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.Toast;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.models.DateService;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.ServiceEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.Data;
import com.gabrielguzman.android.famaex.web.models.User;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Activity that shows the complete list of available assigned services.
 *
 * @author Gabriel Guzman
 * @see AppCompatActivity
 * @since 2016.28.06
 */
public class AvailableServicesActivity extends AppCompatActivity implements AvailableServicesAdapter.UpdateCallback {

    private static int availableServicesRetry;
    public Retrofit retrofit;
    private SharedPreferences mUserPref;
    private ListView availableServices;
    private ArrayList<Service> avServList;
    private AvailableServicesActivity activity;
    private AvailableServicesAdapter asca;
    private static int getUsersProvidersRetry;
    private static final int USERS_PROVIDERS = 2;
    private static final int SERVICE_AVAILABLE = 1;
    private static final String startTimeDefault = "07:00 AM";
    private static final String endTimeDefault = "09:00 PM";
    private static final String LOG_TAG = "AvailableServicesActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.available_services_activity);

        mUserPref = getSharedPreferences(AppSharedPreferences.SHARED_NAME, MODE_PRIVATE);

        //Set the toolbar
        Toolbar famaexToolbar = (Toolbar) findViewById(R.id.famaexAvServToolbar);
        setSupportActionBar(famaexToolbar);
        ActionBar sab = getSupportActionBar();

        if (sab != null) {
            sab.setDisplayHomeAsUpEnabled(true);
        }


        activity = (AvailableServicesActivity) this;
        retrofit = RestClient.getRetrofitInstance();

        Utils.setPortaitOrientation(activity);

        //-------------------
        avServList = (ArrayList<Service>) getIntent().getSerializableExtra(Utils.FAMAEX_AVAILABLE_SERIAZABLE);

        availableServices = (ListView) findViewById(R.id.avActList);
        availableServices.setAdapter(new AvailableServicesAdapter(AvailableServicesActivity.this, avServList, "A", activity));
        availableServices.setEmptyView(findViewById(R.id.emptyAvServActView));

    }

    @Override
    protected void onResume() {
        super.onResume();

        updateData();
    }

    public void updateData() {

        activity = (AvailableServicesActivity) this;
        retrofit = RestClient.getRetrofitInstance();
        setAvailableServices();

    }

    private void setAvailableServices() {

        avServList = new ArrayList<>();

        availableServicesRetry = 0;

        ServiceEndPointInterface serviceAvailableEndPointInterface = retrofit.create(ServiceEndPointInterface.class);

        final User userAvailable = new User(mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), null, null, null, null, null, null, null, null, null);

        Call<com.gabrielguzman.android.famaex.web.models.Service> callAvailable = serviceAvailableEndPointInterface.availableServices(RestClient.CONTENT_TYPE, userAvailable);
        callAvailable.enqueue(new Callback<com.gabrielguzman.android.famaex.web.models.Service>() {
            @Override
            public void onResponse(Call<com.gabrielguzman.android.famaex.web.models.Service> call, Response<com.gabrielguzman.android.famaex.web.models.Service> response) {

                com.gabrielguzman.android.famaex.web.models.Service servicesAvailable;

                if (response.body() != null) {

                    servicesAvailable = response.body();

                    if (Utils.verifyOKTAG(servicesAvailable, activity.getApplicationContext())) {

                        for (Data data : servicesAvailable.getData()) {

                            Service service = new Service();
                            service.setServiceRqID(data.getService_request_id());
                            service.setServiceName(data.getService());
                            service.setSubService(data.getSubservice());

                            if (data.getNte_provider() != null) {

                                if (data.getNte_provider().intValue() != 0) {
                                    service.setNteProvider(data.getNte_provider().intValue());
                                } else {
                                    service.setNteProvider(0);
                                }

                            } else {

                                service.setNteProvider(0);

                            }

                            if (data.getService_per_mts2().equals(Utils.HOURS_TAG)) {

                                service.setPrice(data.getTotal_price_mt2().intValue());
                                service.setTypePrice(true);

                            } else {

                                service.setPrice(data.getTechnical_hour_price().intValue());
                                service.setTypePrice(false);

                            }

                            if (data.getUrgent()) {

                                service.setPriority(Utils.URGENT_TAG);

                            } else {

                                service.setPriority("");

                            }


                            if (data.getComment_client() != null) {

                                service.setComments(data.getComment_client());


                            } else {

                                service.setComments(activity.getResources().getString(R.string.noClientComments));

                            }

                            int indexFor = 0;
                            ArrayList<DateService> dateServices = new ArrayList<DateService>();

                            Collections.sort(data.getPreference_selected(), new Comparator<String>() {
                                public int compare(String dateFirst, String dateSecond) {
                                    return dateFirst.compareTo(dateSecond);
                                }
                            });

                            for (String dateDate : data.getPreference_selected()) {

                                String startDateHourFor = dateDate.substring(0, 19);
                                String endDateHourFor = dateDate.substring(22, dateDate.length());

                                String[] dateStart = startDateHourFor.split(" ");
                                String[] dateEnd = endDateHourFor.split(" ");

                                DateService dateService = new DateService();

                                if (Utils.compareDates(dateStart[0], dateEnd[0]) == 2) {

                                    String[] startDateSplit = dateStart[0].split("-");
                                    String[] endDateSplit = dateEnd[0].split("-");

                                    Date startDateDate = new Date(Integer.parseInt(startDateSplit[2]), Integer.parseInt(startDateSplit[1]) - 1, Integer.parseInt(startDateSplit[0]));
                                    Date endDateDate = new Date(Integer.parseInt(endDateSplit[2]), Integer.parseInt(endDateSplit[1]) - 1, Integer.parseInt(endDateSplit[0]));

                                    Calendar cal1 = new GregorianCalendar();
                                    Calendar cal2 = new GregorianCalendar();

                                    cal1.setTime(startDateDate);
                                    cal2.setTime(endDateDate);

                                    int days = Utils.daysBetween(cal1.getTime(), cal2.getTime());

                                    for (int i = 0; i <= days; i++) {

                                        if (i == 0) {

                                            if (indexFor == 0) {

                                                dateService = new DateService(dateStart[0], dateStart[0], dateStart[1] + " " + dateStart[2], endTimeDefault, true);

                                            } else {

                                                dateService = new DateService(dateStart[0], dateStart[0], dateStart[1] + " " + dateStart[2], endTimeDefault, false);

                                            }

                                        } else if (i == days) {

                                            if (indexFor == 0) {

                                                dateService = new DateService(dateEnd[0], dateEnd[0], startTimeDefault, dateEnd[1] + " " + dateEnd[2], true);

                                            } else {

                                                dateService = new DateService(dateEnd[0], dateEnd[0], startTimeDefault, dateEnd[1] + " " + dateEnd[2], false);

                                            }


                                        } else {

                                            cal1.add(Calendar.DATE, 1);
                                            String dayCalString = cal1.get(Calendar.DAY_OF_MONTH) + "-" + String.valueOf(cal1.get(Calendar.MONTH) + 1) + "-" + String.valueOf(cal1.get(Calendar.YEAR) - 1900);

                                            if (indexFor == 0) {


                                                dateService = new DateService(dayCalString, dayCalString, startTimeDefault, endTimeDefault, true);

                                            } else {

                                                dateService = new DateService(dayCalString, dayCalString, startTimeDefault, endTimeDefault, false);

                                            }


                                        }

                                        indexFor++;

                                        dateServices.add(dateService);


                                    }


                                } else {

                                    if (indexFor == 0) {

                                        dateService = new DateService(dateStart[0], dateEnd[0], dateStart[1] + " " + dateStart[2], dateEnd[1] + " " + dateEnd[2], true);

                                    } else {

                                        dateService = new DateService(dateStart[0], dateEnd[0], dateStart[1] + " " + dateStart[2], dateEnd[1] + " " + dateEnd[2], false);

                                    }

                                    indexFor++;

                                    dateServices.add(dateService);


                                }


                            }

                            String startDateData = data.getPreference_selected().get(0);
                            String endDateData = data.getPreference_selected().get(data.getPreference_selected().size() - 1);


                            service.setPreferedDates(dateServices);

                            String startDateHour = startDateData.substring(0, 19);
                            String endDateHour = endDateData.substring(22, endDateData.length());

                            String[] dateStart = startDateHour.split(" ");
                            String[] dateEnd = endDateHour.split(" ");

                            service.setDateStart(dateStart[0]);
                            service.setDateFinish(dateEnd[0]);
                            service.setTimeStart(dateStart[1] + " " + dateStart[2]);
                            service.setTimeFinish(dateEnd[1] + " " + dateEnd[2]);


                            if (data.getVisibility_tariff().equals(Utils.NO_TAG)) {

                                service.setVisibility(false);

                            } else {

                                service.setVisibility(true);

                            }

                            service.setPostalCode(data.getCodigo_postal());

                            avServList.add(service);


                        }

                        asca = new AvailableServicesAdapter(AvailableServicesActivity.this, avServList, "A", activity);
                        asca.setCallback(AvailableServicesActivity.this);
                        if (availableServices != null) {
                            availableServices.setAdapter(asca);
                            availableServices.setEmptyView(findViewById(R.id.emptyAvServActView));
                        }


                    } else {

                        displayError(servicesAvailable.getMessage(), SERVICE_AVAILABLE);

                    }


                } else

                {

                    availableServicesRetry++;

                    if (availableServicesRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.generic_error_message), SERVICE_AVAILABLE);

                    }

                }

            }

            @Override
            public void onFailure
                    (Call<com.gabrielguzman.android.famaex.web.models.Service> call, Throwable t) {

                availableServicesRetry++;

                if (availableServicesRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.generic_error_message), SERVICE_AVAILABLE);

                }

            }
        });


    }


    private void displayError(String message, int option) {


        if (option == SERVICE_AVAILABLE) {


            Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

            asca = new AvailableServicesAdapter(AvailableServicesActivity.this, avServList, "A", activity);
            asca.setCallback(AvailableServicesActivity.this);
            if (availableServices != null) {
                availableServices.setAdapter(asca);
                availableServices.setEmptyView(findViewById(R.id.emptyAvServActView));
            }

        } else {

            Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void wonDialogDismissed() {
        updateData();
    }

}
