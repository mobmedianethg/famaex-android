package com.gabrielguzman.android.famaex.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.dashboard.DashboardActivity;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.UserEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A login screen that offers access via username/password.
 *
 * @author Gabriel Guzman
 * @see AppCompatActivity
 * @since 2016.23.06
 */
public class LoginActivity extends AppCompatActivity implements Callback<User> {

    public static final String LOG_TAG = "LoginActivity";
    public static int loginUserRetry;
    public Retrofit retrofit;
    private EditText mUsernameView;
    private EditText mPasswordView;
    private View mProgressOverlay;
    private TextView mEmailSignInButton;
    private TextView mRegisterButton;
    private TextView mForgotPsswdTxtButton;
    private LoginActivity activity;
    private SharedPreferences mUserPref;
    private User userResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get the user shared preferences
        mUserPref = getSharedPreferences(AppSharedPreferences.SHARED_NAME, MODE_PRIVATE);

        // Go to Dashboard if the user is already logged
        if (mUserPref.getBoolean(AppSharedPreferences.LOGGED_USER, false)) {

            Intent homeIntent = new Intent(this, DashboardActivity.class);
            startActivity(homeIntent);
            finish();
        }

        activity = this;
        retrofit = RestClient.getRetrofitInstance();

        Utils.setPortaitOrientation(activity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.white_background));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        }

        setContentView(R.layout.activity_login);

        mUsernameView = (EditText) findViewById(R.id.user);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {

                    Utils.hideSoftKeyboard(activity);

                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton = (TextView) findViewById(R.id.sign_in_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideSoftKeyboard(activity);

                attemptLogin();

            }
        });

        mRegisterButton = (TextView) findViewById(R.id.registerButton);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(registerIntent);
            }
        });

        mForgotPsswdTxtButton = (TextView) findViewById(R.id.recoverPsswdTxtButton);
        mForgotPsswdTxtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent recoverIntent = new Intent(LoginActivity.this, RecoverPasswordActivity.class);
                startActivity(recoverIntent);
            }
        });

        mProgressOverlay = findViewById(R.id.progress_overlay);
        ImageView mLogo = (ImageView) findViewById(R.id.famaexLogoLogin);
        if (mLogo != null) {
            mLogo.requestFocus();
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid user, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String user = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {

            mPasswordView.setError(getString(R.string.errorFieldRequired));
            focusView = mPasswordView;
            cancel = true;

        } else if (password.length() < 6) {

            mPasswordView.setError(getString(R.string.invalid_password_lenght));
            focusView = mPasswordView;
            cancel = true;

        }

        // Check for a valid username.
        if (TextUtils.isEmpty(user)) {

            mUsernameView.setError(getString(R.string.errorFieldRequired));
            focusView = mUsernameView;
            cancel = true;

        } else if (!Utils.verifyEmail(user)) {

            mUsernameView.setError(getString(R.string.invalid_mail_format));
            focusView = mUsernameView;
            cancel = true;

        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            mEmailSignInButton.setEnabled(false);
            mForgotPsswdTxtButton.setEnabled(false);
            mRegisterButton.setEnabled(false);

            Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

            UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);

            User userLogin = new User(null, user, password, null, null, null, null, null, null, null);

            Call<User> call = userEndPointInterface.loginUser(RestClient.CONTENT_TYPE, userLogin);
            call.enqueue(activity);
        }
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {

        if (response.body() != null) {

            userResponse = response.body();

            if (Utils.verifyOKTAG(userResponse, Utils.LOGIN_USER, activity)) {

                mUserPref.edit().putBoolean(AppSharedPreferences.LOGGED_USER, true).apply();
                mUserPref.edit().putInt(AppSharedPreferences.PROVIDER_ID, userResponse.getId()).apply();
                mUserPref.edit().putString(AppSharedPreferences.PROVIDER_NAME, userResponse.getName()).apply();
                mUserPref.edit().putString(AppSharedPreferences.PROVIDER_EMAIL, userResponse.getEmail()).apply();
                mUserPref.edit().putString(AppSharedPreferences.PROVIDER_COMPANY, userResponse.getCompany().getCompany()).apply();
                mUserPref.edit().putBoolean(AppSharedPreferences.PROVIDER_ADMIN, userResponse.getAdmin()).apply();

                Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

                Intent homeIntent = new Intent(getBaseContext(), DashboardActivity.class);
                finish();
                startActivity(homeIntent);


            } else {

                displayError(userResponse.getStatus());

            }


        } else {

            loginUserRetry++;

            if (loginUserRetry < 3) {

                call.clone().enqueue(this);

            } else {

                displayError(activity.getResources().getString(R.string.login_error_message));

            }

        }


    }

    private void displayError(String message) {

        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

        mEmailSignInButton.setEnabled(true);
        mForgotPsswdTxtButton.setEnabled(true);
        mRegisterButton.setEnabled(true);


    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {

        loginUserRetry++;

        if (loginUserRetry < 3) {

            call.clone().enqueue(this);

        } else {

            displayError(activity.getResources().getString(R.string.login_error_message));

        }

    }

}
