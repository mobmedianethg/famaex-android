package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 06/04/17.
 */

public class TakeService {

    private Integer id;
    private Integer service_request_id;
    private String preference_date;
    private String preference_time;

    public TakeService() {
    }

    public TakeService(Integer id, Integer service_request_id, String preference_date, String preference_time) {
        this.id = id;
        this.service_request_id = service_request_id;
        this.preference_date = preference_date;
        this.preference_time = preference_time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getService_request_id() {
        return service_request_id;
    }

    public void setService_request_id(Integer service_request_id) {
        this.service_request_id = service_request_id;
    }

    public String getPreference_date() {
        return preference_date;
    }

    public void setPreference_date(String preference_date) {
        this.preference_date = preference_date;
    }

    public String getPreference_time() {
        return preference_time;
    }

    public void setPreference_time(String preference_time) {
        this.preference_time = preference_time;
    }
}
