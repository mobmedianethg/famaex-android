package com.gabrielguzman.android.famaex.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Application;
import android.view.View;

import com.flurry.android.FlurryAgent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Class that extends {@code Application} for initializing the Flurry Analytics
 *
 * @author Gabriel Guzman
 * @see Application
 * @since 2016.23.06
 */
public class Famaex extends Application {

    private final static String FLURRY_API_KEY = "S57CS3JX3JDMWDR8RXR6";
    public static SimpleDateFormat dateBackendFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
    public static SimpleDateFormat dateBackendBackwardFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    public static SimpleDateFormat dateArrivalBackendFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    public static SimpleDateFormat timeBackendFormat = new SimpleDateFormat("kk:mm:ss", Locale.getDefault());
    public static SimpleDateFormat dateAppFormat = new SimpleDateFormat("dd/MMM/yyyy", new Locale("es", "ES"));
    public static SimpleDateFormat timeAppFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    public static SimpleDateFormat timeAppFormatEnglish = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    public static SimpleDateFormat timeServerAppFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    public static SimpleDateFormat timeServerAppFormatEnglish = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    public static SimpleDateFormat combinedAppFormat = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss", Locale.getDefault());

    /**
     * @param view         View to animate
     * @param toVisibility Visibility at the end of animation
     * @param toAlpha      Alpha at the end of animation
     * @param duration     Animation duration in ms
     */
    public static void animateView(final View view, final int toVisibility, float toAlpha, int duration) {
        boolean show = toVisibility == View.VISIBLE;
        if (show) {
            view.setAlpha(0);
        }
        view.setVisibility(View.VISIBLE);
        view.animate()
                .setDuration(duration)
                .alpha(show ? toAlpha : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(toVisibility);
                    }
                });
    }

    public static Calendar dateToCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        new FlurryAgent.Builder()
                .withLogEnabled(false)
                .build(this, FLURRY_API_KEY);
    }
}