package com.gabrielguzman.android.famaex.services.finished;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.models.Service;
import com.gabrielguzman.android.famaex.utils.Utils;

import java.util.ArrayList;

/**
 * Activity that shows the complete list of completed services.
 *
 * @author Gabriel Guzman
 * @see AppCompatActivity
 * @since 2016.21.07
 */
public class FinishedServicesActivity extends AppCompatActivity {

    private ListView compServices;
    private FinishedServicesActivity activity;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finished_services_activity);

        //Set the toolbar
        Toolbar famaexToolbar = (Toolbar) findViewById(R.id.famaexCompServToolbar);
        setSupportActionBar(famaexToolbar);
        ActionBar sab = getSupportActionBar();

        if (sab != null) {
            sab.setDisplayHomeAsUpEnabled(true);
        }

        activity = (FinishedServicesActivity) this;

        Utils.setPortaitOrientation(activity);

        //-------------------
        ArrayList<Service> compServList = (ArrayList<Service>) getIntent().getSerializableExtra(Utils.FAMAEX_FINISHED_SERIAZABLE);

        compServices = (ListView) findViewById(R.id.compActList);
        compServices.setAdapter(new FinishedServicesAdapter(FinishedServicesActivity.this, compServList, "A"));
        compServices.setEmptyView(findViewById(R.id.emptyCompServActView));

    }
}
