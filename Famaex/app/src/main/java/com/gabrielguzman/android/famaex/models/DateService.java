package com.gabrielguzman.android.famaex.models;

import java.io.Serializable;

/**
 * Created by jose on 25/04/17.
 */

public class DateService implements Serializable {

    private String startDate;
    private String endDate;
    private String startTime;
    private String endTime;
    private boolean selected;

    public DateService() {
    }

    public DateService(String startDate, String endDate, String startTime, String endTime, boolean selected) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.selected = selected;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
