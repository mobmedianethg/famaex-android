package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 08/04/17.
 */

public class ServiceDetail {

    private Integer id;

    public ServiceDetail() {
    }

    public ServiceDetail(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
