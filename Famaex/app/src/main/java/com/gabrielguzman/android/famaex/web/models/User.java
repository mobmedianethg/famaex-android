package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 30/03/17.
 */

public class User {

    private Integer id;
    private String email;
    private String password;
    private String name;
    private Boolean admin;
    private Company company;
    private String status;
    private Boolean provider;
    private String password_old;
    private String password_new;

    public User(Integer id, String email, String password, String name, Boolean admin, Company company, String status, Boolean provider, String password_old, String password_new) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.admin = admin;
        this.company = company;
        this.status = status;
        this.provider = provider;
        this.password_old = password_old;
        this.password_new = password_new;
    }

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getProvider() {
        return provider;
    }

    public void setProvider(Boolean provider) {
        this.provider = provider;
    }

    public String getPassword_old() {
        return password_old;
    }

    public void setPassword_old(String password_old) {
        this.password_old = password_old;
    }

    public String getPassword_new() {
        return password_new;
    }

    public void setPassword_new(String password_new) {
        this.password_new = password_new;
    }
}
