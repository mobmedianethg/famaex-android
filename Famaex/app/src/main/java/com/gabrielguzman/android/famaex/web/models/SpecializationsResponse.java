package com.gabrielguzman.android.famaex.web.models;

import java.util.ArrayList;

/**
 * Created by jose on 08/04/17.
 */

public class SpecializationsResponse {

    private String status;
    private ArrayList<String> specialization;

    public SpecializationsResponse() {
    }

    public SpecializationsResponse(String status, ArrayList<String> specialization) {
        this.status = status;
        this.specialization = specialization;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<String> getSpecialization() {
        return specialization;
    }

    public void setSpecialization(ArrayList<String> specialization) {
        this.specialization = specialization;
    }

}
