package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 31/03/17.
 */

public class TokenUpdateResult {

    private String status;


    public TokenUpdateResult(String status) {
        this.status = status;
    }

    public TokenUpdateResult() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
