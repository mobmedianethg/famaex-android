package com.gabrielguzman.android.famaex.web.models;

import java.util.ArrayList;

/**
 * Created by jose on 07/04/17.
 */

public class Approved {

    private String status;
    private String message;
    private ArrayList<DataApproved> data;

    public Approved() {
    }

    public Approved(String status, String message, ArrayList<DataApproved> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DataApproved> getData() {
        return data;
    }

    public void setData(ArrayList<DataApproved> data) {
        this.data = data;
    }
}
