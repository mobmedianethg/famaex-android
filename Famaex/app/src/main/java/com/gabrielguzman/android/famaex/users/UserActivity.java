package com.gabrielguzman.android.famaex.users;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.utils.AppSharedPreferences;
import com.gabrielguzman.android.famaex.utils.Famaex;
import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.utils.Utils;
import com.gabrielguzman.android.famaex.web.endpoints.UserEndPointInterface;
import com.gabrielguzman.android.famaex.web.models.TariffRequest;
import com.gabrielguzman.android.famaex.web.models.TariffResponse;
import com.gabrielguzman.android.famaex.web.models.UserTariff;
import com.rey.material.widget.Switch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UserActivity extends AppCompatActivity implements View.OnClickListener, Switch.OnCheckedChangeListener {

    public static final String LOG_TAG = "UserActivity";
    private static int getTariffVisibilityRetry;
    private static int updateTariffVisibilityRetry;
    private Toolbar activityUserToolbar;
    private TextView activityUserRegisterUserTextview;
    private Switch activityUserVisibilityCheck;
    private TariffResponse tariffResponse;
    public Retrofit retrofit;
    private UserActivity activity;
    private SharedPreferences mUserPref;
    private boolean reverseChange;
    private boolean status;
    private View mProgressOverlay;
    private TextView progressMssg;
    private static final int GET_TARIFF_STATE = 1;
    private static final int UPDATE_TARIFF_STATE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        retrofit = RestClient.getRetrofitInstance();
        activity = this;
        Utils.setPortaitOrientation(activity);

        status = true;
        reverseChange = false;

        activityUserToolbar = (Toolbar) findViewById(R.id.activity_user_toolbar);
        setSupportActionBar(activityUserToolbar);

        mUserPref = getSharedPreferences(AppSharedPreferences.SHARED_NAME, MODE_PRIVATE);

        ActionBar actionBar = getSupportActionBar();

        // Enable the Up button
        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(true);

        }


        activityUserRegisterUserTextview = (TextView) findViewById(R.id.activity_user_register_user_textview);
        mProgressOverlay = findViewById(R.id.progress_overlay);
        progressMssg = (TextView) findViewById(R.id.progressMssg);
        activityUserVisibilityCheck = (Switch) findViewById(R.id.activity_user_visibility_checkbox);


        activityUserRegisterUserTextview.setOnClickListener(this);

        getTariffVisibility();


    }

    public void getTariffVisibility() {

        activityUserRegisterUserTextview.setEnabled(false);
        activityUserVisibilityCheck.setEnabled(false);

        progressMssg.setText(activity.getResources().getString(R.string.loading_getting_tariff_state));

        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        getTariffVisibilityRetry = 0;
        UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);
        TariffRequest tariffRequest = new TariffRequest(mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), null);

        Call<TariffResponse> call = userEndPointInterface.getTariffVisibility(RestClient.CONTENT_TYPE, tariffRequest);
        call.enqueue(new Callback<TariffResponse>() {
            @Override
            public void onResponse(Call<TariffResponse> call, Response<TariffResponse> response) {


                if (response.body() != null) {

                    tariffResponse = response.body();

                    if (Utils.verifyOKTAG(tariffResponse, activity)) {

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

                        for (UserTariff userTariff : tariffResponse.getData()) {

                            if (!userTariff.getVisibilityTariff()) {

                                status = false;
                                break;

                            }

                        }


                        if (status) {

                            activityUserVisibilityCheck.setChecked(true);

                        } else {

                            activityUserVisibilityCheck.setChecked(false);

                        }

                        activityUserVisibilityCheck.setOnCheckedChangeListener(activity);
                        activityUserRegisterUserTextview.setEnabled(true);
                        activityUserVisibilityCheck.setEnabled(true);

                    } else {

                        displayError(tariffResponse.getStatus(), GET_TARIFF_STATE);

                    }


                } else {

                    getTariffVisibilityRetry++;

                    if (getTariffVisibilityRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.global_network_error), GET_TARIFF_STATE);

                    }

                }

            }

            @Override
            public void onFailure(Call<TariffResponse> call, Throwable t) {

                getTariffVisibilityRetry++;

                if (getTariffVisibilityRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.global_network_error), GET_TARIFF_STATE);

                }

            }
        });


    }


    public void updateTariffVisibility() {

        activityUserRegisterUserTextview.setEnabled(false);
        activityUserVisibilityCheck.setEnabled(false);

        progressMssg.setText(activity.getResources().getString(R.string.loading_updating_tariff_state));

        Famaex.animateView(mProgressOverlay, View.VISIBLE, 1.0f, 200);

        getTariffVisibilityRetry = 0;
        UserEndPointInterface userEndPointInterface = retrofit.create(UserEndPointInterface.class);
        TariffRequest tariffRequest = new TariffRequest(mUserPref.getInt(AppSharedPreferences.PROVIDER_ID, 0), status);

        Call<TariffResponse> call = userEndPointInterface.updateTariffVisibility(RestClient.CONTENT_TYPE, tariffRequest);
        call.enqueue(new Callback<TariffResponse>() {
            @Override
            public void onResponse(Call<TariffResponse> call, Response<TariffResponse> response) {


                if (response.body() != null) {

                    tariffResponse = response.body();

                    if (Utils.verifyOKTAG(tariffResponse, activity)) {

                        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);

                        reverseChange = false;

                        activityUserVisibilityCheck.setEnabled(true);
                        activityUserRegisterUserTextview.setEnabled(true);

                    } else {

                        displayError(tariffResponse.getStatus(), UPDATE_TARIFF_STATE);

                    }


                } else {

                    updateTariffVisibilityRetry++;

                    if (updateTariffVisibilityRetry < 3) {

                        call.clone().enqueue(this);

                    } else {

                        displayError(activity.getResources().getString(R.string.global_network_error), UPDATE_TARIFF_STATE);

                    }

                }

            }

            @Override
            public void onFailure(Call<TariffResponse> call, Throwable t) {

                updateTariffVisibilityRetry++;

                if (updateTariffVisibilityRetry < 3) {

                    call.clone().enqueue(this);

                } else {

                    displayError(activity.getResources().getString(R.string.global_network_error), UPDATE_TARIFF_STATE);

                }

            }
        });


    }


    public void displayError(String message, int serviceOption) {

        Famaex.animateView(mProgressOverlay, View.GONE, 0, 200);
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

        switch (serviceOption) {

            case (GET_TARIFF_STATE):

                activityUserRegisterUserTextview.setEnabled(true);
                activityUserVisibilityCheck.setEnabled(true);

                break;

            case (UPDATE_TARIFF_STATE):

                reverseChange = true;

                if (status) {

                    activityUserVisibilityCheck.setChecked(false);
                    status = false;

                } else {

                    activityUserVisibilityCheck.setChecked(true);
                    status = true;

                }

                reverseChange = false;
                activityUserRegisterUserTextview.setEnabled(true);
                activityUserVisibilityCheck.setEnabled(true);

                break;


        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case (R.id.activity_user_register_user_textview):

                Intent userRegisterActivityIntent = new Intent(this, RegisterUserActivity.class);
                startActivity(userRegisterActivityIntent);

                break;


        }

    }

    @Override
    public void onCheckedChanged(Switch view, boolean checked) {

        if (checked) {

            status = true;

        } else {

            status = false;

        }

        if (!reverseChange) {

            updateTariffVisibility();

        }


    }
}
