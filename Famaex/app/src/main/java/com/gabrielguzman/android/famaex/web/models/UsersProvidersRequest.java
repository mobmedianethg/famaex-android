package com.gabrielguzman.android.famaex.web.models;

/**
 * Created by jose on 20/04/17.
 */

public class UsersProvidersRequest {

    private Integer id_admin;

    public UsersProvidersRequest() {
    }

    public UsersProvidersRequest(Integer id_admin) {
        this.id_admin = id_admin;
    }

    public Integer getId_admin() {
        return id_admin;
    }

    public void setId_admin(Integer id_amin) {
        this.id_admin = id_amin;
    }
}
