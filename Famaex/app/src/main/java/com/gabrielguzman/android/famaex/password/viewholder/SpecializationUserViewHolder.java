package com.gabrielguzman.android.famaex.password.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.gabrielguzman.android.famaex.R;

/**
 * Created by macpro1 on 6/12/16.
 */
public class SpecializationUserViewHolder extends RecyclerView.ViewHolder {

    private TextView specializationUserViewHolderNameTextview;

    public SpecializationUserViewHolder(final View itemView) {
        super(itemView);

        specializationUserViewHolderNameTextview = (TextView) itemView.findViewById(R.id.specialization_user_item_view_holder_item_name_textview);

    }

    public TextView getSpecializationUserViewHolderNameTextview() {
        return specializationUserViewHolderNameTextview;
    }

    public void setSpecializationUserViewHolderNameTextview(TextView specializationUserViewHolderNameTextview) {
        this.specializationUserViewHolderNameTextview = specializationUserViewHolderNameTextview;
    }
}
