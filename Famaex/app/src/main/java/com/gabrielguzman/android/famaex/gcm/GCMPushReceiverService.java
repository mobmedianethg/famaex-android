package com.gabrielguzman.android.famaex.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.gabrielguzman.android.famaex.R;
import com.gabrielguzman.android.famaex.dashboard.DashboardActivity;
import com.google.android.gms.gcm.GcmListenerService;

/**
 * Class that manages the push notifications
 *
 * @author Gabriel Guzmán
 * @since 2016.15.07
 */
public class GCMPushReceiverService extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {

        if (data.containsKey("notification")) {
            Bundle message = data.getBundle("notification");
            int srID = Integer.parseInt(
                    (data.containsKey("service_request_id")) ?
                            data.getString("service_request_id") : "0");
            if (message != null && !message.isEmpty()) {
                sendNotification(message, srID);
            }
        }

    }

    private void sendNotification(Bundle message, int srID) {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("com.gabrielguzman.android.famaex.Message", message);
        intent.putExtra("com.gabrielguzman.android.famaex.Srid", srID);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        int requestCode = srID;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        //Setup notification
        //Sound
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //Build notification
        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(message.getString("title"))
                .setSmallIcon(R.drawable.ic_android_black_24dp)
                .setContentText(message.getString("body"))
                .setAutoCancel(true)
                .setSound(sound)
                .setVibrate(new long[]{1000, 500, 250, 500, 250})
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, noBuilder.build()); //0 = ID of notification
    }
}
