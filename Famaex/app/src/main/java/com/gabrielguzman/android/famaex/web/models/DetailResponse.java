package com.gabrielguzman.android.famaex.web.models;

import java.util.ArrayList;

/**
 * Created by jose on 08/04/17.
 */

public class DetailResponse {

    private String status;
    private String message;
    private ArrayList<DataDetail> data;

    public DetailResponse() {
    }

    public DetailResponse(String status, String message, ArrayList<DataDetail> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DataDetail> getData() {
        return data;
    }

    public void setData(ArrayList<DataDetail> data) {
        this.data = data;
    }

}
