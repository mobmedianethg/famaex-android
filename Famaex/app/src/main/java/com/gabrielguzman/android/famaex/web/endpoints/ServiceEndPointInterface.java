package com.gabrielguzman.android.famaex.web.endpoints;

import com.gabrielguzman.android.famaex.utils.RestClient;
import com.gabrielguzman.android.famaex.web.models.Approved;
import com.gabrielguzman.android.famaex.web.models.DetailResponse;
import com.gabrielguzman.android.famaex.web.models.History;
import com.gabrielguzman.android.famaex.web.models.NteRequest;
import com.gabrielguzman.android.famaex.web.models.NteResponse;
import com.gabrielguzman.android.famaex.web.models.Service;
import com.gabrielguzman.android.famaex.web.models.ServiceDetail;
import com.gabrielguzman.android.famaex.web.models.TakeService;
import com.gabrielguzman.android.famaex.web.models.UpdateResponse;
import com.gabrielguzman.android.famaex.web.models.UpdateService;
import com.gabrielguzman.android.famaex.web.models.User;
import com.gabrielguzman.android.famaex.web.models.UsersProvidersRequest;
import com.gabrielguzman.android.famaex.web.models.UsersProvidersResponse;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;

/**
 * Created by jose on 04/04/17.
 */

public interface ServiceEndPointInterface {

    public static final String LOG_TAG = "ServiceEndPointInterface";

    public static final String AVAILABLE_SERVICES_URL = "provider_service_requests/AvailableServices";
    public static final String APPROVED_SERVICES_URL = "provider_service_requests/ApprovedRequests";
    public static final String SERVICE_HISTORY_URL = "provider_service_requests/ServiceHistory";
    public static final String APPLY_SERVICE_URL = "provider_service_requests/AddCandidate";
    public static final String DETAIL_SERVICE_URL = "provider_service_requests/ServiceDetail";
    public static final String UPDATE_SERVICE_URL = "provider_service_requests/UpdateServices";
    public static final String GET_USERS_PROVIDERS_URL = "providers/UsersProvider";
    public static final String NTE_EXCEDED = "provider_service_requests/NTEExceeded";


    public static final String SERVICE_REQUEST_ID = "service_request_id";
    public static final String STATUS_ID = "status_id";
    public static final String COMMENT_PROVIDER = "comment_provider";
    public static final String MATERIAL_COSTS = "material_costs";
    public static final String TECHNICAL_PRICE = "technical_price";
    public static final String ASSISTANCE_PRICE = "assistant_price";


    @POST(AVAILABLE_SERVICES_URL)
    Call<Service> availableServices(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body User user);

    @POST(APPLY_SERVICE_URL)
    Call<Service> applyService(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body TakeService takeService);

    @POST(SERVICE_HISTORY_URL)
    Call<History> serviceHistory(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body User user);

    @POST(APPROVED_SERVICES_URL)
    Call<Approved> approvedServices(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body User user);

    @PUT(UPDATE_SERVICE_URL)
    Call<UpdateResponse> updateService(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body UpdateService updateService);

    @Multipart
    @PUT(UPDATE_SERVICE_URL)
    Call<UpdateResponse> updateServiceMultipart(@Part(SERVICE_REQUEST_ID) RequestBody service_request_id,
                                                @Part(STATUS_ID) RequestBody status_id,
                                                @Part ArrayList<MultipartBody.Part> image);

    @PUT(NTE_EXCEDED)
    Call<NteResponse> updateNteExceded(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body NteRequest nteRequest);

    @Multipart
    @PUT(UPDATE_SERVICE_URL)
    Call<UpdateResponse> updateServiceMultipartFinish(@Part(SERVICE_REQUEST_ID) RequestBody service_request_id,
                                                      @Part(STATUS_ID) RequestBody status_id,
                                                      @Part(COMMENT_PROVIDER) RequestBody commentProvider,
                                                      @Part(MATERIAL_COSTS) RequestBody materialCosts,
                                                      @Part(TECHNICAL_PRICE) RequestBody technicalPrice,
                                                      @Part(ASSISTANCE_PRICE) RequestBody assistancePrice,
                                                      @Part ArrayList<MultipartBody.Part> image);

    @POST(DETAIL_SERVICE_URL)
    Call<DetailResponse> serviceDetail(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body ServiceDetail serviceDetail);

    @POST(GET_USERS_PROVIDERS_URL)
    Call<UsersProvidersResponse> getUsersProviders(@Header(RestClient.CONTENT_TYPE_HEADER) String contentType, @Body UsersProvidersRequest usersProvidersRequest);


}
